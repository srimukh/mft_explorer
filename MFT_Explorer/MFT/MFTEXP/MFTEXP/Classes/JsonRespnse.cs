﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using MFTEXP.Models;

namespace MFTEXP.Classes
{

    #region BASE CLASSES & ENUMS

    public class JsonRespnse
    {
        public JsonResponseStatus Status { get; set; }
        public JsonResponseError Error { get; set; }
        public JsonResponseResult Result { get; set; }

        public JsonRespnse()
        {
            this.Status = JsonResponseStatus.Unknown;
            this.Error = new JsonResponseError();
            this.Result = new JsonResponseResult();
        }

   }

    public enum JsonResponseStatus
    {
        Success,
        Failure,
        Unknown
    }

    public class JsonResponseError
    {
        public int Code { get; set; }
        public string Message { get; set; }
        public string Description { get; set; }
    }

    public class JsonResponseResult
    {
    }

    #endregion

    #region DERIVED CLASSES

    public class AgentsJsonResult : JsonResponseResult
    {
        public List<dynamic> Agents { get; set; }
    }
    public class QueuesJsonResult : JsonResponseResult
    {
        public List<MQ_Queues> Queues { get; set; }
    }
    public class MonitorsJsonResult : JsonResponseResult
    {
        public List<Monitordetails> Monitors { get; set; }
    }
    public class AgentsManagersJsonResult : JsonResponseResult
    {
        public List<MQ_QueueeManager> Managers { get; set; }
        public List<Agentdetails> Agents { get; set; }
    }
    public class TransferDetailsJsonResult : JsonResponseResult
    {
        public Transfer_details MonitorDetails { get; set; }
    }
    public class SchedulessJsonResult : JsonResponseResult
    {
        public List<dynamic> Schedulers { get; set; }
    }
    public class TransferLogsJsonResult : JsonResponseResult
    {
        public object LogData { get; set; }
    }
    public class JobNamesJsonResult : JsonResponseResult
    {
        public List<String> JobNames { get; set; }
    }

    public class HostConfigsJsonResult : JsonResponseResult
    {
        public List<HostConfig> Hosts { get; set; }
    }
    public class QueueManagersJsonResult : JsonResponseResult
    {
        public List<MQ_QueueeManager> Managers { get; set; }
    }
    public class UserExitDetailsJsonResult : JsonResponseResult
    {
        public List<User_Exit> UserExits { get; set; }
    }
    public class UserExitsJsonResult : JsonResponseResult
    {
        public List<UserExits> UserExits { get; set; }
    }
    #endregion

}