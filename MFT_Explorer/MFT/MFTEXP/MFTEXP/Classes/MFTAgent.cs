﻿using IBM.WMQ;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using MFTEXP.Models;
using System.Dynamic;

namespace MFTEXP.Classes
{
    public class MFTAgent
    {
        /// <summary>
        /// This method is for to get the agent and their properties from the MQ server.
        /// </summary>
        /// <param name="environment">environment</param>
        /// <returns></returns>
     
        public List<dynamic> GetAgents()
        {
            var Agent = new List<dynamic>();
            MQQueueManager MQMgr = null;
            MQTopic Mtopic = null;
            Hashtable properties = new Hashtable();
            MQMessage message = new MQMessage();
            MFTCommon mftcommon = new MFTCommon();
            try
            {
                List<HostConfig> hst = new List<HostConfig>();
                hst = mftcommon.GetHostdetails();
                HostConfig HstConfig = hst.FirstOrDefault();
                if (HstConfig != null)
                {
                    properties.Add(MQC.HOST_NAME_PROPERTY, HstConfig.Host_Name);
                    properties.Add(MQC.PORT_PROPERTY, HstConfig.Coord_Port);
                    properties.Add(MQC.CHANNEL_PROPERTY, HstConfig.Default_Channel);
                    MQMgr = new MQQueueManager(HstConfig.Coord_Qmgr, properties);

                    Mtopic = MQMgr.AccessTopic("SYSTEM.FTE/Agents/#", null, MQC.MQTOPIC_OPEN_AS_SUBSCRIPTION, MQC.MQSO_CREATE | MQC.MQSO_FAIL_IF_QUIESCING);
                    message = new MQMessage();
                    Mtopic.Get(message);
                    MQGetMessageOptions mqNext = new MQGetMessageOptions();
                    mqNext.Options = MQC.MQGMO_BROWSE_NEXT;
                    while (true)
                    {
                        var Agtdtls = new Dictionary<string, object>();
                        string strmsg = message.ReadString(message.MessageLength);
                        var xmlval = XElement.Parse(strmsg);
                        foreach (var element in xmlval.Elements())
                        {
                            string strattributevalue = element.Attributes().Single().Value.Trim();
                            string strvalue = string.Empty;
                            if (!string.IsNullOrEmpty(element.Value.ToString())) strvalue = element.Value.Trim().ToString();
                            Agtdtls.Add(strattributevalue,strvalue);
                        }
                        Agent.Add(Agtdtls);
                        message = new MQMessage();
                        Mtopic.Get(message);
                    }
                }
            }
            catch (MQException Mqex)
            {
                if (Mqex.ReasonCode != 2033)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(Mqex);  //This is for to handle "MQRC NO MSG AVAILABLE"
                    MFTErrorMessage.Error_Message = MFTConstants.Default_MQ_Error_Message;
                }
            }
            catch (Exception Ex)
            {
                MFTErrorMessage.Error_Message = MFTConstants.Default_Database_Error_Message;
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);

            }
            finally
            {
                if (MQMgr != null) MQMgr.Disconnect();
                if (Mtopic != null) Mtopic.Close();//Actually this is not required because MQ QueueManager disconnect will close all of queues/topics automatically before closes the connection to the mq queuemanager.
            }
            return Agent;
        }

        public string PingAgent(string AgentName, string AgtQMgr)
        {
            MQQueueManager MQMgr = null;
            MQQueue queue = null;
            MQQueue rqueue = null;
            Hashtable properties = new Hashtable();
            MQMessage message;
            MQPutMessageOptions putMessageOptions;
            String queueName = string.Empty;
            String messageString = string.Empty;
            string Status = "";
            try
            {
                MQMessage response =  new MQMessage();
                List<HostConfig> hst = new List<HostConfig>();
                MFTCommon mftcommon = new MFTCommon();
                hst = mftcommon.GetHostdetails();
                HostConfig HstConfig = hst.FirstOrDefault();
                if (HstConfig != null)
                {
                    queueName = MFTConstants.Default_Queue_Command + "." + AgentName;
                    //MQEnvironment.UserId = HstConfig.Userid;
                    properties.Add(MQC.HOST_NAME_PROPERTY, HstConfig.Host_Name);
                    properties.Add(MQC.PORT_PROPERTY, HstConfig.Agent_QMGR_Port);
                    properties.Add(MQC.CHANNEL_PROPERTY, HstConfig.Default_Channel);
                    //  properties.Add(MQC.USER_ID_PROPERTY, HstConfig.Userid);
                    MQMgr = new MQQueueManager(HstConfig.Agent_QMGR, properties);

                    queue = MQMgr.AccessQueue(queueName, MQC.MQOO_OUTPUT );
                    putMessageOptions = new MQPutMessageOptions();
                    putMessageOptions.Options = MQC.MQPMO_SYNCPOINT;
                    message = new MQMessage();

                    message.CharacterSet = 437;
                    messageString = string.Format(MFTConstants.Ping_Agent, HstConfig.Host_Name, HstConfig.Userid, AgentName, AgtQMgr);

                    try
                    {
                        if (!string.IsNullOrEmpty(messageString))
                        {
                            message.WriteString(messageString);
                                queue.Put(message, putMessageOptions);
                                response = Listen(properties, HstConfig.Coord_Qmgr, queueName);
                                //queue.Close();
                                //MQMgr.Disconnect();
                                //MQMgr = new MQQueueManager(HstConfig.Agent_QMGR, properties);
                                //rqueue = MQMgr.AccessQueue(queueName, MQC.MQOO_INPUT_EXCLUSIVE );
                                //rqueue.Get(response);
                                Status = response.ToString();
 
                            
                        }
                        else
                        {
                            Status = "No Response";
                        }
                        MQMgr.Disconnect();
                        queue.Close();
                    }
                    catch (Exception Ex)
                    {
                        MFTErrorMessage.Error_Message = MFTConstants.Default_Error_Message;
                        Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                        Status = Ex.ToString();
                    }
                }
            }
            catch (MQException Mqex)
            {

                Elmah.ErrorSignal.FromCurrentContext().Raise(Mqex);  //This is for to handle "MQRC NO MSG AVAILABLE"
                MFTErrorMessage.Error_Message = MFTConstants.Default_MQ_Error_Message;

            }
            catch (Exception Ex)
            {
                MFTErrorMessage.Error_Message = MFTConstants.Default_Error_Message;
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            finally
            {
                if (MQMgr != null)
                {
                    MQMgr.Disconnect();
                }

            }
            return Status;
        }


        public  MQMessage Listen(Hashtable properties,  string qmName, string queueName)
        {
            /** MQOO_INPUT_AS_Q_DEF -- open queue to get message 
        *  using queue-define default.
                *  MQOO_FAIL_IF_QUIESCING -- access fail if queue manager is quiescing. **/
            int openOptions = MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_FAIL_IF_QUIESCING;
            MQQueueManager mqManager = new MQQueueManager(qmName, properties);
            MQQueue queue = mqManager.AccessQueue(queueName, openOptions);

            /** MQGMO_FAIL_IF_QUIESCING -- 
        *  get message fail if queue manager is quiescing.
                *  MQGMO_WAIT -- waits for suitable message to arrive.
                *  MQWI_UNLIMITED -- unlimited wait interval. **/
            MQGetMessageOptions gmo = new MQGetMessageOptions();
            gmo.Options = MQC.MQGMO_WAIT;
            gmo.WaitInterval = MQC.MQWI_UNLIMITED;
            MQMessage message = new MQMessage();
            //wait for message
            queue.Get(message, gmo);
            queue.Close();

            //release resource.
            mqManager = null;
            queue = null;
            gmo = null;
            System.GC.Collect();

            return message;
        }
        




    }
}