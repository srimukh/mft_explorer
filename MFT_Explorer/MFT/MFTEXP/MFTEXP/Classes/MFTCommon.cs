﻿using IBM.WMQ;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using MFTEXP.Models;
using System.Reflection;


namespace MFTEXP.Classes
{
    public class MFTCommon
    {
        MFTHelper helper = new MFTHelper();
        public enum Current_Action_Performed
        {
            Create = 0,
            Started = 1,
            Paused = 2,
            Deleted = 3
        }
        public enum ResourceType
        {
            Directory,
            Queue
        }
        public enum RequestType
        {
            Monitor,
            Onetimetransfer
        }



        /// <summary>
        /// This method gets the host details from dbase and assign the same to properties
        /// </summary>
        /// <param name="environment"></param>
        /// <returns></returns>
        public List<HostConfig> GetHostdetails()
        {
            List<HostConfig> host = new List<HostConfig>();
            QueryParameters inputParams = new QueryParameters();
            inputParams.query = MFTQueries.GetHost;
            try
            {
                using (DataTable dt = helper.GetDataTable(inputParams))
                {
                    if (dt.Rows.Count > 0)
                    {
                        host = (from DataRow row in dt.Rows
                                select new HostConfig()
                                {
                                    Host_ID = Convert.ToInt32(row["HOST_ID"]),
                                    Host_Name = Convert.ToString(row["HOST_NAME"]),
                                    Host_IP = Convert.ToString(row["HOST_IP"]),
                                    Coord_Qmgr = Convert.ToString(row["COORD_QMGR"]),
                                    Coord_Port = Convert.ToInt32(row["COORD_PORT"]),
                                    Default_Channel = Convert.ToString(row["DEFAULT_CHANNEL"]),
                                    Agent_QMGR = Convert.ToString(row["AGENT_QMGR"]),
                                    Agent_QMGR_Port = Convert.ToInt32(row["AGENT_PORT"]),
                                    Userid = Convert.ToString(row["USER_ID"]),
                                    Active = Convert.ToChar(row["ACTIVE"])

                                }).ToList();
                    }
                }
            }
            catch(Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                throw;
            }
            
              return host;
        }

        public List<User_Exit> GetUserExitDetails()
        {
            List<User_Exit> UsrExitTemp = new List<User_Exit>();
            QueryParameters inputParams = new QueryParameters();
            inputParams.query = MFTQueries.GetUserExits;
            try
            {
                using (DataTable dt = helper.GetDataTable(inputParams))
                {
                    if (dt.Rows.Count > 0)
                    {
                        UsrExitTemp = (from DataRow row in dt.Rows
                                       select new User_Exit()
                                {
                                    UserExit_type = Convert.ToString(row["userexittype"]),
                                    UserExit_detail = Convert.ToString(row["userexitdetail"]).Trim(),
                                    Exit_Params = Convert.ToString(row["Exit_Params"]).Trim()

                                }).ToList();
                    }
                }
            }
            catch(Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            return UsrExitTemp;
        }

        public List<UserExits> GetUserExitTypes()
        {
            List<UserExits> UsrExitTemp = new List<UserExits>();
            QueryParameters inputParams = new QueryParameters();
            inputParams.query = MFTQueries.GetUserExitTypes;
            try
            {
                using (DataTable dt = helper.GetDataTable(inputParams))
                {
                    if (dt.Rows.Count > 0)
                    {
                        UsrExitTemp = (from DataRow row in dt.Rows
                                       select new UserExits()
                                       {
                                           Id = Convert.ToInt32(row["ID"]),
                                           Exit_Value = Convert.ToString(row["User_Exit_Type"]).Trim()
                                       }).ToList();
                    }
                }
            }
            catch (Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                throw Ex;
            }
            return UsrExitTemp;
        }

        public List<UserExits> GetUserExitValues(string id)
        {
            List<UserExits> UsrExitTemp = new List<UserExits>();
            QueryParameters inputParams = new QueryParameters();
            inputParams.query = MFTQueries.GetUserExitValues;
            inputParams.AddParam("ID", id, OdbcType.Int);
            try
            {
                using (DataTable dt = helper.GetDataTable(inputParams))
                {
                    if (dt.Rows.Count > 0)
                    {
                        UsrExitTemp = (from DataRow row in dt.Rows
                                       select new UserExits()
                                       {
                                           Exit_id = Convert.ToInt32(row["Exit_id"]),
                                           Id = Convert.ToInt32(row["ID"]),
                                           Exit_Value = Convert.ToString(row["user_exit_value"]).Trim(),
                                           Exit_Params = Convert.ToString(row["Exit_Params"]).Trim()

                                       }).ToList();
                    }
                }
            }
            catch (Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                MFTErrorMessage.Error_Message = MFTConstants.Default_Error_Message;
            }
            return UsrExitTemp;
        }

        /// <summary>
        /// Gets the xml from the mft monitor-action table based on the job name for to replay the failure transfer
        /// </summary>
        /// <param name="jobname"></param>
        /// <returns></returns>
        public DataTable GetXMLForReplay(string jobname)
        {
            QueryParameters inputParams = new QueryParameters();
            string strjbname = "%<job><name>" + jobname + "</name></job>%";
            inputParams.query = MFTQueries.Get_JobXML;
            inputParams.AddParam("jbname", strjbname, OdbcType.VarChar);
            using (DataTable dt = helper.GetDataTable(inputParams))
            {
                return dt;
            }
        }


        /// <summary>
        /// Reads the variable values from metadata table for to substitute the values to variable along with source and destination file name details for replay
        /// </summary>
        /// <param name="TransferID"></param>
        /// <returns></returns>
        public DataTable GetVariableValuesForReplay(string TransferID)
        {
            QueryParameters inputParams = new QueryParameters();
            inputParams.query = MFTQueries.Get_TransferValues;
            inputParams.AddParam("transferid", TransferID, OdbcType.Char);
            using (DataTable dt = helper.GetDataTable(inputParams))
            {
                return dt;
            }
        }


        /// <summary>
        /// Gets the list of permenantely deleted monitor names for the selected environment
        /// </summary>
        /// <param name="environment"></param>
        /// <returns></returns>
        public DataTable GetDeletedMonitors(int Host_id)
        {
            QueryParameters inputParams = new QueryParameters();
            inputParams.query =MFTQueries.DeletedMonitorsList;
            inputParams.AddParam("jbname", Host_id.ToString(), OdbcType.Int);
            using (DataTable dt = helper.GetDataTable(inputParams))
            {
                return dt;
            }
        }

        /// <summary>
        /// This method brings the monitor xml from mft monitor-action table for  monitor name
        /// </summary>
        /// <param name="MntrName"></param>
        /// <returns></returns>
        public DataTable GetMonitorXML(string MonitorID)
        {
            QueryParameters inputParams = new QueryParameters();
            inputParams.query = MFTQueries.Get_MonitorXML;
            inputParams.AddParam("MonitorID", MonitorID, OdbcType.Char);
            using (DataTable dt = helper.GetDataTable(inputParams))
            {
                return dt;
            }
        }

        public Boolean PutDeleteMessage(string AgentName, string QueueMessage)
        {

            MQQueueManager MQMgr = null;
            MQQueue queue;
            Hashtable properties = new Hashtable();
            MQMessage message;
            MQPutMessageOptions putMessageOptions;
            String queueName = string.Empty;
            String messageString = string.Empty;
            List<HostConfig> hst = new List<HostConfig>();
            hst = GetHostdetails();
            HostConfig HstConfig = hst.FirstOrDefault();
            Boolean Status = false;
            try
            {
                queueName = MFTConstants.Default_Queue_Command + "." + AgentName;
                //MQEnvironment.UserId = HstConfig.Userid;

                properties.Add(MQC.HOST_NAME_PROPERTY, HstConfig.Host_Name);
                properties.Add(MQC.PORT_PROPERTY, HstConfig.Agent_QMGR_Port);
                properties.Add(MQC.CHANNEL_PROPERTY, HstConfig.Default_Channel);
                //properties.Add(MQC.USER_ID_PROPERTY, HstConfig.Userid);
                MQMgr = new MQQueueManager(HstConfig.Agent_QMGR, properties);

                queue = MQMgr.AccessQueue(queueName, MQC.MQOO_OUTPUT);
                putMessageOptions = new MQPutMessageOptions();
                putMessageOptions.Options = MQC.MQPMO_SYNCPOINT;
                message = new MQMessage();
                message.CharacterSet = 437;
                messageString = QueueMessage;
                try
                {
                    if (!string.IsNullOrEmpty(messageString))
                    {
                        message.WriteString(messageString);
                        //for transaction complete or rollback implicit
                        using (TransactionScope scope = new TransactionScope())
                        {
                            queue.Put(message, putMessageOptions);
                            scope.Complete();
                            Status = true;
                        }
                    }

                    MQMgr.Disconnect();
                }
                catch (Exception Ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                    Status = false;

                }
            }
            catch (MQException mqe)
            {
                throw mqe;
            }
            catch (Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            finally
            {

                if (MQMgr != null) MQMgr.Disconnect();
            }
            return Status;
        }

        public Boolean PutMessage(string AgentName, string QueueMessage, string QMgrName, string HostName, String HostIP, string Channel, int Port, string UserID)
        {

            MQQueueManager MQMgr = null;
            MQQueue queue;
            Hashtable properties = new Hashtable();
            MQMessage message;
            MQPutMessageOptions putMessageOptions;
            String queueName = string.Empty;
            String messageString = string.Empty;
            List<HostConfig> hst = new List<HostConfig>();
            hst = GetHostdetails();
            HostConfig HstConfig = hst.FirstOrDefault();
            Boolean Status = false;
            try
            {
                queueName = MFTConstants.Default_Queue_Command + "." + AgentName;
                //MQEnvironment.UserId = HstConfig.Userid;

                properties.Add(MQC.HOST_NAME_PROPERTY, HostName );
                properties.Add(MQC.PORT_PROPERTY, Port );
                properties.Add(MQC.CHANNEL_PROPERTY, Channel);
                //properties.Add(MQC.USER_ID_PROPERTY, HstConfig.Userid);
                MQMgr = new MQQueueManager(QMgrName, properties);

                queue = MQMgr.AccessQueue(queueName, MQC.MQOO_OUTPUT);
                putMessageOptions = new MQPutMessageOptions();
                putMessageOptions.Options = MQC.MQPMO_SYNCPOINT;
                message = new MQMessage();
                message.CharacterSet = 437;
                messageString = QueueMessage;
                try
                {
                    if (!string.IsNullOrEmpty(messageString))
                    {
                        message.WriteString(messageString);
                        //for transaction complete or rollback implicit
                        using (TransactionScope scope = new TransactionScope())
                        {
                            queue.Put(message, putMessageOptions);
                            scope.Complete();
                            Status = true;
                        }
                    }

                    MQMgr.Disconnect();
                }
                catch (Exception Ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                    Status = false;

                }
            }
            catch (MQException mqe)
            {
                throw mqe;
            }
            catch (Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            finally
            {

                if (MQMgr != null) MQMgr.Disconnect();
            }
            return Status;
        }
        
        public List<Agentdetails> GetAgentsandQMGR()
        {
            var Agent = new List<Agentdetails>();
            MQQueueManager MQMgr = null;
            MQTopic Mtopic = null;
            Hashtable properties = new Hashtable();
            MQMessage message = new MQMessage();
            try
            {
                List<HostConfig> hst = new List<HostConfig>();
                hst = GetHostdetails();
                HostConfig HstConfig = hst.FirstOrDefault();
                if (HstConfig != null)
                {
                    properties.Add(MQC.HOST_NAME_PROPERTY, HstConfig.Host_Name);
                    properties.Add(MQC.PORT_PROPERTY, HstConfig.Coord_Port);
                    properties.Add(MQC.CHANNEL_PROPERTY, HstConfig.Default_Channel);
                    MQMgr = new MQQueueManager(HstConfig.Coord_Qmgr, properties);
                    Mtopic = MQMgr.AccessTopic("SYSTEM.FTE/Agents/#", null, MQC.MQTOPIC_OPEN_AS_SUBSCRIPTION, MQC.MQSO_CREATE | MQC.MQSO_FAIL_IF_QUIESCING);
                    message = new MQMessage();
                    Mtopic.Get(message);
                    MQGetMessageOptions mqNext = new MQGetMessageOptions();
                    mqNext.Options = MQC.MQGMO_BROWSE_NEXT;
                    while (true)
                    {
                        
                        Agentdetails Agtdtls = new Agentdetails();
                        string strmsg = message.ReadString(message.MessageLength);
                        var xmlval = XElement.Parse(strmsg);
                        foreach (var element in xmlval.Elements())
                        {
                            string strattributevalue = element.Attributes().Single().Value.Trim();
                            PropertyInfo PI = Agtdtls.GetType().GetProperty(strattributevalue);
                            if (PI != null)
                            {
                                string strvalue = string.Empty;
                                if (!string.IsNullOrEmpty(element.Value.ToString())) strvalue = element.Value.Trim().ToString();
                                PI.SetValue(Agtdtls, strvalue, null);
                            }
                        }
                        Agent.Add(Agtdtls);
                        message = new MQMessage();
                        try {
                        Mtopic.Get(message);
                        } catch (MQException mqe)
                        {
                            if (mqe.ReasonCode == 2033)
                            {
                                break;
                            }
                            else 
                            {
                                throw mqe;
                            }
                        }

                        //if (message.MessageFlags == 0)
                        //    break;
                        
                    }                    
                }
                Mtopic.Close();
                MQMgr.Close();
                MQMgr.Disconnect();
            }
            catch (MQException Mqex)
            {
                if (Mqex.ReasonCode != 2033)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(Mqex);
                   
                }//This is for to handle "MQRC NO MSG AVAILABLE"
            }
            catch (Exception Ex)
            {

                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                
            }
            finally
            {
                if (MQMgr != null) MQMgr.Disconnect();
                if (Mtopic != null) Mtopic.Close();//Actually this is not required because MQ QueueManager disconnect will close all of queues/topics automatically before closes the connection to the mq queuemanager.
            }
            return Agent;
        }

        public static string GetDefaultTimeZoneName()
        {
            try
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["DefaultTimeZoneName"]))
                    return ConfigurationManager.AppSettings["DefaultTimeZoneName"].ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return "America/New_York";

        }

        //public List<string> GetTimeZones()
        //{
        //    XmlDataDocument xmldoc = new XmlDataDocument();
        //    string str = null;
        //    FileStream fs = new FileStream("product.xml", FileMode.Open, FileAccess.Read);
        //    xmldoc.Load(fs);
        //    xmlnode = xmldoc.GetElementsByTagName("Product");
        //    for (i = 0; i <= xmlnode.Count - 1; i++)
        //    {

        //}


   }
}