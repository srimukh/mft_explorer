﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MFTEXP.Classes
{
    public class MFTConstants
    {

        public const String DeleteMonitor_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                                  "<monitor:deleteMonitor xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"6.00\" xmlns:monitor=\"http://www.ibm.com/xmlns/wmqfte/7.0.1/MonitorDefinition\" xsi:schemaLocation=\"http://www.ibm.com/xmlns/wmqfte/7.0.1/MonitorDefinition ./Monitor.xsd\" >" +
                                  "<name>{0}</name><originator><hostName>{1}</hostName><userID>{2}</userID></originator></monitor:deleteMonitor>";
        
        public const String Default_Queue_Command = "SYSTEM.FTE.COMMAND";

        public const String Host_Config_Failure_Message = "Host Config Update Failed..Please try later..";
        public const String Host_Config_Success_Message = "Host Config details updated Successfully";
        public const String Replay_Failure_Message = "Replay initiation Failed..Please try later..";
        public const String Replay_Success_Message = "Replay initiated Successfully..Please verify the transfer..";
        public const String Default_MQ_Error_Message = "MQ Server Connection failed,please validate and try again..";
        public const String Default_Database_Error_Message = "Database Connectivity failed,please validate and try again..";
        public const String Default_Error_Message = "Error occurred while processing your request,Please try later...";

        public const String Request_Failure_Message = "Unable to process your request, Please try again.";

        public const String QM_Failure_Message = "Queue manager Update Failed..Please try later..";
        public const String QM_Success_Message = "Queue manager details updated Successfully";

        public const String DeleteScheduledTransfer_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                                                        "<request xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" version=\"6.00\" xsi:noNamespaceSchemaLocation=\"FileTransfer.xsd\">" +
                                                        "<deleteScheduledTransfer><originator><delete><hostName>{0}</hostName><userID>{1}</userID></delete>" +
                                                        "</originator><ID>{2}</ID></deleteScheduledTransfer></request>";

        public const String Ping_Agent = @"<?xml version='1.0' encoding='UTF-8'?><ping:pingAgent xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:ping='http://www.ibm.com/xmlns/wmqfte/7.0.1/PingAgent' version='4.00'> <originator> <hostName>{0}</hostName> <userID>{1}</userID> </originator> <agent agent='{2}' QMgr='{3}' />   </ping:pingAgent>";

    }

    public static class MFTErrorMessage
    {
        static string _errorMessage;
        public static string Error_Message 
        {
            
            get { return _errorMessage; }
            set { _errorMessage = value; }
            
            }
    }

    




}