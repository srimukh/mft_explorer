﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace MFTEXP.Classes
{
    public class MFTHelper
    {
        public OdbcConnection GetDb2Connection()
        {
            OdbcConnection conn = new OdbcConnection(ConfigurationManager.ConnectionStrings["DB2ConnectionString"].ConnectionString);
            return conn;
        }

        public DataTable GetDataTable(QueryParameters QParameters)
        {
            using (DataTable DS = new DataTable())
            {
                using (OdbcDataAdapter adapter = new OdbcDataAdapter())
                {
                    OdbcConnection conn = GetDb2Connection();
                    try
                    {
                        conn.Open(); //make sure you are running visual studio as administrator
                        using(OdbcCommand command=new OdbcCommand(QParameters.query,conn))
                        {
                            foreach(QueryParam obj in QParameters.QueryParams)
                            {
                                if(string.IsNullOrEmpty(obj.value))
                                {
                                    command.Parameters.Add(obj.parameter, obj.type).Value = System.DBNull.Value;
                                }
                                else
                                {
                                    command.Parameters.Add(obj.parameter, obj.type).Value = obj.value;
                                }
                            }
                            adapter.SelectCommand = command;
                            adapter.SelectCommand.CommandTimeout = 0;
                            adapter.Fill(DS);
                        }
                     }
                    catch (OdbcException ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        throw;
                    }
                    finally
                    {
                        conn.Close();
                    }
                    return DS;
                }
            }
        }


        public Int32 UpdateCommand(QueryParameters QParameters)
        {
            Int32 updatedrows = 0;
            using (OdbcDataAdapter adapter = new OdbcDataAdapter())
                {
                    OdbcConnection conn = GetDb2Connection();
                    OdbcTransaction trans;
                    try
                    {
                        conn.Open(); //make sure you are running visual studio as administrator
                        trans = conn.BeginTransaction(IsolationLevel.ReadCommitted);

                        adapter.UpdateCommand = new OdbcCommand(QParameters.query, conn);
                        adapter.UpdateCommand.Transaction = trans;
                        using (OdbcCommand command = new OdbcCommand(QParameters.query, conn))
                        {
                            foreach (QueryParam obj in QParameters.QueryParams)
                            {
                                if (string.IsNullOrEmpty(obj.value))
                                {
                                    adapter.UpdateCommand.Parameters.Add(obj.parameter, obj.type).Value = System.DBNull.Value;
                                }
                                else
                                {
                                    adapter.UpdateCommand.Parameters.Add(obj.parameter, obj.type).Value = obj.value;
                                }
                            }
                            updatedrows = adapter.UpdateCommand.ExecuteNonQuery();
                            adapter.UpdateCommand.Transaction.Commit();
                        }
                    }
                    catch (OdbcException ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        throw;
                    }
                    finally
                    {
                        conn.Close();
                    }
                    
                }
            return updatedrows;
        }


        public Int32 InsertCommand(QueryParameters QParameters)
        {
            Int32 insertedrows = 0;
            using (OdbcDataAdapter adapter = new OdbcDataAdapter())
            {
                OdbcConnection conn = GetDb2Connection();
                OdbcTransaction trans;
                try
                {
                    conn.Open(); //make sure you are running visual studio as administrator
                    trans = conn.BeginTransaction(IsolationLevel.ReadCommitted);

                    adapter.InsertCommand = new OdbcCommand(QParameters.query, conn);
                    adapter.InsertCommand.Transaction = trans;
                    using (OdbcCommand command = new OdbcCommand(QParameters.query, conn))
                    {
                        foreach (QueryParam obj in QParameters.QueryParams)
                        {
                            if (string.IsNullOrEmpty(obj.value))
                            {
                                adapter.InsertCommand.Parameters.Add(obj.parameter, obj.type).Value = System.DBNull.Value;
                            }
                            else
                            {
                                adapter.InsertCommand.Parameters.Add(obj.parameter, obj.type).Value = obj.value;
                            }
                        }
                        insertedrows = adapter.InsertCommand.ExecuteNonQuery();
                        adapter.InsertCommand.Transaction.Commit();
                    }
                }
                catch (OdbcException ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    throw;
                }
                finally
                {
                    conn.Close();
                }

            }
            return insertedrows;
        }

    }

    public class QueryParam
    {
        public string parameter { get; set; }
        public string value { get; set; }
        public OdbcType type { get; set; }
    }

    public class QueryParameters
    {
        public string query { get; set; }
        public List<QueryParam> QueryParams { get; set; }
        public QueryParameters()
        {
            QueryParams = new List<QueryParam>();
        }
        public void AddParam(string inparam, string invalue, OdbcType intype)
        {
            QueryParams.Add(new QueryParam()
            {
                parameter = inparam,
                value = invalue,
                type = intype

            });
        }
    }
}