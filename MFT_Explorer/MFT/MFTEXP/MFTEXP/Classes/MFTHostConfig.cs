﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using MFTEXP.Models;
using MFTEXP.Classes;

namespace MFTEXP.Classes
{
    public class MFTHostConfig
    {
       
        /// <summary>
        /// This method used for to save the newly added or updated host details to dbase
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool  SaveHostConfig(HostConfig data)
        {
            bool ActionComplete = false;
            MFTHelper helper = new MFTHelper();
            QueryParameters inputParams = new QueryParameters();
            try
            {
                //inputParams.query = MFTQueries.Insert_Update_Host;
                if (!string.IsNullOrEmpty(data.Action))
                {

                    inputParams.AddParam("Host_Name", data.Host_Name.Trim(), OdbcType.VarChar);
                    inputParams.AddParam("Host_IP", data.Host_IP.Trim(), OdbcType.VarChar);
                    inputParams.AddParam("QMGR", data.Coord_Qmgr.Trim(), OdbcType.VarChar);
                    inputParams.AddParam("PORT", data.Coord_Port.ToString().Trim(), OdbcType.Int);
                    inputParams.AddParam("Agent_Qmgr", data.Agent_QMGR.Trim(), OdbcType.VarChar);
                    inputParams.AddParam("Agent_Port", data.Agent_QMGR_Port.ToString().Trim(), OdbcType.Int);
                    inputParams.AddParam("channel", data.Default_Channel.Trim(), OdbcType.VarChar);
                    inputParams.AddParam("userid", data.Userid.Trim(), OdbcType.VarChar);
                    inputParams.AddParam("status", data.Active.ToString().Trim(), OdbcType.Char);
                    if (data.Action == "Update")
                    {
                        inputParams.AddParam("Host_id", data.Host_ID.ToString().Trim(), OdbcType.Int);
                        inputParams.query = MFTQueries.Update_Host;
                        if (helper.UpdateCommand(inputParams) >0 )
                            ActionComplete = true ;
                        else 
                            ActionComplete = false ;
                    }
                    else
                    {
                        inputParams.query = MFTQueries.Insert_Host;
                        if (helper.InsertCommand(inputParams) > 0)
                        {
                            MFTMonitor mon = new MFTMonitor();
                            mon.DeleteMonitorsWithNewHost();
                            ActionComplete = true;
                        }
                        else
                        {
                            ActionComplete = false;
                        }
                    }
                }
            }
            catch(Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            return ActionComplete;
        }

        public bool DeleteHostConfig(string Hostid)
        {
            bool ActionComplete = false;
            MFTHelper helper = new MFTHelper();
            QueryParameters inputParams = new QueryParameters();
            inputParams.query = MFTQueries.Delete_Host;
            inputParams.AddParam("Host_id", Hostid.Trim(), OdbcType.Int);
            if (helper.UpdateCommand(inputParams) > 0)
                ActionComplete = true;
            else
                ActionComplete = false;
            return ActionComplete;
        }
       

    }
}