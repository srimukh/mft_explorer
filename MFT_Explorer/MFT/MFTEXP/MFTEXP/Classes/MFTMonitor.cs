﻿using IBM.WMQ;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using MFTEXP.Models;
using System.Transactions;
using MFTEXP.Classes;
using System.Data.Odbc;
using System.Data;
using System.Xml.Linq;

using System.Xml;
using System.Text;
using System.IO;

namespace MFTEXP.Classes
{
    public class MFTMonitor
    {
        MFTHelper helper = new MFTHelper();
        MFTCommon mftcommon = new MFTCommon();
        /// <summary>
        /// Inserts the monitor name to dbase which one is selected for permenanent deletion
        /// </summary>
        /// <param name="MonitorName"></param>
        /// <param name="environment"></param>
        /// <returns></returns>
        public Boolean InsertDeletedMonitorNames(string MonitorID)
        {
            List<HostConfig> hst = new List<HostConfig>();
            hst = mftcommon.GetHostdetails();
            HostConfig HstConfig = hst.FirstOrDefault();
            Boolean inserted = false;
            QueryParameters inputParams = new QueryParameters();
            inputParams.query = MFTQueries.InsertDeletedMonitor;
            inputParams.AddParam("Host_id",HstConfig.Host_ID.ToString(), OdbcType.Int);
            inputParams.AddParam("MonitorName", MonitorID.Trim(), OdbcType.VarChar);
            int insertedrows=helper.InsertCommand(inputParams);
            if (insertedrows > 0) inserted = true;
            return inserted;
        }
      
        /// <summary>
        /// This method gets the list of monitor names from mft tables(monitor,monitor_action) along with current status.
        /// </summary>
        /// <param name="environment"></param>
        /// <returns></returns>
       public List<Monitordetails> GetMonitors()
        {
            MFTCommon mftcommon = new MFTCommon();
            List<Agentdetails> Agents = new List<Agentdetails>();
            Agents = mftcommon.GetAgentsandQMGR();

            var mntr = new List<Monitordetails>();
            try
            {
            List<HostConfig> hst = new List<HostConfig>();
            hst = mftcommon.GetHostdetails();
            HostConfig HstConfig = hst.FirstOrDefault();
            QueryParameters inputParams = new QueryParameters();
            inputParams.query =MFTQueries.GetMonitorsFrmMFT;
           
            using (DataTable dl = helper.GetDataTable(inputParams))
            {
                if (dl.Rows.Count > 0)
                {
                    foreach (DataRow dr in dl.Rows)
                    {
                        Monitordetails monitor = new Monitordetails();
                        monitor.MonitorID = Convert.ToString(dr["Monitor_ID"]).Trim();
                        monitor.MonitorName = Convert.ToString(dr["MonitorName"]).Trim();
                        monitor.QueueManagerName = Convert.ToString(dr["QMGR"]).Trim();
                        monitor.AgentName = Convert.ToString(dr["Agent"]).Trim();
                        monitor.MonitorXml = FormatAsXML(Convert.ToString(dr["mxml"]).Trim());

                        //Perform_Monitor_Action(monitor.MonitorID, monitor.AgentName,monitor.QueueManagerName, monitor.AgentName,MFTCommon.Current_Action_Performed.Deleted );
                        //InsertDeletedMonitorNames(monitor.MonitorID);
                        //continue;

                        if (Convert.ToString(dr["Status"]).Trim() == "start")
                        {
                            monitor.MonitorStatus = "Running";
                        }
                        else if (Convert.ToString(dr["Status"]).Trim() == "delete")
                        {
                            monitor.MonitorStatus = "Paused";
                        }
                        else if (Convert.ToString(dr["Status"]).Trim() == "stop")
                        {
                            monitor.MonitorStatus = "Stopped";
                        }
                        else
                        {
                            monitor.MonitorStatus = Convert.ToString(dr["Status"]).Trim(); // "Deleted";
                        }
                        monitor.AgentStatus = "";
                        foreach (Agentdetails agt in Agents)
                        {
                            if (monitor.AgentName == agt.agentName && monitor.QueueManagerName == agt.queueManager)
                            {
                                monitor.AgentStatus = agt.AgentStatus;
                                break;
                            }
                        }

                        mntr.Add(monitor);
                    }

                    

                }
            }
       }
           catch(Exception Ex)
           {
               MFTErrorMessage.Error_Message = MFTConstants.Default_Database_Error_Message;
               Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);  

           }
            return mntr;
        }

       public bool DeleteMonitorsWithNewHost()
       {
           bool ActionComplete = false; 
           MFTCommon mftcommon = new MFTCommon();
           try
           {
               List<HostConfig> hst = new List<HostConfig>();
               hst = mftcommon.GetHostdetails();
               HostConfig HstConfig = hst.FirstOrDefault();
               QueryParameters inputParams = new QueryParameters();
               inputParams.query = MFTQueries.GetMonitorsFrmMFT;

               using (DataTable dl = helper.GetDataTable(inputParams))
               {
                   if (dl.Rows.Count > 0)
                   {
                       foreach (DataRow dr in dl.Rows)
                       {
                            if (Convert.ToString(dr["Status"]).Trim() == "delete")
                           {
                               this.InsertDeletedMonitorNames(Convert.ToString(dr["Monitor_ID"]).Trim() );
                           }
                       }
                       ActionComplete = true;
                   }
               }
           }
           catch (Exception Ex)
           {
               MFTErrorMessage.Error_Message = MFTConstants.Default_Database_Error_Message;
               Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);

           }
           return ActionComplete;
       }

       public string FormatAsXML(string xml)
       {
           XmlDocument xdoc = new XmlDocument();
           StringBuilder results = new StringBuilder();
           try
           {
               xdoc.LoadXml(xml);
               using (StringWriter strWriter = new StringWriter(results))
               {
                   using (XmlTextWriter xmlWriter = new XmlTextWriter(strWriter))
                   {
                       xmlWriter.Formatting = Formatting.Indented;
                       xdoc.WriteTo(xmlWriter);
                   }
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }
           return results.ToString();
       }


        /// <summary>
        /// This method performs the monitor action,pasue or start.
        /// </summary>
        /// <param name="MonitorName"></param>
        /// <param name="QueName"></param>
        /// <param name="AgentName"></param>
        /// <param name="Acttype"></param>
        /// <param name="environment"></param>
        /// <returns></returns>
        public bool Perform_Monitor_Action(string MonitorID, string MonitorName, string QueName, string AgentName, MFTCommon.Current_Action_Performed Acttype)
        {
            MFTErrorMessage.Error_Message = "";
            Boolean MonitorActionPerformed = false;
            if (Acttype == MFTCommon.Current_Action_Performed.Deleted)
            {
                MonitorActionPerformed = InsertDeletedMonitorNames(MonitorID);
            }
            else
            {
               MonitorActionPerformed = PutMessage(MonitorID, MonitorName, QueName, AgentName, Acttype);
            }
            if (MonitorActionPerformed)
            {
                if (Acttype == MFTCommon.Current_Action_Performed.Deleted)
                {
                    MonitorActionPerformed = InsertDeletedMonitorNames(MonitorID);
                }
                   //int milliseconds = 300;
                   //Thread.Sleep(milliseconds);
            }
            return MonitorActionPerformed;
        }

        /// <summary>
        /// This method connects to the mq server and place the message into agent queue
        /// </summary>
        /// <param name="MonitorID"></param>
        /// <param name="MonitorName"></param>
        /// <param name="QueName"></param>
        /// <param name="AgentName"></param>
        /// <param name="Atype"></param>
        /// <param name="environment"></param>
        /// <returns></returns>
        public Boolean PutMessage(string MonitorID, string MonitorName, string QMName, string AgentName, MFTCommon.Current_Action_Performed Atype)
        {

            MFTQueueManager mgrs = new MFTQueueManager();
            MQ_QueueeManager Manager = mgrs.GetAllQueueManagers().Where(x => x.QM_Name == QMName).FirstOrDefault();

            MQQueueManager MQMgr=null;
            MQQueue queue=null;
            Hashtable properties = new Hashtable();
            MQMessage message;
            MQPutMessageOptions putMessageOptions;
            String queueName = string.Empty;
            String messageString = string.Empty;
            Boolean Status = false;
            try
            {
                if (Manager != null)
                {
                    queueName = MFTConstants.Default_Queue_Command + "." + AgentName;

                    properties.Add(MQC.HOST_NAME_PROPERTY, Manager.Host_Name );
                    properties.Add(MQC.PORT_PROPERTY, Manager.QM_Port );
                    properties.Add(MQC.CHANNEL_PROPERTY, Manager.QM_Channel );
                    MQMgr = new MQQueueManager(Manager.QM_Name , properties);

                    queue = MQMgr.AccessQueue(queueName, MQC.MQOO_OUTPUT);
                    putMessageOptions = new MQPutMessageOptions();
                    putMessageOptions.Options = MQC.MQPMO_SYNCPOINT;
                    message = new MQMessage();

                    message.CharacterSet = 437;
                    if (Atype == MFTCommon.Current_Action_Performed.Started)
                    {
                        messageString = StartMonitor(MonitorID);
                         InsertDeletedMonitorNames(MonitorID);
                    }
                    else
                    {
                        messageString = DeleteMonitor(MonitorName, Manager.Host_Name , Manager.QM_UserID);
                    }

                    try
                    {
                        if (!string.IsNullOrEmpty(messageString))
                        {
                            message.WriteString(messageString);
                            //for transaction complete or rollback implicit
                            using (TransactionScope scope = new TransactionScope())
                            {
                                queue.Put(message, putMessageOptions);
                                scope.Complete();
                                Status = true;
                            }
                        }
                        else
                        {
                            Status = false;
                        }
                        MQMgr.Disconnect();
                        queue.Close();
                    }
                    catch (Exception Ex)
                    {
                        MFTErrorMessage.Error_Message = MFTConstants.Default_Error_Message;
                        Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                        Status = false;
                    }
                }
            }
            catch (MQException Mqex)
            {

                Elmah.ErrorSignal.FromCurrentContext().Raise(Mqex);  //This is for to handle "MQRC NO MSG AVAILABLE"
                MFTErrorMessage.Error_Message = MFTConstants.Default_MQ_Error_Message;

            }
            catch (Exception Ex)
            {
                MFTErrorMessage.Error_Message = MFTConstants.Default_Error_Message;
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            finally
            {
                if (MQMgr != null)
                {
                    MQMgr.Disconnect();
                }
               
            }
            return Status;
        }

        /// <summary>
        /// gets the delete monitor xml for the monitor to delete
        /// </summary>
        /// <param name="MonitorName"></param>
        /// <param name="hostname"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public String DeleteMonitor(string MonitorName, string hostname, string userid)
        {
            string deletmonitor = String.Format(MFTConstants.DeleteMonitor_XML, MonitorName, hostname, userid);
            return deletmonitor;
        }

        /// <summary>
        /// Gets the monitor xml from mft table for to start that monitor
        /// </summary>
        /// <param name="MonitorID"></param>
        /// <returns></returns>
        public string StartMonitor(string MonitorID)
        {
            string strtmntr = string.Empty;
            try
            {
                using (DataTable dt = mftcommon.GetMonitorXML(MonitorID))
                {
                    if (dt.Rows.Count > 0)
                    {
                        strtmntr = (Convert.ToString(dt.Rows[0]["MO_XML"]).Trim());
                    }
                }
            }
            catch(Exception Ex)
            {
                throw Ex;
            }
            return strtmntr;
        }
       

    }
}
