﻿using IBM.WMQ;
using IBM.WMQ.PCF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using MFTEXP.Models;

namespace MFTEXP.Classes
{
    public class MFTMonitorCreation
    {
        MFTHelper helper = new MFTHelper();
        public string hostName { get; set; }
        public string userID { get; set; }
        public int port { get; set; }
        public string channelName { get; set; }

        public string userPwd { get; set; }


        /// <summary>
        /// Creates a new xml by calling few static methods with the values provided in the screen and places that xml in the corresponding agent queue to gets monitor created.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="reqtype"></param>
        /// <returns></returns>
        public Boolean CreateXML(Transfer_details data, MFTCommon.RequestType reqtype)
        {
            string xmlstring = string.Empty;
            XmlDocument MonitorDoc = new XmlDocument();
            Boolean strstatus = false;
            try
            {
                if (reqtype == MFTCommon.RequestType.Monitor)
                {
                    string MainNode = "monitor";
                    MonitorDoc = BuildMFTMonitorHeader("monitor", "6.00", "http://www.ibm.com/xmlns/wmqfte/7.0.1/MonitorDefinition ./Monitor.xsd", "true");
                    BuildMonitorName(MonitorDoc, MainNode, data.Monitor_Name);
                    BuildPollinterval(MonitorDoc, MainNode, data.Poll_Frequency, data.Poll_Interval);
                    AddAgent(MonitorDoc, MainNode, data.Src_Agent);
                    AddResources(MonitorDoc, MainNode, data);
                    Addtriggermatch(MonitorDoc, MainNode, data);
                    AddReplyQueue(MonitorDoc, MainNode, data);
                    AddTasks(MonitorDoc, MainNode, data);
                    XmlNode rootnode = MonitorDoc.SelectSingleNode("monitor");
                    BuildOriginator(MonitorDoc, rootnode, this.hostName, this.userID);
                    
                }
                else
                {
                    MonitorDoc = BuildTransferTemplate(data);                    

                }
                xmlstring = MonitorDoc.OuterXml.ToString();

                xmlstring = xmlstring.Replace("<monitor", "<?xml version=\"1.0\" encoding=\"UTF-8\"?><monitor:monitor");
                xmlstring = xmlstring.Replace("</monitor>", "</monitor:monitor>");
                xmlstring = xmlstring.Replace("schemaLocation=", "xsi:schemaLocation=");
                xmlstring = xmlstring.Replace("noNamespaceSchemaLocation", "xsi:noNamespaceSchemaLocation");
                MFTCommon mftcommon = new MFTCommon();
                Boolean submitstatus = mftcommon.PutMessage(data.Src_Agent.Trim(), xmlstring, 
                    data.Src_QueMgr, data.Src_HostName, data.Src_HostIP, data.Src_Channel, data.Src_Port, data.Src_UserID );
                if (submitstatus) { strstatus = true; }


            }
            catch (Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            finally
            {
                MonitorDoc = null;
            }
            return strstatus;
        }

        public static void BuildMonitorName(XmlDocument document, string mainroot, string monitorname)
        {
            XmlNode rootnode = document.SelectSingleNode(mainroot);
            XmlNode NodeMonitorName = document.CreateNode(XmlNodeType.Element, "name", null);
            NodeMonitorName.InnerText = monitorname;
            rootnode.AppendChild(NodeMonitorName);

        }

        public static void BuildPollinterval(XmlDocument document, string mainroot, string pollfreq, int pollintvl)
        {
            XmlNode rootnode = document.SelectSingleNode(mainroot);
            XmlElement PollElement = document.CreateElement("pollInterval");
            PollElement.SetAttribute("units", pollfreq);
            PollElement.InnerText = Convert.ToString(pollintvl);
            rootnode.AppendChild(PollElement);

        }

        public static void AddAgent(XmlDocument document, string mainroot, string SAgent)
        {
            XmlNode rootnode = document.SelectSingleNode(mainroot);
            XmlElement AgtElement = document.CreateElement("agent");
            AgtElement.InnerText = SAgent;
            rootnode.AppendChild(AgtElement);

        }

        public static void AddResources(XmlDocument document, string mainroot, Transfer_details tdata)
        {
            XmlNode rootnode = document.SelectSingleNode(mainroot);
            XmlNode resourceNode = document.CreateNode(XmlNodeType.Element, "resources", null);
            rootnode.AppendChild(resourceNode);
            string strresourcetype = tdata.resource_type.ToLower();
            XmlElement resourcetypeElement = document.CreateElement(strresourcetype);
            if (strresourcetype == "directory")
            {
                resourcetypeElement.SetAttribute("recursionLevel", "0");
            }
            resourcetypeElement.InnerText = tdata.resource_info;
            resourceNode.AppendChild(resourcetypeElement);

        }

        public static void Addtriggermatch(XmlDocument document, string mainroot, Transfer_details tdata)
        {
            XmlNode rootnode = document.SelectSingleNode(mainroot);
            XmlNode trgNode = document.CreateNode(XmlNodeType.Element, "triggerMatch", null);
            rootnode.AppendChild(trgNode);
            XmlNode cdtionsNode = document.CreateNode(XmlNodeType.Element, "conditions", null);
            trgNode.AppendChild(cdtionsNode);
            XmlNode allofNode = document.CreateNode(XmlNodeType.Element, "allOf", null);
            cdtionsNode.AppendChild(allofNode);
            XmlNode cdtionNode = document.CreateNode(XmlNodeType.Element, "condition", null);
            allofNode.AppendChild(cdtionNode);

            if (tdata.resource_type.ToLower() == "queue")
            {
                XmlNode trgqueNode = document.CreateNode(XmlNodeType.Element, tdata.Trg_condition, null);
                cdtionNode.AppendChild(trgqueNode);
            }
            else
            {
                XmlNode trgfNode = document.CreateNode(XmlNodeType.Element, tdata.Trg_condition, null);
                if (tdata.Trg_condition == "fileSizeSame")
                {

                    XmlAttribute attr = document.CreateAttribute("polls");
                    attr.Value = tdata.polls;
                    trgfNode.Attributes.SetNamedItem(attr);


                }
                cdtionNode.AppendChild(trgfNode);

                if (tdata.Trg_condition == "fileSize")
                {
                    XmlElement fsElement = document.CreateElement("compare");
                    fsElement.SetAttribute("operator", "&gt;=");
                    fsElement.SetAttribute("units", tdata.filesizeb);
                    fsElement.InnerText = tdata.polls;
                    trgfNode.AppendChild(fsElement);

                }

                if (!string.IsNullOrEmpty(tdata.M_Pattern))
                {
                    XmlElement MpElement = document.CreateElement("pattern");
                    MpElement.InnerText = tdata.M_Pattern;
                    trgfNode.AppendChild(MpElement);
                }
                if (!string.IsNullOrEmpty(tdata.E_Pattern))
                {
                    XmlElement EpElement = document.CreateElement("exclude");
                    EpElement.InnerText = tdata.E_Pattern;
                    trgfNode.AppendChild(EpElement);
                }

            }



        }

        public static void AddReplyQueue(XmlDocument document, string mainroot, Transfer_details tdata)
        {
            if (!string.IsNullOrEmpty(tdata.reply_Queue))
            {
                XmlNode rootnode = document.SelectSingleNode(mainroot);
                XmlNode replyNode = document.CreateNode(XmlNodeType.Element, "reply", null);
                XmlAttribute attr = document.CreateAttribute("QMGR");
                attr.Value = tdata.Src_QueMgr;
                replyNode.Attributes.SetNamedItem(attr);
                replyNode.InnerText = tdata.reply_Queue;
                rootnode.AppendChild(replyNode);
            }
        }


        public void AddTasks(XmlDocument document, string mainroot, Transfer_details tdata)
        {
            XmlNode rootnode = document.SelectSingleNode(mainroot);
            XmlNode tsksNode = document.CreateNode(XmlNodeType.Element, "tasks", null);
            rootnode.AppendChild(tsksNode);
            XmlNode tskNode = document.CreateNode(XmlNodeType.Element, "task", null);
            tsksNode.AppendChild(tskNode);
            XmlNode nameNode = document.CreateNode(XmlNodeType.Element, "name", null);
            tskNode.AppendChild(nameNode);
            XmlNode transferNode = document.CreateNode(XmlNodeType.Element, "transfer", null);
            tskNode.AppendChild(transferNode);
            XmlDocument document1 = BuildTransferTemplate(tdata);
            XmlNode rnode = document1.SelectSingleNode("request");
            XmlNode xnode = document.ImportNode(rnode, true);
            transferNode.AppendChild(xnode);
        }

        public XmlDocument BuildTransferTemplate(Transfer_details bdata)
        {
            XmlDocument TemplateDoc = new XmlDocument();
            GetHostinformation();
            TemplateDoc = BuildMFTRequestHeader("request", "6.00", "FileTransfer.xsd");
            TemplateDoc = BuildManagedTransfer(TemplateDoc, bdata);
            return TemplateDoc;
        }

        public static XmlDocument BuildMFTRequestHeader(string requesttype, string version, string xsd)
        {
            XmlDocument document = new XmlDocument();
            XmlElement Firstelement = document.CreateElement(requesttype);
            Firstelement.SetAttribute("version", version);
            Firstelement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            Firstelement.SetAttribute("xsi:noNamespaceSchemaLocation", xsd);
            document.AppendChild(Firstelement);
            return document;
        }

        public static XmlDocument BuildMFTMonitorHeader(string monitortype, string version, string xsd, string overwrite)
        {
            XmlDocument document = new XmlDocument();
            XmlElement Firstelement = document.CreateElement(monitortype);
            Firstelement.SetAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            Firstelement.SetAttribute("version", version);
            Firstelement.SetAttribute("xmlns:monitor", "http://www.ibm.com/xmlns/wmqfte/7.0.1/MonitorDefinition");
            Firstelement.SetAttribute("xsi:schemaLocation", xsd);
            Firstelement.SetAttribute("overwrite", overwrite);
            document.AppendChild(Firstelement);
            return document;
        }

        public XmlDocument BuildManagedTransfer(XmlDocument document, Transfer_details tdata)
        {
           
            XmlNode rootnode = document.SelectSingleNode("request");
            XmlNode requestNode = document.CreateNode(XmlNodeType.Element, "managedTransfer", null);
            rootnode.AppendChild(requestNode);
            BuildOriginator(document, requestNode, this.hostName, this.userID);
            if (tdata.Schedule != null)
            {
                BuildSchedule(document, requestNode, tdata.Schedule);
            }
            AddAgent(document, requestNode, tdata.Src_Agent, tdata.Src_QueMgr, "sourceAgent");
            AddAgent(document, requestNode, tdata.Trgt_Agent, tdata.Trgt_QueMgr, "destinationAgent");
            AddTransferSet(document, requestNode, tdata);
            XmlElement jbelement = document.CreateElement("job");
            requestNode.AppendChild(jbelement);
            XmlElement jbnameelement = document.CreateElement("name");
            jbnameelement.InnerText = tdata.Job_name.Trim();
            jbelement.AppendChild(jbnameelement);
            
            return document;

        }

        public static void BuildOriginator(XmlDocument document, XmlNode OriginatorNode, string hname, string usid)
        {


            XmlNode NodeOriginator = document.CreateNode(XmlNodeType.Element, "originator", null);
            OriginatorNode.AppendChild(NodeOriginator);
            XmlNode NodeNewHost = document.CreateNode(XmlNodeType.Element, "hostName", null);
            NodeNewHost.InnerText = hname;
            XmlNode NodeNewUser = document.CreateNode(XmlNodeType.Element, "userID", null);
            NodeNewUser.InnerText = usid;
            NodeOriginator.AppendChild(NodeNewHost);
            NodeOriginator.AppendChild(NodeNewUser);

            //return document;
        }


        public static void BuildSchedule(XmlDocument document, XmlNode OriginatorNode, Schedule_details schedule)
        {
            XmlNode NodeSchedule = document.CreateNode(XmlNodeType.Element, "schedule", null);
            OriginatorNode.AppendChild(NodeSchedule);
            
            XmlElement emementSubmit = document.CreateElement("submit", null);
            emementSubmit.SetAttribute("timebase",schedule.Timebase );
            //emementSubmit.SetAttribute("timezone", "America/New_York");
            //emementSubmit.SetAttribute("timezone", "Asia/Calcutta");
            emementSubmit.SetAttribute("timezone", MFTCommon.GetDefaultTimeZoneName());
            emementSubmit.InnerText = schedule.Starttime;
            NodeSchedule.AppendChild(emementSubmit);


            XmlNode nodeRepeat = document.CreateElement("repeat", null);
            XmlElement emementFrequency = document.CreateElement("frequency", null);
            emementFrequency.SetAttribute("interval", schedule.Frequency );            
            emementFrequency.InnerText = schedule.Interval;
            nodeRepeat.AppendChild(emementFrequency);

            if (!string.IsNullOrEmpty(schedule.Endtime))
            {
                XmlElement emementExpireTime = document.CreateElement("expireTime", null);
                emementExpireTime.InnerText = schedule.Endtime;
                nodeRepeat.AppendChild(emementExpireTime);
            }
            NodeSchedule.AppendChild(nodeRepeat);

            //XmlElement emementFNext = document.CreateElement("next", null);
            //emementFNext.InnerText = schedule.Starttime;
            //NodeSchedule.AppendChild(emementFNext);
 
            //return document;
        }

        public static void AddAgent(XmlDocument document, XmlNode requestNode, string agentData, string QMgrData, string nodeName)
        {
            XmlElement Agentelement = document.CreateElement(nodeName);
            Agentelement.SetAttribute("agent", agentData);
            Agentelement.SetAttribute("QMgr", QMgrData);
            requestNode.AppendChild(Agentelement);

        }

        public static void AddTransferSet(XmlDocument document, XmlNode requestNode, Transfer_details tdata)
        {
            XmlElement TransferSetelement = document.CreateElement("transferSet");
            if (!string.IsNullOrEmpty(tdata.Trans_priority))
            {
                TransferSetelement.SetAttribute("priority", tdata.Trans_priority);
            }
            requestNode.AppendChild(TransferSetelement);
            if (!string.IsNullOrEmpty(tdata.Pre_Src) || !string.IsNullOrEmpty(tdata.Post_Src) || !string.IsNullOrEmpty(tdata.Pre_dest) || !string.IsNullOrEmpty(tdata.Post_dest))
            {
                XmlNode metaNode = document.CreateNode(XmlNodeType.Element, "metaDataSet", null);
                TransferSetelement.AppendChild(metaNode);
                if (!string.IsNullOrEmpty(tdata.Pre_Src))
                {
                    builduserexits(document, metaNode, tdata.Pre_Src, "PreSrc");
                }
                if (!string.IsNullOrEmpty(tdata.Post_Src))
                {
                    builduserexits(document, metaNode, tdata.Post_Src, "PostSrc");
                }
                if (!string.IsNullOrEmpty(tdata.Pre_dest))
                {
                    builduserexits(document, metaNode, tdata.Pre_dest, "PreDst");
                }
                if (!string.IsNullOrEmpty(tdata.Post_dest))
                {
                    builduserexits(document, metaNode, tdata.Post_dest, "PostDst");
                }
            }
            XmlElement itemelement = document.CreateElement("item");
            itemelement.SetAttribute("checksumMethod", tdata.Checksum);
            itemelement.SetAttribute("mode", tdata.Trans_Mode);


            TransferSetelement.AppendChild(itemelement);
            XmlElement srcelement = document.CreateElement("source");
            if (string.IsNullOrEmpty(tdata.Src_disposition))
            {
                srcelement.SetAttribute("disposition", "leave");
            }
            else
            {
                srcelement.SetAttribute("disposition", tdata.Src_disposition);
            }

            srcelement.SetAttribute("recursive", "false");
            if (tdata.resource_type.ToLower() == "queue") { srcelement.SetAttribute("type", "queue"); }

            itemelement.AppendChild(srcelement);

            if (tdata.resource_type.ToLower() == "queue")
            {
                XmlElement qelement = document.CreateElement("queue");
                qelement.SetAttribute("useGroups", "false");
                if (tdata.Action.Trim() == "transfer")
                {
                    qelement.InnerText = tdata.resource_info + "@" + tdata.Trgt_QueMgr;
                }
                else
                {
                    qelement.InnerText = "${QueueName}@" + tdata.Trgt_QueMgr;
                }
                srcelement.AppendChild(qelement);
            }
            else
            {
                XmlElement felement = document.CreateElement("file");
                felement.InnerText = tdata.Src_file;
                srcelement.AppendChild(felement);

            }

            XmlElement trgelement = document.CreateElement("destination");

            if (tdata.Dest_type.ToLower() == "queue")
            {
                trgelement.SetAttribute("type", tdata.Dest_type.ToLower());
                XmlElement telement = document.CreateElement("queue");
                telement.SetAttribute("persistent", "true");
                telement.SetAttribute("setMqProps", "false");
                telement.InnerText = tdata.Trgt_file + "@" + tdata.Trgt_QueMgr;
                trgelement.AppendChild(telement);
            }
            else
            {
                if (string.IsNullOrEmpty(tdata.Trgt_exists))
                {
                    trgelement.SetAttribute("exist", "error");
                }
                else
                {
                    trgelement.SetAttribute("exist", tdata.Trgt_exists);
                }
                trgelement.SetAttribute("type", tdata.Dest_type.ToLower());
                itemelement.AppendChild(trgelement);

                XmlElement telement = document.CreateElement("file");
                telement.InnerText = tdata.Trgt_file;
                trgelement.AppendChild(telement);
            }

        }

        public static void builduserexits(XmlDocument document, XmlNode xnode, string minnertext, string mvalue)
        {

            XmlNode presrcNode = document.CreateNode(XmlNodeType.Element, "metaData", null);
            XmlAttribute attr = document.CreateAttribute("key");
            attr.Value = mvalue;
            presrcNode.Attributes.SetNamedItem(attr);
            presrcNode.InnerText = minnertext;
            xnode.AppendChild(presrcNode);

        }

       
        public void GetHostinformation()
        {
            MFTCommon mftcommon = new MFTCommon();
            List<HostConfig> hst = new List<HostConfig>();
            hst = mftcommon.GetHostdetails();
            HostConfig HstConfig = hst.FirstOrDefault();
            
            this.hostName = HstConfig.Host_IP;
            this.port = HstConfig.Agent_QMGR_Port;
            this.channelName = HstConfig.Default_Channel;
            this.userID = HstConfig.Userid;
            

        }

      

        /// <summary>
        /// This method gets all of the queue names from the MQ server for user to select the reply queue while creating a monitor.
        /// </summary>
        /// <param name="QueueMgr"></param>
        /// <param name="environment"></param>
        /// <returns></returns>
        public List<MQ_Queues> GetAllQueueNames(string QueueMgr,string environment)
        {

            List<MQ_Queues> lstqueues = new List<MQ_Queues>();
            List<MQ_Queues> Custom_Queues = null;
            MQQueueManager MQMgr = null;
            Hashtable properties = new Hashtable();
            MFTCommon mftcommon = new MFTCommon();
            List<HostConfig> hst = new List<HostConfig>();
            hst = mftcommon.GetHostdetails();
            HostConfig HstConfig = hst.FirstOrDefault();
            //MQEnvironment.UserId = HstConfig.Userid;
            properties.Add(MQC.HOST_NAME_PROPERTY, HstConfig.Host_Name);
            properties.Add(MQC.PORT_PROPERTY, HstConfig.Coord_Port);
            properties.Add(MQC.CHANNEL_PROPERTY, HstConfig.Default_Channel);
            try
            {
                MQMgr = new MQQueueManager(HstConfig.Coord_Qmgr, properties);

                //MQEnvironment.UserId = HstConfig.Userid;
            PCFMessageAgent agent = new PCFMessageAgent(MQMgr);
            PCFMessage reqeuestMessage = new PCFMessage(CMQCFC.MQCMD_INQUIRE_Q_NAMES);
            reqeuestMessage.AddParameter(MQC.MQCA_Q_NAME, "*");
            reqeuestMessage.AddParameter(MQC.MQIA_Q_TYPE, MQC.MQQT_LOCAL);

            PCFMessage[] response;

            response = agent.Send(reqeuestMessage);
            string[] queueNames = response[0].GetStringListParameterValue(CMQCFC.MQCACF_Q_NAMES);

            for (int i = 0; i < queueNames.Length; i++)
            {
                MQ_Queues sysQue = new MQ_Queues();
                sysQue.Queues = queueNames[i].Trim();
                lstqueues.Add(sysQue);
            }
                Custom_Queues = lstqueues
                                        .Where(i => !i.Queues.StartsWith("SYSTEM", StringComparison.InvariantCultureIgnoreCase))
                                        .ToList();

                MQMgr.Close();
                MQMgr.Disconnect();
            }
            catch (Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            finally
                {
                    if (MQMgr != null) MQMgr.Disconnect();
                }
            
            return Custom_Queues;


        }

        public Transfer_details GetMonitorValues(string MonitorID)
        {
            MFTCommon mcommon = new MFTCommon();
            Transfer_details tftdetails = new Transfer_details();
            string strxml = string.Empty;
            using (DataTable dt = mcommon.GetMonitorXML(MonitorID))
            {
                if (dt.Rows.Count > 0)
                {
                    strxml=Convert.ToString(dt.Rows[0]["MO_XML"]).Trim();
                }
            }
            try
            {
                if (!string.IsNullOrEmpty(strxml))
                {
                    var XmlMsg = XDocument.Parse(strxml);
                    XmlDocument mDocument = new XmlDocument();
                    XmlNode mCurrentNode;
                    mDocument.LoadXml(XmlMsg.ToString());
                    mCurrentNode = mDocument.DocumentElement;
                    XmlNodeList nodeList = mCurrentNode.SelectNodes("*");
                    DisplayList(nodeList, tftdetails);
                    tftdetails.Action = "Edit";
                }
            }
            catch (Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            return tftdetails;
        }


        /// <summary>
        /// This method will extract the values from Monitor xml and assigns the values to the transfer_details properties for to replay the failure.
        /// </summary>
        /// <param name="Monitor_Xml"></param>
        /// <returns></returns>
        public Transfer_details GetMonitorValuesForReplay(string Monitor_Xml)
        {
           
            Transfer_details tftdetails = new Transfer_details();
            string strxml = Monitor_Xml;
            try
            {
                if (!string.IsNullOrEmpty(strxml))
                {
                    var XmlMsg = XDocument.Parse(strxml);
                    XmlDocument mDocument = new XmlDocument();
                    XmlNode mCurrentNode;
                    mDocument.LoadXml(XmlMsg.ToString());
                    mCurrentNode = mDocument.DocumentElement;
                    XmlNodeList nodeList = mCurrentNode.SelectNodes("*");
                    DisplayList(nodeList, tftdetails);
                   
                }
            }
            catch (Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            return tftdetails;
        }


        static void DisplayList(XmlNodeList nodeList, Transfer_details tdetails)
        {
            try
            {

                tdetails.Monitor_Name = nodeList[0].InnerText.Trim().ToUpper();
                tdetails.Poll_Interval = Convert.ToInt16(nodeList[1].InnerText.Trim());
                tdetails.Poll_Frequency = nodeList[1].Attributes[0].Value.Trim();
                tdetails.Src_Agent = nodeList[2].InnerText.Trim();
                tdetails.resource_type = nodeList[3].LastChild.Name.Trim();
                tdetails.resource_info = nodeList[3].InnerText.Trim();


                if (tdetails.resource_type == "directory")
                {
                    tdetails.Trg_condition = nodeList[4].LastChild.LastChild.FirstChild.LastChild.Name.Trim();
                    XmlNode trgnode = nodeList[4].LastChild.LastChild.FirstChild.LastChild;
                    if (tdetails.Trg_condition == "fileSizeSame")
                    {
                        tdetails.polls = nodeList[4].LastChild.LastChild.FirstChild.LastChild.Attributes["polls"].Value.Trim();

                    }
                    if (tdetails.Trg_condition == "fileSize")
                    {
                        tdetails.filesizeb = trgnode.SelectNodes("compare")[0].Attributes["units"].Value.Trim();
                        tdetails.polls = trgnode.SelectNodes("compare")[0].InnerText.Trim();
                    }
                    tdetails.M_Pattern = trgnode.SelectNodes("pattern")[0].InnerText.Trim();
                    int excludeexists = trgnode.SelectNodes("exclude").Count;
                    if (excludeexists > 0) tdetails.E_Pattern = trgnode.SelectNodes("exclude")[0].InnerText.Trim();
                }
                else
                {
                    tdetails.Trg_condition = nodeList[4].LastChild.LastChild.FirstChild.LastChild.Name.Trim();
                }
                int tnode = 0;
                if (nodeList[5].Name == "reply")
                {
                    tdetails.reply_Queue = nodeList[5].InnerText.Trim();
                    tnode = 6;
                }
                else if (nodeList[5].Name == "tasks")
                {
                    tnode = 5;
                }
                XmlNode tasknode = nodeList[tnode].LastChild.LastChild.LastChild.LastChild;
                tdetails.Src_QueMgr = tasknode.SelectNodes("sourceAgent")[0].Attributes["QMgr"].Value.Trim();
                tdetails.Trgt_QueMgr = tasknode.SelectNodes("destinationAgent")[0].Attributes["QMgr"].Value.Trim();
                tdetails.Trgt_Agent = tasknode.SelectNodes("destinationAgent")[0].Attributes["agent"].Value.Trim();
                if (tasknode.SelectNodes("transferSet")[0].Attributes["priority"] != null) tdetails.Trans_priority = tasknode.SelectNodes("transferSet")[0].Attributes["priority"].Value.Trim();
                if (tasknode.SelectNodes("job").Count > 0) tdetails.Job_name = tasknode.SelectNodes("job")[0].InnerText.Trim();
               // tdetails.Trans_priority = tasknode.SelectNodes("transferSet")[0].Attributes["priority"].Value.Trim();


                foreach (XmlNode node in nodeList[tnode].LastChild.LastChild.LastChild.LastChild.SelectNodes("transferSet")[0].SelectNodes("metaDataSet"))
                {
                    RecurseXmlDocumentNoSiblings(node, tdetails);
                }

                XmlNode itemnode = tasknode.SelectNodes("transferSet")[0].SelectNodes("item")[0];
                tdetails.Checksum = itemnode.Attributes["checksumMethod"].Value.Trim();
                tdetails.Trans_Mode = itemnode.Attributes["mode"].Value.Trim();
                if (tdetails.resource_type != "queue") tdetails.Src_file = itemnode.SelectNodes("source")[0].InnerText.Trim();
                if (itemnode.SelectNodes("source")[0].Attributes["type"] != null)
                {
                    tdetails.Source_type = itemnode.SelectNodes("source")[0].Attributes["type"].Value.Trim();
                    tdetails.Source_type = tdetails.Source_type.First().ToString().ToUpper() + tdetails.Source_type.Substring(1);
                }
                else
                {
                    tdetails.Source_type = "File";
                }
                if (itemnode.SelectNodes("source")[0].Attributes["disposition"] != null) tdetails.Src_disposition = itemnode.SelectNodes("source")[0].Attributes["disposition"].Value.Trim();
                if (itemnode.SelectNodes("destination")[0].Attributes["exist"] != null) tdetails.Trgt_exists = itemnode.SelectNodes("destination")[0].Attributes["exist"].Value.Trim();
                tdetails.Dest_type = itemnode.SelectNodes("destination")[0].Attributes["type"].Value.Trim();
                tdetails.Dest_type = tdetails.Dest_type.First().ToString().ToUpper() + tdetails.Dest_type.Substring(1);
                tdetails.Trgt_file = itemnode.SelectNodes("destination")[0].InnerText.Trim();
            }

            catch (Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                throw;
            }
        }

        static void RecurseXmlDocumentNoSiblings(XmlNode root, Transfer_details tdetails)
        {
            if (root is XmlElement)
            {
                if (root.HasChildNodes)
                    RecurseXmlDocument(root.FirstChild, tdetails);
            }



        }
        static void RecurseXmlDocument(XmlNode root, Transfer_details tdetails)
        {
            string strattribute = string.Empty;
            if (root is XmlElement)
            {
                if (root.HasChildNodes)
                    strattribute = root.Attributes[0].Value.Trim();
                switch (strattribute)
                {
                    case "PreSrc":
                        tdetails.Pre_Src = root.FirstChild.Value.Trim();
                        break;
                    case "PostSrc":
                        tdetails.Post_Src = root.FirstChild.Value.Trim();
                        break;
                    case "PreDst":
                        tdetails.Pre_dest = root.FirstChild.Value.Trim();
                        break;
                    case "PostDst":
                        tdetails.Post_dest = root.FirstChild.Value.Trim();
                        break;
                }

                if (root.NextSibling != null)
                    RecurseXmlDocument(root.NextSibling, tdetails);
            }


        }

    }
}