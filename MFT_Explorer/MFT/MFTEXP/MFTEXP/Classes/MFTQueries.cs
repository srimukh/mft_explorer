﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MFTEXP.Classes
{
    public class MFTQueries
    {
        
           public const String Transfer_Log1Old = @"select job_name as JobName, transfer.transfer_id as TransferId, transfer.resultcode as OverallResult,
                                                        transfer_start.SOURCE_AGENT as SourceSystem, transfer_start.DESTINATION_AGENT as TargetSystem,
                                                        transfer.resulttext as OverallResultText, transfer_start.action_time as StartTime, 
                                                        transfer_end.action_time as EndTime, 
                                                        transfer_item.source_filename as SourceFile, coalesce(transfer_item.SOURCE_FILE_SIZE,0) as SourceFileSize, 
                                                        coalesce(transfer_item.DESTINATION_FILE_SIZE,0) as DestinationfileSize, coalesce(transfer_start.TRANSFERSET_SIZE,0) as Numberoffiles,
                                                        destination_filename as DestinationFile, transfer_item.resultcode as FileResult, 
                                                        coalesce(transfer_item.result_text,'') as FileResultMessage,transfer.status as Status 
                                                        from transfer transfer,transfer_event transfer_start, transfer_event transfer_end, 
                                                        transfer_item transfer_item where transfer_start.id = transfer.start_id and transfer_end.id = transfer.complete_id and 
                                                        transfer_item.transfer_id = transfer.transfer_id and transfer_start.action_time>=? and transfer_end.action_time <=? order by transfer_start.action_time desc with ur";
           
        public const String Transfer_Log = @"SELECT TRANSFER.JOB_NAME AS JOBNAME,TRANSFER.TRANSFER_ID AS TRANSFERID ,TRANSFER.RESULTCODE AS OVERALLRESULT,TRANSFER_START.SOURCE_AGENT AS SOURCESYSTEM,
                                                        TRANSFER_START.DESTINATION_AGENT AS TARGETSYSTEM,TRANSFER.RESULTTEXT AS OVERALLRESULTTEXT,TRANSFER_START.ACTION_TIME AS STARTTIME,
                                                        TRANSFER_END.ACTION_TIME AS ENDTIME,COALESCE(TRANSFER_ITEM.SOURCE_FILENAME,'') AS SOURCEFILE,
                                                        COALESCE(TRANSFER_ITEM.SOURCE_FILE_SIZE, 0) AS SOURCEFILESIZE,
                                                        COALESCE(TRANSFER_ITEM.DESTINATION_FILE_SIZE, 0) AS DESTINATIONFILESIZE,
                                                        COALESCE(TRANSFER_START.TRANSFERSET_SIZE, 0) AS NUMBEROFFILES,
                                                        COALESCE(transfer_item.DESTINATION_FILENAME, '') AS DESTINATIONFILE,
                                                        TRANSFER_ITEM.RESULTCODE AS FILERESULT,
                                                        COALESCE(TRANSFER_ITEM.RESULT_TEXT, '') AS FILERESULTMESSAGE,
                                                        TRANSFER.STATUS AS STATUS
                                                            FROM TRANSFER TRANSFER 
                                                                    inner join TRANSFER_EVENT TRANSFER_START
                                                                             on TRANSFER_START.ID = TRANSFER.START_ID
                                                            and TRANSFER_START.ACTION_TIME >= ? 
                                                            inner join TRANSFER_EVENT TRANSFER_END
                                                    on TRANSFER_END.ID = TRANSFER.COMPLETE_ID 
                                                            AND TRANSFER_END.ACTION_TIME <= ? 
                                                     inner join TRANSFER_ITEM TRANSFER_ITEM
                                                   on TRANSFER_ITEM.TRANSFER_ID = TRANSFER.TRANSFER_ID
                                                     where TRANSFER.JOB_NAME LIKE ? 
                                               ORDER BY TRANSFER_START.ACTION_TIME DESC
                                            WITH UR;";

//        public const String GetMonitorsFrmMFT = @"select p.id as ID,p.action as Status,os.name as MonitorName,os.agent as Agent,os.qmgr as QMGR from 
//                                                        (select max(ss.id) as id,ss.name,ss.agent,ss.qmgr from (select MA.ID,MA.Action,M.ID as Monitor_ID,M.Name,M.Agent,M.QMGR
//                                                        from monitor_action ma inner join monitor m on ma.monitor=m.id order by MA.ID)ss group by ss.name,ss.agent,ss.qmgr) as os
//                                                         inner join monitor_action P on p.id=os.id order by p.id asc with ur;";
//        public const String GetMonitorsFrmMFT = @"select p.id as ID,p.action as Status,os.name as MonitorName,os.agent as Agent, os.qmgr as QMGR,os.Monitor_ID from 
//                                                        (select max(ss.id) as id,ss.Monitor_ID, ss.name,ss.agent,ss.qmgr from (select MA.ID,MA.Action,M.ID as Monitor_ID, M.Name,M.Agent,M.QMGR
//                                                        from monitor_action ma inner join monitor m on ma.monitor=m.id order by MA.ID )ss group by ss.Monitor_ID, ss.name,ss.agent,ss.qmgr) as os
//                                                         inner join monitor_action P on p.id=os.id 
//where os.Monitor_id not in ( select Monitor_Name from   MQMFT_MONITORSTATUS)  order by p.id asc with ur;";
//        public const String GetMonitorsFrmMFT = @"select p.id as ID,p.action as Status,os.name as MonitorName,os.agent as Agent, os.qmgr as QMGR,os.Monitor_ID from 
//                                                        (select max(ss.id) as id,ss.Monitor_ID, ss.name,ss.agent,ss.qmgr from (select MA.ID,MA.Action,M.ID as Monitor_ID, M.Name,M.Agent,M.QMGR
//                                                        from monitor_action ma inner join monitor m on ma.monitor=m.id order by MA.ID )ss group by ss.Monitor_ID, ss.name,ss.agent,ss.qmgr) as os
//                                                         inner join monitor_action P on p.id=os.id 
//                                                         where os.monitor_id not in ( select Monitor_Name from   MQMFT_MONITORSTATUS)  
//                                                         order by p.id asc with ur;";

        public const String GetMonitorsFrmMFT = @"select p.id as ID,p.action as Status,os.name as MonitorName,os.agent as Agent, os.qmgr as QMGR,os.Monitor_ID, mx.original_xml_request as mxml from 
                                                        (select max(ss.id) as id,ss.Monitor_ID, ss.name,ss.agent,ss.qmgr from (select MA.ID,MA.Action,M.ID as Monitor_ID, M.Name,M.Agent,M.QMGR
                                                        from monitor_action ma inner join monitor m on ma.monitor=m.id order by MA.ID )ss group by ss.Monitor_ID, ss.name,ss.agent,ss.qmgr) as os
                                                         inner join monitor_action P on p.id=os.id 
                                                         inner join monitor_action mx on mx.monitor  = p.monitor and mx.action = 'create'
                                                         where os.monitor_id not in ( select Monitor_Name from   MQMFT_MONITORSTATUS)  
                                                         order by p.id asc with ur;";

          public const String InsertDeletedMonitor = @"INSERT INTO MQMFT_MONITORSTATUS (HOST_ID,Monitor_Name)values(?,?)";

          //public const String DeletedMonitorsList = @"select Monitor_Name as MonitorNames from MQMFT_MONITORSTATUS where Host_id=? with ur;";                                    
          public const String DeletedMonitorsList = @"select distinct Monitor_Name as MonitorNames from MQMFT_MONITORSTATUS with ur;";                                    

          
          public const String Insert_Update_Host = @"select * from final table 
                                                    (merge into MQMFT_HOST MS
		                                            using ( values (?,?,?,?,?,?,?,?,?,?)) as TX
		                                            (HOST_ID,HOST_NAME,HOST_IP,COORD_QMGR,COORD_PORT,AGENT_QMGR,AGENT_PORT,DEFAULT_CHANNEL,USER_ID,ACTIVE) 
		                                            on MS.HOST_ID=TX.HOST_ID 
		                                            when matched then 
		                                            update set HOST_NAME=TX.HOST_NAME,HOST_IP=TX.HOST_IP,COORD_QMGR=TX.COORD_QMGR,COORD_PORT=TX.COORD_PORT,AGENT_QMGR=TX.AGENT_QMGR,AGENT_PORT=TX.AGENT_PORT,DEFAULT_CHANNEL=TX.DEFAULT_CHANNEL,USER_ID=TX.USER_ID,ACTIVE=TX.ACTIVE
		                                            when not matched then 
		                                            insert(HOST_NAME,HOST_IP,COORD_QMGR,COORD_PORT,AGENT_QMGR,AGENT_PORT,DEFAULT_CHANNEL,USER_ID,ACTIVE,DELETED) 
		                                            values (TX.HOST_NAME,TX.HOST_IP,TX.COORD_QMGR,TX.COORD_PORT,TX.AGENT_QMGR,TX.AGENT_PORT,TX.DEFAULT_CHANNEL,TX.USER_ID,TX.ACTIVE,'N' 
		                                            not atomic continue on Sqlexception);";


        public const String Insert_Host = @"Insert into MQMFT_HOST(HOST_NAME,HOST_IP,COORD_QMGR,COORD_PORT,AGENT_QMGR,AGENT_PORT,DEFAULT_CHANNEL,USER_ID,ACTIVE)VALUES
                                                (?,?,?,?,?,?,?,?,?)";

        public const String Update_Host = @"Update MQMFT_HOST set HOST_NAME=?,HOST_IP=?,COORD_QMGR=?,COORD_PORT=?,AGENT_QMGR=?,AGENT_PORT=?,DEFAULT_CHANNEL=?,USER_ID=?,ACTIVE=? where HOST_ID=?";
        
        public const String Delete_Host = @"update MQMFT_HOST set deleted='Y' where Host_ID=?;";


        public const string Insert_Update_QM = @"select * from final table (merge into ftelog.MQMFT_QM_DETAILS MS using ( values (?,?,?,?,?,?,?,?,?,?) ) as TX ( QMID,QM_NAME,QM_CHANNEL,QM_PORT,QM_USERID,IS_AGENT,IS_CMND,IS_COORD,IS_ACTIVE,HOST_ID) on MS.QMID=TX.QMID when matched then update set QM_NAME=TX.QM_NAME,QM_CHANNEL=TX.QM_CHANNEL,QM_PORT=TX.QM_PORT,QM_USERID=TX.QM_USERID,IS_AGENT=TX.IS_AGENT,IS_CMND=TX.IS_CMND,IS_COORD=TX.IS_COORD,IS_ACTIVE=TX.IS_ACTIVE,HOST_ID=TX.HOST_ID when not matched then insert (QM_NAME,QM_CHANNEL,QM_PORT,QM_USERID,IS_AGENT,IS_CMND,IS_COORD,IS_ACTIVE,HOST_ID) values (TX.QM_NAME,TX.QM_CHANNEL,TX.QM_PORT,TX.QM_USERID,TX.IS_AGENT,TX.IS_CMND,TX.IS_COORD,TX.IS_ACTIVE,TX.HOST_ID) not atomic continue on Sqlexception);";
        public const string Insert_QM = @"insert into MQMFT_QM_DETAILS  (QM_NAME,QM_CHANNEL,QM_PORT,QM_USERID,IS_AGENT,IS_CMND,IS_COORD,IS_ACTIVE,HOST_ID) values  (?,?,?,?,?,?,?,?,?) ;";
        public const string Update_QM = @"update MQMFT_QM_DETAILS  set QM_NAME=?,QM_CHANNEL=?,QM_PORT=?,QM_USERID=?,IS_AGENT=?,IS_CMND=?,IS_COORD=?,IS_ACTIVE=?,HOST_ID=? where QMID=?";
        public const string Delete_QM = @"delete from  MQMFT_QM_DETAILS  where QMID=?";

        //public const String Get_MonitorXML = @"select ma.original_xml_request as MO_XML from (select max(id) as id from monitor where Name=?)ss inner join monitor_action ma on ma.monitor=ss.id and ma.action='create' and ma.original_xml_request is not null with ur;";
        public const String Get_MonitorXML = @"select ma.original_xml_request as MO_XML from (select max(id) as id from monitor where id=?)ss inner join monitor_action ma on ma.monitor=ss.id and ma.action='create' and ma.original_xml_request is not null with ur;";

        public const String Get_JobXML = @"select MA.original_xml_request as JBXML from(select monitor,original_xml_request from monitor_action where original_xml_request is not null and original_xml_request like ? order by ID desc  FETCH FIRST 1 ROWS only)MA inner join monitor M on M.ID=MA.monitor with ur;";

        public const String GetHost = @"select * from MQMFT_HOST where deleted='N' with ur;";

        public const String GetQueueManagers = @" SELECT qm.*,host.host_name, host.host_ip  
                                                                    FROM MQMFT_QM_DETAILS qm 
                                                                    inner join MQMFT_HOST host on qm.HOST_ID = host.HOST_ID 
                                                                    with ur;";

        //public const string GetUserExits = @"select u.user_exit_type as userexittype,ud.user_Exit_value as userexitdetail from MQMFT_USER_EXITS u , MQMFT_USER_EXIT_DETAILS ud where ud.id=u.id and ud.deleted='N' group by u.user_exit_type,ud.user_exit_value";
        public const string GetUserExits = @"select u.user_exit_type as userexittype,ud.user_Exit_value as userexitdetail, ud.Exit_Params  from MQMFT_USER_EXITS u , MQMFT_USER_EXIT_DETAILS ud where ud.id=u.id and ud.deleted='N' group by u.user_exit_type,ud.user_exit_value,ud.Exit_Params";

        public const String GetUserExitTypes = @"Select ID,User_Exit_Type from mqmft_user_exits with ur;";
        public const String InsertUserExitTypes = @"Insert into MQMFT_User_Exits(User_Exit_Type)values(?);";
        public const String UpdateUserExitTypes = @"Update MQMFT_User_Exits set User_Exit_Type=? where ID=?;";

        public const String GetUserExitValues = @"select Exit_id,ID,user_exit_value,Exit_Params from mqmft_user_exit_details where ID=? and Deleted='N' order by ID with ur;";
        public const String InsertUserExitvalues = @"Insert into MQMFT_User_Exit_Details(ID,User_Exit_value,Exit_Params)values(?,?,?);";
        public const String UpdateUserExitValues = @"Update MQMFT_User_Exit_Details set User_Exit_Value=?,Exit_Params=? where Exit_id=?;";
        public const String DeleteUserExitValues = @"Update MQMFT_User_Exit_Details set Deleted='Y' where Exit_id=?;";

        public const String Get_TransferValues = @"select M.key as KEYNAME,M.value as KEYVALUES,T.source_filename as SFileName,T.Destination_filename as DFileName,T.destination_message_queue_name as DMQName 
                                                    from metadata M inner join Transfer TS 
                                                    on TS.transfer_id=? inner join 
                                                    Transfer_item T on T.transfer_id=TS.transfer_id where M.transfer_event_id=TS.complete_id with ur;";


        public const String All_JobNames = @"SELECT DISTINCT JOB_NAME FROM TRANSFER order by job_name with ur;";
    
    }
}