﻿using IBM.WMQ;
using IBM.WMQ.PCF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using MFTEXP.Models;
using System.Dynamic;

namespace MFTEXP.Classes
{
    public class MFTQueue
    {
        /// <summary>
        /// This method is for to get the Queues and their properties from the MQ server.
        /// </summary>
        /// <param name="environment">environment</param>
        /// <returns></returns>

        public List<MQ_Queues> GetAllQueues(string Prefix, MQ_QueueeManager QMgr)
        {
            if (string.IsNullOrEmpty(Prefix))
            {
                Prefix = "SYSTEM.FTE.*";
            }

            var QueUsage = new String[] { "Normal", "Transmission"};

            List<MQ_Queues> lstqueues = new List<MQ_Queues>();
            //List<MQ_Queues> Custom_Queues = null;
            MQQueueManager MQMgr = null;
            Hashtable properties = new Hashtable();
            MFTCommon mftcommon = new MFTCommon();
            List<HostConfig> hst = new List<HostConfig>();
            hst = mftcommon.GetHostdetails();
            HostConfig HstConfig = hst.FirstOrDefault();
            //MQEnvironment.UserId = HstConfig.Userid;
            properties.Add(MQC.HOST_NAME_PROPERTY, QMgr.Host_Name);
            properties.Add(MQC.PORT_PROPERTY, QMgr.QM_Port  );
            properties.Add(MQC.CHANNEL_PROPERTY, QMgr.QM_Channel);
            try
            {
                MQMgr = new MQQueueManager(QMgr.QM_Name  , properties);

                //MQEnvironment.UserId = HstConfig.Userid;
                PCFMessageAgent agent = new PCFMessageAgent(MQMgr);
                PCFMessage reqeuestMessage = new PCFMessage(CMQCFC.MQCMD_INQUIRE_Q_NAMES);
                reqeuestMessage.AddParameter(MQC.MQCA_Q_NAME, Prefix);
                reqeuestMessage.AddParameter(MQC.MQIA_Q_TYPE, MQC.MQQT_LOCAL);


                PCFMessage[] response;

                response = agent.Send(reqeuestMessage);
                string[] queueNames = response[0].GetStringListParameterValue(CMQCFC.MQCACF_Q_NAMES);



                for (int i = 0; i < queueNames.Length; i++)
                {
                    MQ_Queues sysQue = new MQ_Queues();
                    sysQue.Queues = queueNames[i].Trim();

                    try
                    {
                        MQQueue mqQueue = MQMgr.AccessQueue(
                                 queueNames[i].Trim(),
                                 MQC.MQOO_OUTPUT                   // open queue for output
                                 + MQC.MQOO_INQUIRE              // inquire required to get CurrentDepth 
                                 + MQC.MQOO_FAIL_IF_QUIESCING);   // but not if MQM stopping


                        sysQue.CurrentDepth = mqQueue.CurrentDepth.ToString();
                        sysQue.MaxDepth = mqQueue.MaximumDepth.ToString();
                        sysQue.OpenInputCount = mqQueue.OpenInputCount.ToString();
                        sysQue.messages = new List<string>();
                        sysQue.Usage = QueUsage[mqQueue.Usage];

                        //if (Convert.ToInt32(sysQue.CurrentDepth) > 0)
                        //{
                        //    MQMessage msg = new MQMessage();
                        //    MQGetMessageOptions mqGetNextMsgOpts = new MQGetMessageOptions();
                        //    mqGetNextMsgOpts.Options = MQC.MQGMO_BROWSE_NEXT;

                        //    try
                        //    {
                        //        while (true)
                        //        {
                        //            string messageText = msg.ReadString(msg.MessageLength);
                        //            sysQue.messages.Add(messageText);
                        //            mqQueue.Get(msg, mqGetNextMsgOpts);
                        //        }
                        //    }
                        //    catch (MQException mqex)
                        //    {
                        //        throw ex;
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        throw ex;
                        //    }
                        //}
                    }
                    catch (MQException mex)
                    {
                        sysQue.CurrentDepth = "";
                        sysQue.MaxDepth = "";
                        sysQue.Usage = "";
                        sysQue.OpenInputCount = "";
                        sysQue.messages = new List<string>();
                    }

                    lstqueues.Add(sysQue);
                }
                

                MQMgr.Close();
                MQMgr.Disconnect();
            }
            catch (Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                throw new Exception(MFTConstants.Default_MQ_Error_Message);
            }
            finally
            {
                if (MQMgr != null) MQMgr.Disconnect();
            }

            return lstqueues;


        }

        
        public List<dynamic> MFTQueues()
        {
            var Agent = new List<dynamic>();
            MQQueueManager MQMgr = null;
            MQTopic Mtopic = null;
            Hashtable properties = new Hashtable();
            MQMessage message = new MQMessage();
            MFTCommon mftcommon = new MFTCommon();
            try
            {
                List<HostConfig> hst = new List<HostConfig>();
                hst = mftcommon.GetHostdetails();
                HostConfig HstConfig = hst.FirstOrDefault();
                if (HstConfig != null)
                {
                    properties.Add(MQC.HOST_NAME_PROPERTY, HstConfig.Host_Name);
                    properties.Add(MQC.PORT_PROPERTY, HstConfig.Coord_Port);
                    properties.Add(MQC.CHANNEL_PROPERTY, HstConfig.Default_Channel);
                    MQMgr = new MQQueueManager(HstConfig.Coord_Qmgr, properties);

                    Mtopic = MQMgr.AccessTopic("SYSTEM.FTE/Agents/#", null, MQC.MQTOPIC_OPEN_AS_SUBSCRIPTION, MQC.MQSO_CREATE | MQC.MQSO_FAIL_IF_QUIESCING);
                    message = new MQMessage();
                    Mtopic.Get(message);
                    MQGetMessageOptions mqNext = new MQGetMessageOptions();
                    mqNext.Options = MQC.MQGMO_BROWSE_NEXT;
                    while (true)
                    {
                        var Agtdtls = new Dictionary<string, object>();
                        string strmsg = message.ReadString(message.MessageLength);
                        var xmlval = XElement.Parse(strmsg);
                        foreach (var element in xmlval.Elements())
                        {
                            string strattributevalue = element.Attributes().Single().Value.Trim();
                            string strvalue = string.Empty;
                            if (!string.IsNullOrEmpty(element.Value.ToString())) strvalue = element.Value.Trim().ToString();
                            Agtdtls.Add(strattributevalue,strvalue);
                        }
                        Agent.Add(Agtdtls);
                        message = new MQMessage();
                        Mtopic.Get(message);
                    }
                }
            }
            catch (MQException Mqex)
            {
                if (Mqex.ReasonCode != 2033)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(Mqex);  //This is for to handle "MQRC NO MSG AVAILABLE"
                    MFTErrorMessage.Error_Message = MFTConstants.Default_MQ_Error_Message;
                }
            }
            catch (Exception Ex)
            {
                MFTErrorMessage.Error_Message = MFTConstants.Default_Database_Error_Message;
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);

            }
            finally
            {
                if (MQMgr != null) MQMgr.Disconnect();
                if (Mtopic != null) Mtopic.Close();//Actually this is not required because MQ QueueManager disconnect will close all of queues/topics automatically before closes the connection to the mq queuemanager.
            }
            return Agent;
        }

     
    }
}