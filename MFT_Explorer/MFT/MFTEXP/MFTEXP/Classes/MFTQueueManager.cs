﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using MFTEXP.Models;

namespace MFTEXP.Classes
{
    public class MFTQueueManager
    {

        MFTHelper helper = new MFTHelper();

        public List<MQ_QueueeManager> GetQueueManagers(string HostID)
        {
            List<MQ_QueueeManager> managers = new List<MQ_QueueeManager>();
            
            QueryParameters inputParams = new QueryParameters();
            inputParams.query = MFTQueries.GetQueueManagers;
            try
            {
                using (DataTable dt = helper.GetDataTable(inputParams))
                {
                    if (dt.Rows.Count > 0)
                    {
                        managers = (from DataRow row in dt.Rows
                                    select new MQ_QueueeManager()
                                {
                                    QM_ID = Convert.ToInt32(row["QMID"]),
                                    QM_Name = Convert.ToString(row["QM_NAME"]),
                                    QM_Channel = Convert.ToString(row["QM_CHANNEL"]),
                                    QM_Port = Convert.ToInt32(row["QM_PORT"]),
                                    QM_UserID = Convert.ToString(row["QM_USERID"]),
                                    Is_Agent = Convert.ToString(row["IS_AGENT"]),
                                    Is_Cmnd = Convert.ToString(row["IS_CMND"]),
                                    Is_Coord = Convert.ToString(row["IS_COORD"]),
                                    Is_Active = Convert.ToString(row["IS_ACTIVE"]),
                                    Host_ID = Convert.ToInt32(row["HOST_ID"]),
                                    Host_Name = Convert.ToString(row["HOST_NAME"]),
                                    Host_IP = Convert.ToString(row["HOST_IP"])

                                }).Where(x => x.Host_ID == Convert.ToInt32(HostID)).ToList();
                    }
                }
            }
            catch (Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                throw;
            }
            return managers;
        }

        public List<MQ_QueueeManager> GetAllQueueManagers()
        {
            List<MQ_QueueeManager> managers = new List<MQ_QueueeManager>();

            QueryParameters inputParams = new QueryParameters();
            inputParams.query = MFTQueries.GetQueueManagers;
            try
            {
                using (DataTable dt = helper.GetDataTable(inputParams))
                {
                    if (dt.Rows.Count > 0)
                    {
                        managers = (from DataRow row in dt.Rows
                                    select new MQ_QueueeManager()
                                    {
                                        QM_ID = Convert.ToInt32(row["QMID"]),
                                        QM_Name = Convert.ToString(row["QM_NAME"]),
                                        QM_Channel = Convert.ToString(row["QM_CHANNEL"]),
                                        QM_Port = Convert.ToInt32(row["QM_PORT"]),
                                        QM_UserID = Convert.ToString(row["QM_USERID"]),
                                        Is_Agent = Convert.ToString(row["IS_AGENT"]),
                                        Is_Cmnd = Convert.ToString(row["IS_CMND"]),
                                        Is_Coord = Convert.ToString(row["IS_COORD"]),
                                        Is_Active = Convert.ToString(row["IS_ACTIVE"]),
                                        Host_ID = Convert.ToInt32(row["HOST_ID"]),
                                        Host_Name = Convert.ToString(row["HOST_NAME"]),
                                        Host_IP = Convert.ToString(row["HOST_IP"])

                                    }).ToList();
                    }
                }
            }
            catch (Exception Ex )
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                throw;
            }
            return managers;
        }

        public  bool SaveQueueManager(MQ_QueueeManager data)
        {
            bool ActionComplete = false;
            MFTHelper helper = new MFTHelper();
            QueryParameters inputParams = new QueryParameters();
            List<MQ_QueueeManager> Managers = new List<MQ_QueueeManager>();
            try
            {
                inputParams.query = MFTQueries.Insert_Update_QM;
                if (!string.IsNullOrEmpty(data.Action))
                {
                    
                    inputParams.AddParam("QM_Name", data.QM_Name.Trim(), OdbcType.VarChar);
                    inputParams.AddParam("QM_Channel", data.QM_Channel.Trim(), OdbcType.VarChar);
                    inputParams.AddParam("QM_Port", data.QM_Port.ToString().Trim(), OdbcType.Int);
                    inputParams.AddParam("QM_UserID", data.QM_UserID.Trim(), OdbcType.VarChar);
                    inputParams.AddParam("Is_Agent", data.Is_Agent.ToString().Trim(), OdbcType.Char);
                    inputParams.AddParam("Is_Cmnd", data.Is_Cmnd.Trim(), OdbcType.Char);
                    inputParams.AddParam("Is_Coord", data.Is_Coord.Trim(), OdbcType.Char);
                    inputParams.AddParam("Is_Active", data.Is_Active.ToString().Trim(), OdbcType.Char);
                    inputParams.AddParam("Host_ID", data.Host_ID.ToString().Trim(), OdbcType.Int );

                    if (data.Action == "Update")
                    {
                        inputParams.query = MFTQueries.Update_QM;
                        inputParams.AddParam("QM_ID", data.QM_ID.ToString().Trim(), OdbcType.Int);
                        if (helper.UpdateCommand(inputParams) > 0)
                            ActionComplete = true;
                        else
                            ActionComplete = false;
                        
                    }
                    else
                    {
                        inputParams.query = MFTQueries.Insert_QM;
                        //inputParams.AddParam("QM_ID", "0", OdbcType.Int);
                        if (helper.InsertCommand(inputParams) > 0)
                            ActionComplete = true;
                        else
                            ActionComplete = false;
                    }
                }
            }
            catch (Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                //throw new Exception(MFTConstants.WM_Failure_Message);
                throw Ex;
            }
            return ActionComplete;
        }


        public bool DeleteQM(string QMID)
        {
            bool  ActionComplete = false ;
            MFTHelper helper = new MFTHelper();
            QueryParameters inputParams = new QueryParameters();
            inputParams.query = MFTQueries.Delete_QM;
            inputParams.AddParam("QMID", QMID.Trim(), OdbcType.Int);
            if (helper.UpdateCommand(inputParams) > 0)
                ActionComplete = true;
            else
                ActionComplete = false;
            return ActionComplete;
        }



    }
}