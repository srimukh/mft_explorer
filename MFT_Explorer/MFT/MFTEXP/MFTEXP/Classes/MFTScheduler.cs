﻿using IBM.WMQ;
using MFTEXP.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace MFTEXP.Classes
{
    public class MFTScheduler
    {
        /// <summary>
        /// This method is for to get the list of Scheduled Tranfers.
        /// </summary>
        /// <returns></returns>

        public List<dynamic> GetScheduledTransfers()
        {
            var Schedulers = new List<dynamic>();
            MQQueueManager MQMgr = null;
            MQTopic Mtopic = null;
            Hashtable properties = new Hashtable();
            MQMessage message = new MQMessage();
            MFTCommon mftcommon = new MFTCommon();
            try
            {
                List<HostConfig> hst = new List<HostConfig>();
                hst = mftcommon.GetHostdetails();
                HostConfig HstConfig = hst.FirstOrDefault();
                if (HstConfig != null)
                {
                    properties.Add(MQC.HOST_NAME_PROPERTY, HstConfig.Host_Name);
                    properties.Add(MQC.PORT_PROPERTY, HstConfig.Coord_Port);
                    properties.Add(MQC.CHANNEL_PROPERTY, HstConfig.Default_Channel);
                    MQMgr = new MQQueueManager(HstConfig.Coord_Qmgr, properties);

                    Mtopic = MQMgr.AccessTopic("SYSTEM.FTE/Scheduler/#", null, MQC.MQTOPIC_OPEN_AS_SUBSCRIPTION, MQC.MQSO_CREATE | MQC.MQSO_FAIL_IF_QUIESCING);
                    message = new MQMessage();
                    Mtopic.Get(message);
                    MQGetMessageOptions mqNext = new MQGetMessageOptions();
                    mqNext.Options = MQC.MQGMO_BROWSE_NEXT;
                    while (true)
                    {
                        var scheduledtls = new Dictionary<string, object>();
                        string strmsg = message.ReadString(message.MessageLength);
                        var xmlval = XElement.Parse(strmsg);
                        int NoofTransfer = 0;
                        foreach (var element in xmlval.Elements())
                        {
                            if (NoofTransfer > 0)
                            {
                                Schedulers.Add(scheduledtls);
                                scheduledtls = new Dictionary<string, object>();
                            }
                            scheduledtls.Add("Name", element.Attribute("id").Value.ToString());
                            scheduledtls.Add("Job", element.Descendants("job").Elements("name").Single().Value.ToString());
                            scheduledtls.Add("SAgent", element.Descendants("sourceAgent").Attributes("agent").Single().Value.ToString());
                            scheduledtls.Add("DAgent", element.Descendants("destinationAgent").Attributes("agent").Single().Value.ToString());
                            scheduledtls.Add("scheduletimezone", element.Descendants("schedule").Elements("submit").Attributes("timezone").Single().Value.ToString());

                          //  DateTime dt = DateTime.Parse(element.Descendants("schedule").Elements("submit").Attributes("timezone").Single().Value.ToString().Trim()).ToUniversalTime();
                            
                            scheduledtls.Add("Scheduledtime", element.Descendants("schedule").Elements("submit").Single().Value.ToString());
                            if (element.Descendants("schedule").Elements("repeat").Elements("frequency").Count() > 0)
                            {
                                scheduledtls.Add("RepeatEvery", element.Descendants("schedule").Elements("repeat").Elements("frequency").Single().Value.ToString());
                                scheduledtls.Add("Repeattype", element.Descendants("schedule").Elements("repeat").Elements("frequency").Attributes("interval").Single().Value.ToString());
                            }
                            else
                            {
                                scheduledtls.Add("RepeatEvery", "-");
                                scheduledtls.Add("Repeattype", "-");
                            }

                            if (element.Descendants("schedule").Elements("repeat").Elements("expireTime").Count() > 0)
                            {
                                scheduledtls.Add("RepeatUntil", element.Descendants("schedule").Elements("repeat").Elements("expireTime").Single().Value.ToString());
                            }
                            else if (element.Descendants("schedule").Elements("repeat").Elements("expireCount").Count() > 0)
                            {
                                scheduledtls.Add("RepeatUntil", element.Descendants("schedule").Elements("repeat").Elements("expireCount").Single().Value.ToString() + " repetitions");
                            }
                            else
                            {
                                scheduledtls.Add("RepeatUntil", "-");
                            }
                            scheduledtls.Add("NextTransfer", element.Descendants("schedule").Elements("next").Single().Value.ToString());
                            scheduledtls.Add("Xml", element.ToString());
                            NoofTransfer = NoofTransfer + 1;
                        }
                        if (NoofTransfer > 0) Schedulers.Add(scheduledtls);
                        message = new MQMessage();
                        Mtopic.Get(message);


                    }
                }
            }
            catch (MQException Mqex)
            {
                if (Mqex.ReasonCode != 2033)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(Mqex);  //This is for to handle "MQRC NO MSG AVAILABLE"
                    MFTErrorMessage.Error_Message = MFTConstants.Default_MQ_Error_Message;
                }
            }
            catch (Exception Ex)
            {
                MFTErrorMessage.Error_Message = MFTConstants.Default_Error_Message;
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);

            }
            finally
            {
                if (MQMgr != null) MQMgr.Disconnect();
                if (Mtopic != null) Mtopic.Close();//Actually this is not required because MQ QueueManager disconnect will close all of queues/topics automatically before closes the connection to the mq queuemanager.
            }
            return Schedulers;
        }

        public Boolean DeleteScheduledTransfer(string TrnsId, String SrcAgent)
        {
            MFTCommon mftcommon = new MFTCommon();
            Boolean status = false;
            try
            {
                List<HostConfig> hst = new List<HostConfig>();
                hst = mftcommon.GetHostdetails();
                HostConfig HstConfig = hst.FirstOrDefault();
                if (HstConfig != null)
                {
                    string deleteTransfer = String.Format(MFTConstants.DeleteScheduledTransfer_XML, HstConfig.Host_Name, HstConfig.Userid,TrnsId);
                    status = mftcommon.PutDeleteMessage(SrcAgent.Trim(), deleteTransfer);
                   
                }
            }
            catch(Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            return status;
        }
    }
}