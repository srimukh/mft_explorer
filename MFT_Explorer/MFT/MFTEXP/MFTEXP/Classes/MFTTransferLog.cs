﻿using IBM.WMQ;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using MFTEXP.Models;
using System.Xml;

namespace MFTEXP.Classes
{
    public class MFTTransferLog
    {
        MFTHelper helper = new MFTHelper();

        public object GetTransferLogs(string sdate, string edate, string JobName)
        {
            MFTCommon mftcommon = new MFTCommon();
            QueryParameters inputParams = new QueryParameters();
            inputParams.query = MFTQueries.Transfer_Log;
            inputParams.AddParam("fromDateTime", sdate, OdbcType.Char);
            inputParams.AddParam("toDateTime", edate, OdbcType.Char);
            if (string.IsNullOrEmpty(JobName))
            { inputParams.AddParam("JobName", "%", OdbcType.VarChar); }
            else
            { inputParams.AddParam("JobName", JobName, OdbcType.VarChar); }
            var tResult = new List<TransferLogs>();
            var response = new JsonResult();
            try
            {
                using (DataTable dt = helper.GetDataTable(inputParams))
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            TransferLogs tlog = new TransferLogs();
                            tlog.JobName = Convert.ToString(dr["JobName"]);
                            tlog.TransferID = Convert.ToString(dr["TransferID"]); ;
                            tlog.OverAllResult = Convert.ToString(dr["OverAllResult"]);
                            tlog.SourceSystem = Convert.ToString(dr["SourceSystem"]);
                            tlog.TargetSystem = Convert.ToString(dr["TargetSystem"]);
                            tlog.OverAllResultText = Convert.ToString(dr["OverAllResultText"]);
                            tlog.StartTime = Convert.ToString(dr["StartTime"]);
                            tlog.EndTime = Convert.ToString(dr["EndTime"]);
                            tlog.SourceFile = Extractfilename(Convert.ToString(dr["SourceFile"]));
                            tlog.SourceFileSize = Convert.ToInt64(dr["SourceFileSize"]);
                            tlog.DestinationFile = Extractfilename(Convert.ToString(dr["DestinationFile"]));
                            tlog.DestinationFileSize = Convert.ToInt64(dr["DestinationFileSize"]);
                            tlog.NumberOfFiles = Convert.ToInt32(dr["NumberOfFiles"]);
                            tlog.FileResult = Convert.ToString(dr["FileResult"]);
                            tlog.FileResultMessage = Convert.ToString(dr["FileResultMessage"]);
                            tlog.Status = Convert.ToString(dr["Status"]).Trim();
                            TimeSpan ts = Convert.ToDateTime(dr["EndTime"]) - Convert.ToDateTime(dr["StartTime"]);
                            tlog.TimeElapsed = string.Format("{0}:{1}", ts.Minutes.ToString("00"), ts.Seconds.ToString("00"));
                            tResult.Add(tlog);
                        }
                    }
                }
                TransferLogsModel TLogsModel = new TransferLogsModel();
                TLogsModel.TransferLogs = tResult.ToArray();
               
                response.Data = TLogsModel;
                var SuccessCount = tResult.Where(log => !string.IsNullOrEmpty(log.Status) && log.Status.ToLower() == "success").Count();
                var FailureCount = tResult.Where(log => !string.IsNullOrEmpty(log.Status) && log.Status.ToLower() == "failure").Count();
                var PartialsuccessCount = tResult.Where(log => !string.IsNullOrEmpty(log.Status) && log.Status.ToLower() == "partial success").Count();
                var CancelledCount = tResult.Where(log => !string.IsNullOrEmpty(log.Status) && log.Status.ToLower() == "cancelled").Count();



                TLogsModel.TransferLogColumn2DChart = new Models.TransferLogColumn2D
                {
                    Chart = new Models.ChartProperties
                    {
                        Caption = string.Format("Transfer Logs from {0} to {1}", sdate, edate),
                        ShowLabels = 1,
                        ShowLegend = 0
                    },
                    Data = new Models.ChartDataProperties[]{ 
                
                    new Models.ChartDataProperties { Label = "Success", Value = SuccessCount.ToString(),color="#39e600" },
                    new Models.ChartDataProperties { Label = "Failure", Value = FailureCount.ToString(),color="#ff4000" },
                    new Models.ChartDataProperties { Label = "Partial Success", Value = PartialsuccessCount.ToString(),color="#ffa31a" },
                    new Models.ChartDataProperties { Label = "Cancelled", Value = CancelledCount.ToString(),color="#fffff" }
                }
                };
            }
            catch(Exception Ex)
            {
                MFTErrorMessage.Error_Message = MFTConstants.Default_Error_Message;
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            return response.Data;


           
        }

        public string Extractfilename(string sfile)
        {
            string strextracted = string.Empty;
            if (!string.IsNullOrEmpty(sfile))
            {
                int lastindex = 0;
                if (sfile.Contains("\\"))
                {
                    lastindex = sfile.LastIndexOf("\\");
                }
                else if (sfile.Contains("/"))
                {
                    lastindex = sfile.LastIndexOf("/");
                }

                strextracted = sfile.Substring(lastindex + 1);
            }
            else
            {
               
                strextracted = "-";
            }
            return strextracted;
        }


        public string ExtractDirectory(string sfile)
        {
            string strextracted = string.Empty;
            if (!string.IsNullOrEmpty(sfile))
            {
                int lastindex = 0;
                if (sfile.Contains("\\"))
                {
                    lastindex = sfile.LastIndexOf("\\");
                }
                else if (sfile.Contains("/"))
                {
                    lastindex = sfile.LastIndexOf("/");
                }

                strextracted = sfile.Substring(0, lastindex);
            }
            else
            {

                strextracted = "-";
            }
            return strextracted;
        }

        public bool PerformReplayAction(string JobName, string Transfer_ID, string TargetExists)
        {
            bool replayedstatus = false;
            MFTCommon mftcommon = new MFTCommon();
            Transfer_details Ts_Details = new Transfer_details();
            MFTMonitorCreation Mntr_Creation = new MFTMonitorCreation();
            try
            {
                using (DataTable dl = mftcommon.GetXMLForReplay(JobName.Trim()))
                {
                    if (dl.Rows.Count > 0)
                    {
                        
                        Ts_Details = Mntr_Creation.GetMonitorValuesForReplay(Convert.ToString(dl.Rows[0]["JBXML"]).Trim());
                        MFTQueueManager qms = new MFTQueueManager();
                        List<MQ_QueueeManager> qmgrs = qms.GetAllQueueManagers();
                        ;
                        foreach (MQ_QueueeManager qmgr in qmgrs)
                        {
                            if (qmgr.QM_Name == Ts_Details.Src_QueMgr )
                            {
                                Ts_Details.Src_Channel = qmgr.QM_Channel;
                                Ts_Details.Src_Port = qmgr.QM_Port;
                                Ts_Details.Src_HostIP = qmgr.Host_IP;
                                Ts_Details.Src_HostName = qmgr.Host_Name;
                                break;
                            }
                        }
                        


                        using (DataTable ds = mftcommon.GetVariableValuesForReplay(Transfer_ID.Trim()))
                        {
                            if (ds.Rows.Count > 0)
                            {
                                if (Ts_Details.Source_type.ToLower() == "file")
                                {
                                    Ts_Details.Src_file = Convert.ToString(ds.Rows[0]["SFileName"]);
                                }
                                else if (Ts_Details.Source_type.ToLower() == "queue")
                                {
                                    Ts_Details.Src_file = Convert.ToString(ds.Rows[0]["DMQName"]);
                                }
                                Ts_Details.Source_type = Ts_Details.Source_type.ToLower();
                                if (Ts_Details.Dest_type.ToLower() == "directory")
                                {
                                    Ts_Details.Trgt_file = ExtractDirectory(Convert.ToString(ds.Rows[0]["DFileName"]));
                                }
                                else if (Ts_Details.Dest_type.ToLower() == "file")
                                {
                                    Ts_Details.Trgt_file = Convert.ToString(ds.Rows[0]["DFileName"]);
                                }
                                else if (Ts_Details.Dest_type.ToLower() == "queue")
                                {
                                    Ts_Details.Trgt_file = Convert.ToString(ds.Rows[0]["DMQName"]);
                                }

                                Ts_Details.Dest_type = Ts_Details.Dest_type.ToLower();

                                foreach (DataRow Dr in ds.Rows)
                                {
                                    switch (Convert.ToString(Dr["KEYNAME"]).Trim())
                                    {
                                        case "PreSrc":
                                            Ts_Details.Pre_Src = Convert.ToString(Dr["KEYVALUES"]).Trim();
                                            break;
                                        case "PreDst":
                                            Ts_Details.Pre_dest = Convert.ToString(Dr["KEYVALUES"]).Trim();
                                            break;
                                        case "PostDst":
                                            Ts_Details.Post_dest = Convert.ToString(Dr["KEYVALUES"]).Trim();
                                            break;
                                        case "PostSrc":
                                            Ts_Details.Post_Src = Convert.ToString(Dr["KEYVALUES"]).Trim();
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(Ts_Details.Src_Agent))
                {
                    if (Ts_Details.Trgt_exists ==  "error" && TargetExists == "overwrite")
                    {
                        Ts_Details.Trgt_exists = TargetExists;
                    }
                     replayedstatus = Mntr_Creation.CreateXML(Ts_Details, MFTCommon.RequestType.Onetimetransfer);
                }
            }
            catch(Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            return  replayedstatus ;

        }



        public List<String> GetAllJobNames()
        {
            List<String> jobnames = new List<String>();

            QueryParameters inputParams = new QueryParameters();
            inputParams.query = MFTQueries.All_JobNames;
            try
            {
                using (DataTable dt = helper.GetDataTable(inputParams))
                {
                    if (dt.Rows.Count > 0)
                    {
                        jobnames = (from DataRow row in dt.Rows
                                    select Convert.ToString(row["JOB_NAME"]) ).ToList();
                    }
                }
            }
            catch (Exception Ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
                throw;
            }
            return jobnames;
        }

    }
}