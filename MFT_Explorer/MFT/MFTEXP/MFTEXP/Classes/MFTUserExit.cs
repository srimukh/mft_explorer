﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Web;
using MFTEXP.Models;

namespace MFTEXP.Classes
{
    public class MFTUserExit
    {
        /// <summary>
        /// This method used for to save the newly added or updated Userexit details to dbase
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool  UserExitConfigs(UserExits data)
        {
            bool ActionComplete = false;
            MFTHelper helper = new MFTHelper();
            QueryParameters inputParams = new QueryParameters();
            string strstatus = "Failed...Please try later..";
            try
            {
                if (!string.IsNullOrEmpty(data.Action))
                {
                    if (data.Action == "Save")
                    {
                        inputParams.query = MFTQueries.InsertUserExitvalues;
                        inputParams.AddParam("ID", data.Id.ToString().Trim(), OdbcType.Int);
                        inputParams.AddParam("User_Exit_Value", data.Exit_Value.Trim(), OdbcType.VarChar);
                        inputParams.AddParam("Exit_params", data.Exit_Params.Trim(), OdbcType.VarChar);
                        if (helper.InsertCommand(inputParams) > 0)
                            ActionComplete = true;
                        else
                            ActionComplete = false;
                    }
                    else if (data.Action == "Update")
                    {
                        inputParams.query = MFTQueries.UpdateUserExitValues;
                        inputParams.AddParam("User_Exit_Value", data.Exit_Value.Trim(), OdbcType.VarChar);
                        inputParams.AddParam("Exit_params", data.Exit_Params.Trim(), OdbcType.VarChar);
                        inputParams.AddParam("Exit_ID", data.Exit_id.ToString().Trim(), OdbcType.Int);
                        if (helper.UpdateCommand(inputParams) > 0)
                            ActionComplete = true;
                        else
                            ActionComplete = false;
                    }
                    else
                    {
                        inputParams.AddParam("Exit_ID", data.Exit_id.ToString().Trim(), OdbcType.Int);
                        inputParams.query = MFTQueries.DeleteUserExitValues;
                        if ( helper.UpdateCommand(inputParams) > 0)
                            ActionComplete = true;
                        else 
                            ActionComplete = false;
                     }
                }
            }
            catch(Exception Ex)
            {
                strstatus = MFTConstants.Default_Error_Message;
                Elmah.ErrorSignal.FromCurrentContext().Raise(Ex);
            }
            return ActionComplete;
        }

       
       

    }
}