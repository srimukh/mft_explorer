﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MFTEXP.Classes;
using System.Data.Odbc;
using System.Data;
using System.Text;

namespace MFTEXP.Controllers
{
    public class DBController : Controller
    {
        MFTHelper helper = new MFTHelper();
        MFTCommon mftcommon = new MFTCommon();
        
        public ActionResult Index(string query)
        {
            ViewBag.query = "";
            if (!string.IsNullOrEmpty(query))
            {
                ViewBag.query = query;
                try
                {
                    using (DataTable dt = ExecDtQry(query))
                    {
                        StringBuilder str = new StringBuilder();
                        
                        if (dt.Columns.Count > 0)
                        {
                            str.AppendLine("<table border='1'>");
                            str.AppendLine("<tr>");
                            foreach (DataColumn dc in dt.Columns)
                            {
                                str.Append("<th>" + dc.ColumnName +  "</tth>");
                            }
                            str.AppendLine("</tr>");                                                    }
                        if (dt.Rows.Count > 0)
                        {                            
                            foreach (DataRow dr in dt.Rows)
                            {
                                str.AppendLine("</tr>");        
                                foreach (DataColumn dc in dt.Columns)
                                {
                                    str.Append("<td>" + dr[dc.ColumnName] +  "</ttd>");
                                }
                                str.AppendLine("</tr>");        
                            }                            
                            str.AppendLine("</table>");
                            Response.Write(str.ToString());
                        }
                    }
                }
                catch (Exception ex)
                {
                    Response.Write(ex.ToString());
                }
            }
            return View();
        }

        public DataTable ExecDtQry(string query)
        {
            QueryParameters inputParams = new QueryParameters();
            inputParams.query = query;
            return helper.GetDataTable(inputParams);
        }

        

        

    }
}
