﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using MFTEXP.Classes;
using MFTEXP.Models;

using System.Text;
using System.Web.Script.Serialization;

namespace MFTEXP.Controllers
{
    public class MFTController : Controller
    {
        //
        // GET: /MFT/

        public ActionResult Index()
        {
            return View();
        }

        #region AGENTS
        public ActionResult Agents()
        {
            return View("~/Views/MFT/MFTAgent.cshtml");
        }

        public JsonResult GetAgents()
        {
            var response = new JsonRespnse();
            try
            {
                MFTAgent Agt = new MFTAgent();
                response.Result = new AgentsJsonResult() {
                    Agents = Agt.GetAgents()
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure ;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region MONITORS
        public ActionResult Monitors()
        {
            return View("~/Views/MFT/MFTMonitor.cshtml");
        }

        public JsonResult GetMonitors()
        {
            var response = new JsonRespnse();
            try
            {
                MFTMonitor mntr = new MFTMonitor();
                response.Result = new MonitorsJsonResult()
                {
                    Monitors = mntr.GetMonitors()
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PerformMonitorAction(string QMGR, string MID, string MName, string AName, MFTCommon.Current_Action_Performed actiontype)
        {
            var response = new JsonRespnse();
            try
            {
                MFTMonitor mnclass = new MFTMonitor();
                if (!mnclass.Perform_Monitor_Action(MID, MName, QMGR, AName, actiontype))
                {
                    response.Status = JsonResponseStatus.Failure;
                    response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
                }
                else
                {
                    response.Status = JsonResponseStatus.Success;
                }
                MFTMonitor mntr = new MFTMonitor();
                response.Result = new MonitorsJsonResult()
                {
                    Monitors = mntr.GetMonitors()
                };
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
           return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region QUEUES
        public ActionResult Queues()
        {
            return View("~/Views/MFT/MFTQueue.cshtml");
        }

        public JsonResult GetAllQueues(String Prefix, string QMgr)
        {
            var response = new JsonRespnse();
            try
            {
                JavaScriptSerializer jsonObj = new JavaScriptSerializer();
                MQ_QueueeManager qMgr = jsonObj.Deserialize<MQ_QueueeManager>(QMgr);

                MFTQueue Que = new MFTQueue();
                response.Result = new QueuesJsonResult() {
                    Queues  = Que.GetAllQueues(Prefix, qMgr)
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                        
        }
        #endregion

        #region TRANSFERS / MONITORS
        public ActionResult Transfers()
        {
            return View("~/Views/MFT/MFTMonitorCreation.cshtml");
        }

        public JsonResult GetAgents_QMgrs()
        {
            var response = new JsonRespnse();
            try
            {
                MFTCommon mftcommon = new MFTCommon();
                MFTQueueManager mftManager = new MFTQueueManager();
                response.Result = new AgentsManagersJsonResult()
                {
                    Agents = mftcommon.GetAgentsandQMGR(),
                    Managers =  mftManager.GetAllQueueManagers()
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetQueues(string QMgr, string environment)
        {
            var response = new JsonRespnse();
            try
            {
                //JavaScriptSerializer jsonObj = new JavaScriptSerializer();
                //MQ_QueueeManager qMgr = jsonObj.Deserialize<MQ_QueueeManager>(QMgr);

                MFTMonitorCreation MFTTrans = new MFTMonitorCreation();
                response.Result = new QueuesJsonResult()
                {
                    Queues = MFTTrans.GetAllQueueNames(QMgr, environment)
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                        
        }
                
        public JsonResult BuildMonitor(string requestMonitor)
        {
            var response = new JsonRespnse();
            try
            {
                JavaScriptSerializer jsonObj = new JavaScriptSerializer();
                Transfer_details data = jsonObj.Deserialize<Transfer_details>(requestMonitor);

                MFTMonitorCreation MFTTrans = new MFTMonitorCreation();
                Boolean ActionComplete = false;
                if (!string.IsNullOrEmpty(data.Action))
                {
                    if (data.Action.Trim() == "Create")
                    {
                        ActionComplete = MFTTrans.CreateXML(data, MFTCommon.RequestType.Monitor);

                    }
                    else if (data.Action.Trim() == "Edit")
                    {
                        MFTMonitor mcls = new MFTMonitor();
                        Boolean monitordeleted = mcls.PutMessage(data.Monitor_ID.Trim(), data.Monitor_Name.Trim().ToUpper(), data.Exists_QueMGR.Trim(), data.Exists_AgentName.Trim(), MFTCommon.Current_Action_Performed.Deleted);
                        if (monitordeleted)
                        {
                            MFTMonitor monitor = new MFTMonitor();
                            monitor.InsertDeletedMonitorNames(data.Monitor_ID);
                            ActionComplete = MFTTrans.CreateXML(data, MFTCommon.RequestType.Monitor);
                        }
                    }
                    else
                    {
                        ActionComplete = MFTTrans.CreateXML(data, MFTCommon.RequestType.Onetimetransfer);
                    }

                    if (!ActionComplete)
                    {
                        response.Status = JsonResponseStatus.Failure;
                        response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
                    }
                    else
                    {
                        response.Status = JsonResponseStatus.Success;
                    }
                }


                MFTMonitor mntr = new MFTMonitor();
                response.Result = new MonitorsJsonResult()
                {
                    Monitors = mntr.GetMonitors()
                };
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);
       }

        public JsonResult GetMonitordetails(string MonitorID)
        {
            var response = new JsonRespnse();
            try
            {
                MFTMonitorCreation MFTTrans = new MFTMonitorCreation();
                response.Result = new TransferDetailsJsonResult()
                {
                    MonitorDetails = MFTTrans.GetMonitorValues(MonitorID)
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SCHEDULES
        public ActionResult Schedules()
        {
            return View("~/Views/MFT/MFTScheduler.cshtml");
        }

        public JsonResult GetScheduledTransfers()
        {
            var response = new JsonRespnse();
            try
            {
                MFTScheduler Scdler = new MFTScheduler();
                response.Result = new SchedulessJsonResult()
                {
                    Schedulers = Scdler.GetScheduledTransfers()
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                        
        }

        public JsonResult DeleteTransfer(string TransferId, string SourceAgent)
        {
            var response = new JsonRespnse();
            try
            {
                MFTScheduler Scdler = new MFTScheduler();
                if (Scdler.DeleteScheduledTransfer(TransferId, SourceAgent))
                {
                    response.Status = JsonResponseStatus.Success;
                }
                else
                {
                    response.Status = JsonResponseStatus.Failure;
                    response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
                }
                response.Result = new SchedulessJsonResult()
                {
                    Schedulers = Scdler.GetScheduledTransfers()
                };                
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                        
        }
        #endregion

        #region TRANSFER LOG
        public ActionResult TransferLog()
        {
            return View("~/Views/MFT/MFTTransferLog.cshtml");
        }

        public JsonResult PerformReplay(string JobName, string Transfer_ID,string TargetExists)
        {
            var response = new JsonRespnse();
            try
            {
                MFTTransferLog TL = new MFTTransferLog();
                if (TL.PerformReplayAction(JobName, Transfer_ID, TargetExists))
                {
                    response.Status = JsonResponseStatus.Success;
                }
                else 
                {
                    response.Status = JsonResponseStatus.Failure;
                    response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
                }
                
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                        
        }

        public JsonResult GetTransferLogs(string fromDate, string toDate, string JobName)
        {
            var response = new JsonRespnse();
            try
            {
                MFTTransferLog TL = new MFTTransferLog();
                string fromdatewithtime = fromDate + " 00:00:00";
                string enddatewithtime = toDate + " 23:59:59";

                response.Result = new TransferLogsJsonResult()
                {
                    LogData = TL.GetTransferLogs(fromdatewithtime, enddatewithtime, JobName)
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                        
        }
        #endregion

        public JsonResult GetJobNames()
        {
            var response = new JsonRespnse();
            try
            {
                MFTTransferLog TL = new MFTTransferLog();


                response.Result = new JobNamesJsonResult()
                {
                    JobNames = TL.GetAllJobNames()
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        #region ADMIN HOSTCONFIG
        public ActionResult HostConfig()
        {
            return View("~/Views/MFT/MFTHostConfig.cshtml");
        }

        public JsonResult SaveHost(HostConfig data)
        {
            var response = new JsonRespnse();
            try
            {
                MFTHostConfig Hst = new MFTHostConfig();
                if (Hst.SaveHostConfig(data))
                {
                    response.Status = JsonResponseStatus.Success;
                }
                else
                {
                    response.Status = JsonResponseStatus.Failure;
                    response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
                }

                MFTCommon mftcommon = new MFTCommon();
                response.Result = new HostConfigsJsonResult()
                {
                    Hosts = mftcommon.GetHostdetails()
                };
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                        
        }

        public JsonResult GetHosts()
        {
            var response = new JsonRespnse();
            try
            {
                MFTCommon mftcommon = new MFTCommon();
                response.Result = new HostConfigsJsonResult()
                {
                    Hosts = mftcommon.GetHostdetails()
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                        
        }

        public JsonResult DeleteHost(string hostid)
        {
            var response = new JsonRespnse();
            try
            {
                MFTHostConfig Hst = new MFTHostConfig();
                if (Hst.DeleteHostConfig(hostid))
                {
                    response.Status = JsonResponseStatus.Success;
                }
                else
                {
                    response.Status = JsonResponseStatus.Failure;
                    response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
                }

                MFTCommon mftcommon = new MFTCommon();
                response.Result = new HostConfigsJsonResult()
                {
                    Hosts = mftcommon.GetHostdetails()
                };
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                        
        }
        #endregion

        #region  QUEUE MANAGERS
        public ActionResult QueueManagers()
        {
            return View("~/Views/MFT/MFTQueueManagers.cshtml");
        }

        public JsonResult GetQueueManagers(string HostID)
        {
            var response = new JsonRespnse();
            try
            {
                MFTQueueManager mftManager = new MFTQueueManager();
                response.Result = new QueueManagersJsonResult()
                {
                    Managers = mftManager.GetQueueManagers(HostID)
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                   
        }

        public JsonResult SaveQueueManager(MQ_QueueeManager data)
        {
            var response = new JsonRespnse();
            try
            {
                MFTQueueManager qm = new MFTQueueManager();
                if (qm.SaveQueueManager(data))
                {
                    response.Status = JsonResponseStatus.Success;
                }
                else 
                {
                    response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
                }
                response.Result = new QueueManagersJsonResult()
                {
                    Managers = qm.GetQueueManagers(data.Host_ID.ToString())
                };  
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                   
        }

        public JsonResult DeleteQM(string QMID, string Host_ID)
        {
            var response = new JsonRespnse();
            try
            {
                MFTQueueManager qm = new MFTQueueManager();
                if (qm.DeleteQM(QMID))
                {
                    response.Status = JsonResponseStatus.Success;
                }
                else 
                {
                    response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
                }
                response.Result = new QueueManagersJsonResult()
                {
                    Managers = qm.GetQueueManagers(Host_ID)
                };                
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                   
        }

        public JsonResult GetAllQueueManagers()
        {
            var response = new JsonRespnse();
            try
            {
                MFTQueueManager mftManager = new MFTQueueManager();
                response.Result = new QueueManagersJsonResult()
                {
                    Managers =  mftManager.GetAllQueueManagers()
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                   
        }
        #endregion

        #region ADMIN USEREXITSCONFIG
        public ActionResult UserExitS()
        {
            return View("~/Views/MFT/MFTUserExitConfig.cshtml");
        }

        public JsonResult SaveUserExits(UserExits data)
        {
            var response = new JsonRespnse();
            try
            {
                MFTUserExit UEC = new MFTUserExit();
                if (UEC.UserExitConfigs(data))
                {
                    response.Status = JsonResponseStatus.Success;
                }
                else
                {
                    response.Status = JsonResponseStatus.Failure;
                    response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
                }

                MFTCommon mftcommon = new MFTCommon();
                response.Result = new UserExitsJsonResult()
                {
                    UserExits = mftcommon.GetUserExitValues(data.Id.ToString())
                };                
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUserExitTypes()
        {
            var response = new JsonRespnse();
            try
            {
                MFTCommon mftcommon = new MFTCommon();
                response.Result = new UserExitsJsonResult()
                {
                    UserExits = mftcommon.GetUserExitTypes()
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);                   
        }

        public JsonResult GetUserExits()
        {
            var response = new JsonRespnse();
            try
            {
                MFTCommon mftcommon = new MFTCommon();
                response.Result = new UserExitDetailsJsonResult()
                {
                    UserExits = mftcommon.GetUserExitDetails()
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetUserExitValues(string id)
        {
            var response = new JsonRespnse();
            try
            {
                MFTCommon mftcommon = new MFTCommon();
                response.Result = new UserExitsJsonResult()
                {
                    UserExits = mftcommon.GetUserExitValues(id)
                };
                response.Status = JsonResponseStatus.Success;
            }
            catch (Exception ex)
            {
                response.Status = JsonResponseStatus.Failure;
                response.Error = new JsonResponseError() { Message = MFTConstants.Request_Failure_Message };
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        
    }
}
