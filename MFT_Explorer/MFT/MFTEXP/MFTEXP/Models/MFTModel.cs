﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MFTEXP.Models
{
    public class MFTModel
    {
    }
    public class HostConfig
    {
        public int Host_ID { get; set; }
        public string Host_Name { get; set; }
        public string Host_IP { get; set; }
        public int Coord_Port { get; set; }
        public string Coord_Qmgr { get; set; }
        public string Default_Channel { get; set; }
        public string Userid { get; set; }
        public string Agent_QMGR { get; set; }
        public int Agent_QMGR_Port { get; set; }
        public char Active { get; set; }
        public string Action { get; set; }
           

     
    }

    public class Monitordetails
    {
        public string QueueManagerName { get; set; }
        public string MonitorID { get; set; }
        public string MonitorName { get; set; }
        public string AgentName { get; set; }
        public string MonitorStatus { get; set; }
        public string AgentStatus { get; set; }
        public string MonitorXml { get; set; }
              
    }

    public class Agentdetails
    {
        public string queueManager { get; set; }
        public string agentName { get; set; }
        public string AgentStatus { get; set; }
        public string agentType { get; set; }

        //public Dictionary<string, object> Properties { get; set; }
        //public Agentdetails()
        //{
        //    Properties = new Dictionary<string, object>(); 
        //}
    }

    public class ViewProperties
    {
        public String totalRecords { get; set; }
    }

    public class TransferLogs
    {
        public String JobName { get; set; }
        public String TransferID { get; set; }
        public String OverAllResult { get; set; }
        public String SourceSystem { get; set; }
        public String TargetSystem { get; set; }
        public String OverAllResultText { get; set; }
        public String StartTime { get; set; }
        public String EndTime { get; set; }
        public String SourceFile { get; set; }
        public long SourceFileSize { get; set; }
        public String DestinationFile { get; set; }
        public long DestinationFileSize { get; set; }
        public int NumberOfFiles { get; set; }
        public String FileResult { get; set; }
        public String FileResultMessage { get; set; }
        public String Status { get; set; }
        public String TimeElapsed { get; set; }
       
     
    }
    public class TransferLogsModel
    {
        public TransferLogs[] TransferLogs { get; set; }
        public TransferLogPie3D TransferLogPie3DChart { get; set; }
        public TransferLogColumn2D TransferLogColumn2DChart { get; set; }

    }
    public class TransferLogPie3D : Pie3D
    {


    }
    public class TransferLogColumn2D : Column2D
    {


    }
    public class ChartProperties
    {
        public string Caption { get; set; }
        public int ShowLabels { get; set; }
        public int ShowLegend { get; set; }
        public string StartingAngle { get { return "120"; } }
        public string EnableMultiSlicing { get { return "0"; } }
        public string SlicingDistance { get { return "15"; } }
        public string Theme { get { return "fint"; } }
    }

    public class ChartDataProperties
    {
        public string Label { get; set; }
        public string Value { get; set; }
        public string color { get; set; }
    }

    public class ChartBase
    {
        public ChartProperties Chart { get; set; }
        public ChartDataProperties[] Data { get; set; }
    }

    public class Pie3D : ChartBase
    {

    }

    public class Column2D : ChartBase
    {

    }

    public class TrnsTemplate
    {
        public string Template_Code { get; set; }
        public string Template_Description { get; set; }
        public string Template_XML { get; set; }
    }

    public class Transfer_details
    {
        public string Monitor_ID { get; set; }
        public string Monitor_Name { get; set; }
        public string Src_QueMgr { get; set; }

        public string Src_HostName { get; set; }
        public string Src_HostIP { get; set; }
        public string Src_Channel { get; set; }
        public int Src_Port { get; set; }
        public string Src_UserID { get; set; }

        public string Trgt_QueMgr { get; set; }
        public string Src_Agent { get; set; }
        public string Trgt_Agent { get; set; }
        public string resource_type { get; set; }
        public string resource_info { get; set; }
        public string M_Pattern { get; set; }
        public string E_Pattern { get; set; }
        public string Pattern_type { get; set; }
        public string Trans_Mode { get; set; }
        public string Poll_Frequency { get; set; }
        public string Src_file { get; set; }
        public string Trgt_file { get; set; }
        public string Pre_Src { get; set; }
        public string Post_Src { get; set; }
        public string Pre_dest { get; set; }
        public string Post_dest { get; set; }
        public string Job_name { get; set; }
        public int Poll_Interval { get; set; }
        public string reply_Queue { get; set; }
        public string Trg_condition { get; set; }
        public string Src_disposition { get; set; }
        public string Trgt_exists { get; set; }
        public string Trans_priority { get; set; }
        public string Checksum { get; set; }
        public string filesizeb { get; set; }
        public string polls { get; set; }
        public string Source_type { get; set; }
        public string Dest_type { get; set; }
        public string Action { get; set; }
        public string Exists_QueMGR { get; set; }
        public string Exists_AgentName { get; set; }
        public Schedule_details Schedule { get; set; }
    }
    public class Schedule_details
    {
        public string Timebase { get; set; }
        public string Frequency { get; set; }
        public string Interval { get; set; }
        public string Starttime { get; set; }
        public string Endtime { get; set; }
    }

    public class User_Exit
    {
        public string UserExit_type { get; set; }
        public string UserExit_detail { get; set; }
        public string Exit_Params { get; set; }
    }

    public class UserExits
    {
        public int Exit_id { get; set; }
        public int Id { get; set; }
        public string Exit_Value { get; set; }
        public string Action { get; set; }
        public string Exit_Params { get; set; }
    }

    public class MQ_Queues
    {
        public string Queues { get; set; }
        public string CurrentDepth { get; set; }
        public string MaxDepth { get; set; }
        public string Usage { get; set; }
        public string OpenInputCount { get; set; }
        public List<string> messages { get; set; }
        public MQ_Queues()
        {
            messages = new List<string>();
        }
    }


    public class MQ_QueueeManager
    {
        public int QM_ID { get; set; }
        public string QM_Name { get; set; }
        public string QM_Channel { get; set; }
        public int QM_Port { get; set; }
        public string QM_UserID { get; set; }
        public string Is_Agent { get; set; }
        public string Is_Cmnd { get; set; }
        public string Is_Coord { get; set; }
        public string Is_Active { get; set; }
        public int Host_ID { get; set; }
        public string Host_Name { get; set; }
        public string Host_IP { get; set; }
        public string Action { get; set; }
    }
        

}