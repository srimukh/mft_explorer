﻿(function () {
    var myApp = angular.module('QUEMGR', ['ui.bootstrap'])
     .config(['$httpProvider', function ($httpProvider) {
         $httpProvider.defaults.headers.common["AsyncRequestFromClient"] = "true";
     }]);
    myApp.controller('QueMgrController', ['$scope', '$http', '$modal', function ($scope, $http, $modal) {
        var quemgr = this;
        $scope.quemgr = quemgr;
        quemgr.Hostlist = [];
        quemgr.selected_Host = "";
      
        quemgr.ShowManagers = false;
        quemgr.showadd = false;
        quemgr.showtable = false;
        quemgr.Coordinateqm = false;
        quemgr.savebuttonlabel = 'Save';
        quemgr.currentPage = 1;
        quemgr.pageSize = "10";

        quemgr.pleaseWaitDiv = $('<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m"><div class="modal-content"><div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body"><h1>Processing...</h1><div class="progress progress-striped active" style="margin-bottom:0;">' +
            '<div class="progress-bar" style="width: 100%"></div></div></div></div></div></div>');

        quemgr.showPleaseWait = function () {
            quemgr.pleaseWaitDiv.modal();
        };

        quemgr.hidePleaseWait = function () {
            quemgr.pleaseWaitDiv.modal('hide');
        };

        quemgr.resultsObj = {
            totalItems: null,
            currentPage: 1,
            setPage: function (pageNum) {
                quemgr.resultsObj.currentPage = pageNum;
            },
            setPageSize: function (size) {
                quemgr.resultsObj.pageSize = size;
                quemgr.resultsObj.pageChanged();
            },
            pageSize: 10,
            availPageSizes: [10, 20, 30, 40],
            numPages: 9,
            startRecord: 1,
            endRecord: 5,
            pageChanged: function () {
                var tempResults = quemgr.resultsObj.fullResults;
                var start = ((quemgr.resultsObj.currentPage == 1) ? 0 : ((quemgr.resultsObj.currentPage - 1) * quemgr.resultsObj.pageSize));
                var end = start + quemgr.resultsObj.pageSize;
                if (end > tempResults.length) {
                    end = tempResults.length;
                }
                quemgr.resultsObj.startRecord = start + 1;
                quemgr.resultsObj.endRecord = end;
                quemgr.resultsObj.currentResults.details = tempResults.slice(start, end);
            },
            fullResults: null,
            currentResults: { details: null }
        };

        quemgr.QM = {
            QM_ID: '',
            QM_Name: '',
            QM_Channel: quemgr.selected_Host.Default_Channel,
            QM_Port: '',
            QM_UserID: quemgr.selected_Host.Userid,
            Is_Agent: 'Y',
            Is_Cmnd: 'N',
            Is_Coord: 'N',
            Is_Active: 'Y',
            Host_ID: quemgr.selected_Host.Host_ID,
            Action: 'Save'
        }


        quemgr.GetManagers = function () {
            if (quemgr.selected_Host == '' || quemgr.selected_Host == null ) {
                quemgr.ShowManagers = false;
            } else {
                quemgr.showPleaseWait();
                $http({
                    url: GlobalURLs.QueueManagersByHost,
                    method: 'get',
                    params: {HostID: quemgr.selected_Host.Host_ID}
                }).success(function (data, status) {
                    if (data.Status == GlobalConstants.Success) {
                        //quemgr.showDialog('Queue Managers', GlobalConstants.ActionSuccessMessage, 'HostDialog.html');
                        if (data.Result.Managers.length == 0) {
                            quemgr.showDialog('Queue Managers', "No Queue Manager Exists.", 'HostDialog.html');
                        }
                        quemgr.Managers = data.Result.Managers;
                        quemgr.resultsObj.fullResults = data.Result.Managers;
                        quemgr.resultsObj.totalItems = data.Result.Managers.length;
                        quemgr.resultsObj.setPage(1);
                        quemgr.resultsObj.pageChanged();
                        quemgr.results = data.Result.Managers;
                        quemgr.hidePleaseWait();
                        quemgr.showtable = true;
                        quemgr.showadd = false;

                        quemgr.Coordinateqm = false;
                        angular.forEach(quemgr.Managers, function (qmr,ind) {
                            if (qmr.Is_Coord == 'Y') {
                                quemgr.Coordinateqm = true;
                            }
                            
                            
                        });


                    } else {
                        quemgr.showDialog('Queue Managers', data.Error.Message, 'HostDialog.html');
                        //quemgr.showDialog('Queue Managers', data.Error.Message, 'HostDialog.html');
                        quemgr.hidePleaseWait();
                    }
                }).error(function (data, status) {
                    quemgr.hidePleaseWait();
                    quemgr.showDialog('Queue Managers', data, 'HostDialog.html');
                });

                quemgr.ShowManagers = true;
            }
        }

        quemgr.Clear = function () {
            //angular.forEach(quemgr.QM, function (value, key) {
            //    quemgr.QM[key] = '';
            //});
            quemgr.QM = {
                QM_ID: '',
                QM_Name: '',
                QM_Channel: quemgr.selected_Host.Default_Channel,
                QM_Port: '',
                QM_UserID: quemgr.selected_Host.Userid,
                Is_Agent: 'Y',
                Is_Cmnd: 'N',
                Is_Coord: 'N',
                Is_Active: 'Y',
                Host_ID: quemgr.selected_Host.Host_ID,
                Action: 'Save'
            }
        };

        quemgr.CallAdd = function () {
            quemgr.showadd = true;
            quemgr.savebuttonlabel = 'Save';
            quemgr.showtable = false;
            quemgr.Clear();
            quemgr.QM.QM_ID = '0';
            quemgr.QM.Action = 'Save';
        };
   
        quemgr.checkValidInputs = function (value) {
            if (value.QM_Name != '' && value.WM_Channel != '' && value.QM_Port != '' && value.QM_UserID != '') {
                return true;
            }
            else {
                return false;
            }
        };

  
        quemgr.Save = function () {
            quemgr.showPleaseWait();
            $http({
                url: GlobalURLs.SaveQueueManager,
                method: 'POST',
                data: quemgr.QM 
            }).success(function (data, status) {
                if (data.Result.Managers != null) {

                    if (data.Status == GlobalConstants.Success) {
                        quemgr.showDialog('Queue Managers', GlobalConstants.ActionSuccessMessage, 'HostDialog.html')
                    } else {
                        quemgr.showDialog('Queue Managers', data.Error.Message, 'HostDialog.html')
                    }
                    quemgr.Managers = data.Result.Managers;
                    quemgr.resultsObj.fullResults = data.Result.Managers;
                    quemgr.resultsObj.totalItems = data.Result.Managers.length;
                    quemgr.resultsObj.setPage(1);
                    quemgr.resultsObj.pageChanged();
                    quemgr.results = data.Result.Managers;
                    quemgr.hidePleaseWait();
                    quemgr.showtable = true;
                    quemgr.showadd = false;

                    quemgr.Coordinateqm = false;
                    angular.forEach(quemgr.Managers, function (qmr, ind) {
                        if (qmr.Is_Coord == 'Y') {
                            quemgr.Coordinateqm = true;
                        }
                    });
                }
                quemgr.Clear();
                quemgr.showadd = false;
                quemgr.showtable = true;
                quemgr.hidePleaseWait();
            }).error(function (data, status) {
                quemgr.hidePleaseWait();
                quemgr.showDialog('Queue Managers', data, 'HostDialog.html');
            });

        };


        quemgr.Cancel = function () {
            quemgr.Clear();
            quemgr.showadd = false;
            quemgr.showtable = true;

            quemgr.Coordinateqm = false;
            angular.forEach(quemgr.Managers, function (qmr, ind) {
                if (qmr.Is_Coord == 'Y') {
                    quemgr.Coordinateqm = true;
                }
            });
        };

        quemgr.PerformEditAction = function (qm, index) {
            quemgr.QM.QM_ID = qm.QM_ID;
            quemgr.QM.QM_Name = qm.QM_Name;
            quemgr.QM.QM_Channel = qm.QM_Channel;
            quemgr.QM.QM_Port = qm.QM_Port;
            quemgr.QM.QM_UserID = qm.QM_UserID;
            quemgr.QM.Is_Agent = qm.Is_Agent;
            quemgr.QM.Is_Cmnd = qm.Is_Cmnd;
            quemgr.QM.Is_Coord = qm.Is_Coord;
            quemgr.QM.Is_Active = qm.Is_Active;
            quemgr.QM.Host_ID = quemgr.selected_Host.Host_ID;
            quemgr.showadd = true;
            quemgr.savebuttonlabel = 'Update';
            quemgr.QM.Action = 'Update';
            quemgr.showtable = false;
            quemgr.updateindex = index;

            if (quemgr.QM.Is_Coord == 'Y') {
                quemgr.Coordinateqm = false;
            }

        };

        quemgr.PerformAddorUpdate = function () {
            if (quemgr.checkValidInputs(quemgr.QM)) {
                quemgr.Save();
            }
            else {
                quemgr.showDialog('Queue Managers', 'Please Enter All Config Values', 'HostDialog.html')
            }
        };

        quemgr.PerformDeleteAction = function (value) {
            quemgr.showPleaseWait();
            $http({
                url: GlobalURLs.DeleteQueueManager,
                method: 'POST',
                params: { QMID: value.QM_ID,Host_ID: value.Host_ID }
            }).success(function (data, status) {
                if (data.Status == GlobalConstants.Success) {
                    quemgr.showDialog('Queue Managers', GlobalConstants.ActionSuccessMessage, 'HostDialog.html')
                } else {
                    quemgr.showDialog('Queue Managers', data.Error.Message, 'HostDialog.html')
                }
                quemgr.Managers = data.Result.Managers;
                quemgr.resultsObj.fullResults = data.Result.Managers;
                quemgr.resultsObj.totalItems = data.Result.Managers.length;
                quemgr.resultsObj.setPage(1);
                quemgr.resultsObj.pageChanged();
                quemgr.results = data.Result.Managers;

                quemgr.Clear();
                quemgr.showadd = false;
                quemgr.showtable = true;
                quemgr.Coordinateqm = false;
                angular.forEach(quemgr.Managers, function (qmr,ind) {
                    if (qmr.Is_Coord == 'Y') {
                        quemgr.Coordinateqm = true;
                    }
                            
                            
                });

                quemgr.hidePleaseWait();
            }).error(function (data, status) {
                quemgr.hidePleaseWait();
                quemgr.showDialog('Queue Managers', data, 'HostDialog.html')

            });
        };

        quemgr.GetHosts = function () {
                quemgr.showPleaseWait();
                $http({
                    url: GlobalURLs.HostsList,
                    method: 'get'
                }).success(function (data, status) {
                    if (data.Status == GlobalConstants.Success) {
                        if (data.Result.Hosts.length == 0) {
                            quemgr.showDialog('Queue Managers', "No Host Exists, please enter Host.", 'HostDialog.html')
                        }
                        quemgr.Hostlist = data.Result.Hosts;
                        quemgr.hidePleaseWait();
                    }
                    else
                    {
                        quemgr.hidePleaseWait();
                        quemgr.showDialog('Queue Managers', data.Error.Message, 'HostDialog.html')
                    }
                }).error(function (data, status) {
                    quemgr.hidePleaseWait();
                    quemgr.showDialog('Queue Managers', data, 'HostDialog.html')
                });           
           

        };

        quemgr.showDialog = function (heading, details, template) {
            var dialog = $modal.open({
                templateUrl: template,
                controller: 'MDialogCtrl',
                size: 'md',
                resolve: {
                    heading: function () {
                        return heading;
                    },
                    details: function () {
                        return details;
                    }
                }
            });

        };

        quemgr.DeleteDialog = function (details) {
            var dialog = $modal.open({
                templateUrl: 'DeleteDialog.html',
                controller: 'MDialogCtrl',
                size: 'md',
                resolve: {
                    heading: function () {
                        return "Are you sure to delete " + details.QM_Name + " ?";
                    },
                    details: function () {
                        return details;
                    }
                }
            });
            dialog.result.then(function (result) {
                if (result == 'Yes') {
                    quemgr.PerformDeleteAction(details);
                }
            });
        };


    }
    ]);

    myApp.controller('MDialogCtrl', function ($scope, $modalInstance, heading, details) {
        var dlgCtrl = this;
        $scope.dlgCtrl = dlgCtrl;
        dlgCtrl.heading = heading;
        dlgCtrl.details = details;
        dlgCtrl.Yes = function () {
            $modalInstance.close('Yes');
        };
        dlgCtrl.Close = function () {
            $modalInstance.dismiss('cancel');
        };
    });

})();