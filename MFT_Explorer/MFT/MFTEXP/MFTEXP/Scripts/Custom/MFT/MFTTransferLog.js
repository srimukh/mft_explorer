﻿(function () {

    var myApp = angular.module('Transfer', ['ui.bootstrap', 'ng-fusioncharts'])
     .config(['$httpProvider', function ($httpProvider) {
         $httpProvider.defaults.headers.common["AsyncRequestFromClient"] = "true";
     }]);
   // var myApp = angular.module('Transfer', ['ui.bootstrap', 'ng-fusioncharts'])
    myApp.factory('SharedService', function ($rootScope) {
        var dates = {
            startdate: '',
            enddate: ''
        };
        return {
            setstartdate: function (sdate) {
                dates.startdate = sdate;
            },
            getstartdate: function () {
                return dates.startdate;
            },
            setenddate: function (edate) {
                dates.enddate = edate;
            },
            getenddate: function () {
                return dates.enddate;
            }
        };

    });
    myApp.controller('StartDatepickerCtrl', ['$scope', 'SharedService', function ($scope, SharedService) {

        var Strt = this;
        Strt.valuationDate = new Date();
        Strt.valuationDatePickerIsOpen = false;
        Strt.opens = [];
        $scope.$watch(function () {
            SharedService.setstartdate(Strt.valuationDate);
            return Strt.valuationDatePickerIsOpen;
        },
            function (value) {
                Strt.opens.push("valuationDatePickerIsOpen: " + value + " at: " + new Date());
            });

        Strt.valuationDatePickerOpen = function ($event) {
            if ($event) {
                $event.preventDefault();
                $event.stopPropagation();
            }
            this.valuationDatePickerIsOpen = true;

        };
    }]);

    myApp.controller('EndDatepickerCtrl', ['$scope', 'SharedService', function ($scope, SharedService) {

        var Ed = this;
        Ed.valuationDate = new Date();
        Ed.valuationDatePickerIsOpen = false;
        Ed.opens = [];
        $scope.$watch(function () {
            SharedService.setenddate(Ed.valuationDate);
            return Ed.valuationDatePickerIsOpen;
        },
        function (value) {
            Ed.opens.push("valuationDatePickerIsOpen: " + value + " at: " + new Date());
        });
        Ed.valuationDatePickerOpen = function ($event) {

            if ($event) {
                $event.preventDefault();
                $event.stopPropagation();
            }
            this.valuationDatePickerIsOpen = true;
        };

    }]);

    myApp.controller('MFTTransferLogController', ['$scope', '$http', 'SharedService', '$modal', '$filter', function ($scope, $http, SharedService, $modal, $filter) {
        var TransCtrl = this;
        TransCtrl.TransferLogs = [];
        TransCtrl.JobNames = [];
        TransCtrl.SelectedJob = "";
        TransCtrl.TransferLogsChartColumn2DSource = {};
        TransCtrl.showGrid = false;
        TransCtrl.currentPage = 1;
        TransCtrl.pageSize = "10";
        TransCtrl.starttime = "00";
        TransCtrl.sortType;
        TransCtrl.sortReverse = true;
        TransCtrl.search;
 

        TransCtrl.ReplayOptions = {
            TargetExists: 'error'
        };

        TransCtrl.pleaseWaitDiv = $('<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
       '<div class="modal-dialog modal-m"><div class="modal-content"><div class="modal-header"><h3 style="margin:0;"></h3></div>' +
           '<div class="modal-body"><h1>Processing...</h1><div class="progress progress-striped active" style="margin-bottom:0;">' +
           '<div class="progress-bar" style="width: 100%"></div></div></div></div></div></div>');
        //For Grid Pagination
        TransCtrl.resultsObj = {
            totalItems: null,

            currentPage: 1,
            setPage: function (pageNum) {
                TransCtrl.resultsObj.currentPage = pageNum;
            },
            setPageSize: function (size) {
                TransCtrl.resultsObj.pageSize = size;
                TransCtrl.resultsObj.pageChanged();
            },
            pageSize: 10,
            availPageSizes: [10, 20, 30, 40],
            numPages: 9,
            startRecord: 1,
            endRecord: 5,
            pageChanged: function () {

                var tempResults = TransCtrl.resultsObj.fullResults;
                var start = ((TransCtrl.resultsObj.currentPage == 1) ? 0 : ((TransCtrl.resultsObj.currentPage - 1) * TransCtrl.resultsObj.pageSize));
                var end = start + TransCtrl.resultsObj.pageSize;
                if (end > tempResults.length) {
                    end = tempResults.length;
                }

                TransCtrl.resultsObj.startRecord = start + 1;
                TransCtrl.resultsObj.endRecord = end;
                TransCtrl.resultsObj.currentResults.details = tempResults.slice(start, end);

            },
            fullResults: null,
            currentResults: { details: null }
        };

     
        TransCtrl.show=function(){
            TransCtrl.showGrid=false;
        };

  
        TransCtrl.MyFilter = {};

        TransCtrl.pieevent = {
            dataplotclick: function (ev, props) {
                TransCtrl.MyFilter = angular.lowercase(props.categoryLabel);
                TransCtrl.filteredRecords = $filter('filter')(TransCtrl.TransferLogs, { 'Status': TransCtrl.MyFilter }, true);
                TransCtrl.resultsObj.fullResults = TransCtrl.filteredRecords;
                TransCtrl.resultsObj.totalItems = TransCtrl.filteredRecords.length;
                TransCtrl.resultsObj.setPage(1);
                TransCtrl.resultsObj.pageChanged();
                TransCtrl.resultsObj.currentPage = 1;
                TransCtrl.results = TransCtrl.filteredRecords;
                $scope.$apply();

            }
        };

        var date = new Date();

        TransCtrl.GetTransferLogs = function () {
               TransCtrl.startdate = SharedService.getstartdate();
               TransCtrl.enddate = SharedService.getenddate();

                if (TransCtrl.startdate == null || TransCtrl.enddate == null)
                {
                    TransCtrl.showDialog('Transfer Log', 'Please enter the date', 'TransferDialog.html', 'md');
                    TransCtrl.showGrid = false;
                }
                else if (TransCtrl.enddate < TransCtrl.startdate) {
                    TransCtrl.showDialog('Transfer Log', 'End date should be greater than Start date', 'TransferDialog.html', 'md');
                    TransCtrl.showGrid = false;
                }
                else {
                    TransCtrl.showPleaseWait();
                    TransCtrl.FormattedStartDate = $filter('date')(new Date(TransCtrl.startdate), 'yyyy-MM-dd'); //'yyyy-MM-dd HH:MM:ss'
                    TransCtrl.FormattedEndDate = $filter('date')(new Date(TransCtrl.enddate), 'yyyy-MM-dd'); //'yyyy-MM-dd HH:MM:ss'
                    TransCtrl.resultsObj.fullResults = [];
                    TransCtrl.resultsObj.totalItems = [];
                    $http({
                        url: GlobalURLs.TransferLogsList,
                        method: 'GET',
                        params: { fromDate: TransCtrl.FormattedStartDate, toDate: TransCtrl.FormattedEndDate, JobName: TransCtrl.SelectedJob }
                    }).success(function (data, status) {
                        TransCtrl.hidePleaseWait();
                        if (data.Status == GlobalConstants.Success) {
                            if (data.Result.LogData.TransferLogs.length == 0) {
                                TransCtrl.showGrid = false;
                                TransCtrl.showDialog('Transfer Log', "No logs are found with the selection.", 'TransferDialog.html', 'md')
                            } else {
                                TransCtrl.showGrid = true;
                            }
                            TransCtrl.MyFilter = {};
                            TransCtrl.TransferLogs = data.Result.LogData.TransferLogs;
                            TransCtrl.resultsObj.fullResults = data.Result.LogData.TransferLogs;
                            TransCtrl.resultsObj.totalItems = data.Result.LogData.TransferLogs.length;
                            TransCtrl.resultsObj.setPage(1);
                            TransCtrl.resultsObj.pageChanged();
                            TransCtrl.results = data.Result.LogData.TransferLogs;
                            TransCtrl.TransferLogsChartColumn2DSource = data.Result.LogData.TransferLogColumn2DChart;
                        }
                        else
                        {
                            TransCtrl.showDialog('Transfer Log', data.Error.Message, 'TransferDialog.html', 'md')
                        }
                    }).error(function (data, status) {
                        TransCtrl.showDialog('Transfer Log', data, 'TransferDialog.html', 'md')

                    });

                }
            
        };

        TransCtrl.GetJobNames = function () {
            TransCtrl.showPleaseWait();
            $http({
                url: GlobalURLs.JobNamesList,
                method: 'GET'
           
            }).success(function (data, status) {
                TransCtrl.hidePleaseWait();
                if (data.Status == GlobalConstants.Success) {               
                    TransCtrl.JobNames = data.Result.JobNames;
                }
                else {
                    TransCtrl.showDialog('Transfer Log', data.Error.Message, 'TransferDialog.html', 'md')
                }
            }).error(function (data, status) {
                TransCtrl.showDialog('Transfer Log', data, 'TransferDialog.html', 'md')

            });
        }

        TransCtrl.ReplayDialog = function (replayrow) {
            TransCtrl.ReplayOptions.TargetExists = 'error';
            var dialog = $modal.open({
                templateUrl: 'ReplayDialog.html',
                controller: 'TransCtrl',
                size: 'md',
                resolve: {
                    heading: function () {
                        return 'Confirm Replay ...';
                    },
                    details: function () {
                        return replayrow;
                    }
                }
            });
            dialog.result.then(function (result) {
                if (result.TargetExists == 'overwrite') {
                    TransCtrl.ReplayOptions.TargetExists = 'overwrite';
                }
                TransCtrl.PerformReplay(replayrow);
            });
        };



        TransCtrl.PerformReplay = function (replayrow) {
           
            TransCtrl.showPleaseWait();
            $http({
                url: GlobalURLs.PerformReplay,
                method: 'POST',
                params: { JobName: replayrow.JobName, Transfer_ID: replayrow.TransferID, TargetExists: TransCtrl.ReplayOptions.TargetExists }
            }).success(function (data, status) {
                if (data.Status == GlobalConstants.Success) {
                    TransCtrl.hidePleaseWait();
                    TransCtrl.showDialog('Transfer Log', GlobalConstants.ActionSuccessMessage, 'TransferDialog.html', 'md');
                } else {
                    TransCtrl.showDialog('Transfer Log', data.Error.Message, 'TransferDialog.html', 'md');
                }
                TransCtrl.ReplayOptions.TargetExists = 'error';
            }).error(function (data, status) {
                TransCtrl.hidePleaseWait();
                TransCtrl.showDialog('Transfer Log', data, 'TransferDialog.html','md');
                TransCtrl.ReplayOptions.TargetExists = 'error';
            });

        };

        TransCtrl.showPleaseWait = function () {
            TransCtrl.pleaseWaitDiv.modal();
        };
        TransCtrl.hidePleaseWait = function () {
            TransCtrl.pleaseWaitDiv.modal('hide');
        };

        TransCtrl.showDialog = function (heading, details, template,size) {
            var dialog = $modal.open({
                templateUrl: template,
                controller: 'TransCtrl',
                size: size,
                resolve: {
                    heading: function () {
                        return heading;
                    },
                    details: function () {
                        return details;
                    }
                }
            });

        };

        TransCtrl.showtransDialog = function (heading, details, template) {
            var dialog = $modal.open({
                templateUrl: template,
                controller: 'TransCtrl',
                size: 'md',
                resolve: {
                    heading: function () {
                        return heading;
                    },
                    details: function () {
                        return details;
                    }
                }
            });

        };

        TransCtrl.Showdetails = function (selectedRowData) {
            TransCtrl.showDialog('Transfer Log', selectedRowData, 'Transferdetails.html','lg');
        };


    }
    ]);

    myApp.controller('TransCtrl', function ($scope, $modalInstance, heading, details) {
        var dlgCtrl = this;
        $scope.dlgCtrl = dlgCtrl;
        dlgCtrl.heading = heading;
        dlgCtrl.details = details;
        dlgCtrl.ReplayOptions = {
            TargetExists: 'error'
        };

        dlgCtrl.Replay = function () {
            $modalInstance.close(dlgCtrl.ReplayOptions);
        };
        dlgCtrl.Close = function () {
            $modalInstance.dismiss('cancel');
        };

    });

    //myApp.controller('TransCtrl', function ($scope, $modalInstance, heading, details) {
    //    var trans = this;
    //    $scope.trans = trans;
    //    trans.heading = heading;
    //    trans.details = details;
    //    trans.Close = function () {
    //        $modalInstance.dismiss('cancel');
    //    };

    //});

})();