﻿(function () {
    var myApp = angular.module('Agents', ['ui.bootstrap'])
     .config(['$httpProvider', function ($httpProvider) {
         $httpProvider.defaults.headers.common["AsyncRequestFromClient"] = "true";
     }]);

    myApp.controller('AgentController', ['$scope', '$http', '$modal', function ($scope, $http, $modal) {
        var Agt = this;
        $scope.Agt = Agt;
        Agt.sortType;
        Agt.sortReverse = true;
        Agt.search;
        Agt.pleaseWaitDiv = $('<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m"><div class="modal-content"><div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body"><h1>Processing...</h1><div class="progress progress-striped active" style="margin-bottom:0;">' +
            '<div class="progress-bar" style="width: 100%"></div></div></div></div></div></div>');

        Agt.GetAgents = function () {
            Agt.showPleaseWait();
                $http({
                    url: GlobalURLs.AgentsList,
                    method: 'get'
                }).success(function (data, status) {
                    Agt.hidePleaseWait();
                    if (data.Status == 0) {
                        Agt.resultsObj.fullResults = data.Result.Agents;
                        Agt.resultsObj.totalItems = data.Result.Agents.length;
                        Agt.resultsObj.setPage(1);
                        Agt.resultsObj.pageChanged();
                        Agt.results = data.Result.Agents;
                    }
                    else
                    {
                        Agt.showDialog('Agent Details', data.Error.Message, 'AgentDialog.html', 'md')
                    }
                }).error(function (data, status) {
                    Agt.hidePleaseWait();
                    Agt.showDialog('Agent Details', data, 'AgentDialog.html','md')
                });
            
        };

        Agt.currentPage = 1;
        Agt.pageSize = "10";
        Agt.resultsObj = {
            totalItems: null,
            currentPage: 1,
            setPage: function (pageNum) {
                Agt.resultsObj.currentPage = pageNum;
            },
            setPageSize: function (size) {
                Agt.resultsObj.pageSize = size;
                Agt.resultsObj.pageChanged();
            },
            pageSize: 10,
            availPageSizes: [10, 20, 30, 40],
            numPages: 9,
            startRecord: 1,
            endRecord: 5,
            pageChanged: function () {
                var tempResults = Agt.resultsObj.fullResults;
                var start = ((Agt.resultsObj.currentPage == 1) ? 0 : ((Agt.resultsObj.currentPage - 1) * Agt.resultsObj.pageSize));
                var end = start + Agt.resultsObj.pageSize;
                if (end > tempResults.length) {
                    end = tempResults.length;
                }
                Agt.resultsObj.startRecord = start + 1;
                Agt.resultsObj.endRecord = end;
                Agt.resultsObj.currentResults.details = tempResults.slice(start, end);
            },
            fullResults: null,
            currentResults: { details: null }
        };

        Agt.showPleaseWait = function () {
           
           Agt.pleaseWaitDiv.modal();
        };

        Agt.hidePleaseWait = function () {
            Agt.pleaseWaitDiv.modal('hide');
        };

        Agt.ViewXML = function (Agtheader, AgentProperties) {
           
            Agt.showDialog(Agtheader, AgentProperties, 'ViewXML.html','lg')
        };


        Agt.showDialog = function (heading, details, template,size) {
            var dialog = $modal.open({
                templateUrl: template,
                controller: 'XMLDialogCtrl',
                size: size,
                resolve: {
                    heading: function () {
                        return heading;
                    },
                    details: function () {
                        return details;
                    }
                }
            });

        };
    }
    ]);

    myApp.controller('XMLDialogCtrl', function ($scope, $modalInstance, heading, details) {
        var dlgCtrl = this;
        $scope.dlgCtrl = dlgCtrl;
        dlgCtrl.heading = heading;
        dlgCtrl.details = details;
        dlgCtrl.Close = function () {
            $modalInstance.dismiss('cancel');
        };
    });

})();

