﻿(function () {
    var myApp = angular.module('HOSTCFG', ['ui.bootstrap'])
     .config(['$httpProvider', function ($httpProvider) {
         $httpProvider.defaults.headers.common["AsyncRequestFromClient"] = "true";
     }]);
    myApp.controller('HostController', ['$scope', '$http', '$modal', function ($scope, $http, $modal) {
        var hostcfg = this;
        $scope.hostcfg = hostcfg;
        hostcfg.Hostlist = [];
        hostcfg.showadd = false;
        hostcfg.showtable = false;
        hostcfg.savebuttonlabel = 'Save';
        hostcfg.currentPage = 1;
        hostcfg.pageSize = "10";

        hostcfg.pleaseWaitDiv = $('<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m"><div class="modal-content"><div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body"><h1>Processing...</h1><div class="progress progress-striped active" style="margin-bottom:0;">' +
            '<div class="progress-bar" style="width: 100%"></div></div></div></div></div></div>');

        hostcfg.showPleaseWait = function () {
            hostcfg.pleaseWaitDiv.modal();
        };

        hostcfg.hidePleaseWait = function () {
            hostcfg.pleaseWaitDiv.modal('hide');
        };

        hostcfg.resultsObj = {
            totalItems: null,
            currentPage: 1,
            setPage: function (pageNum) {
                hostcfg.resultsObj.currentPage = pageNum;
            },
            setPageSize: function (size) {
                hostcfg.resultsObj.pageSize = size;
                hostcfg.resultsObj.pageChanged();
            },
            pageSize: 10,
            availPageSizes: [10, 20, 30, 40],
            numPages: 9,
            startRecord: 1,
            endRecord: 5,
            pageChanged: function () {
                var tempResults = hostcfg.resultsObj.fullResults;
                var start = ((hostcfg.resultsObj.currentPage == 1) ? 0 : ((hostcfg.resultsObj.currentPage - 1) * hostcfg.resultsObj.pageSize));
                var end = start + hostcfg.resultsObj.pageSize;
                if (end > tempResults.length) {
                    end = tempResults.length;
                }
                hostcfg.resultsObj.startRecord = start + 1;
                hostcfg.resultsObj.endRecord = end;
                hostcfg.resultsObj.currentResults.details = tempResults.slice(start, end);
            },
            fullResults: null,
            currentResults: { details: null }
        };

        hostcfg.Host = {
            Host_id:'',
            Host_Name: '',
            Host_IP: '',
            Coord_Port: '',
            Coord_Qmgr: '',
            Default_Channel: '',
            Host_Userid: '',
            Agent_QMGR: '',
            Agent_QMGR_Port: '',
            active: '',
            action:''
        }


        hostcfg.Clear = function () {
            angular.forEach(hostcfg.Host, function (value, key) {
                hostcfg.Host[key] = '';
            });
        };

        hostcfg.CallAdd = function () {
            hostcfg.showadd = true;
            hostcfg.savebuttonlabel = 'Save';
            hostcfg.showtable = false;
            hostcfg.Clear();
            hostcfg.Host.Host_id = '0';
            hostcfg.Host.action = 'Save';
        };
   
        hostcfg.checkValidInputs = function (value) {
            if (value.Host_Name != '' && value.Host_IP != '' && value.Coord_Port != 0 && value.Coord_Port != null && value.Coord_Qmgr != '' && value.Default_Channel != '' && value.Agent_QMGR != '' && value.Agent_QMGR_Port != 0 && value.Agent_QMGR_Port != null && value.Userid != '') {
                return true;
            }
            else {
                return false;
            }
        };

  
        hostcfg.Save = function () {
            hostcfg.showPleaseWait();
            $http({
                url: GlobalURLs.SaveHost,
                method: 'POST',
                data: hostcfg.Host
            }).success(function (data, status) {
                if (data.Status == GlobalConstants.Success) {
                    hostcfg.showDialog('Host Configuration', GlobalConstants.ActionSuccessMessage, 'HostDialog.html')
                } else {
                    hostcfg.showDialog('Host Configuration', GlobalConstants.ActionFailureMessage, 'HostDialog.html')
                }
                
                hostcfg.resultsObj.fullResults = data.Result.Hosts;
                hostcfg.resultsObj.totalItems = data.Result.Hosts.length;
                hostcfg.resultsObj.setPage(1);
                hostcfg.resultsObj.pageChanged();
                hostcfg.results = data.Result.Hosts;

                hostcfg.Clear();
                hostcfg.showadd = false;
                hostcfg.showtable = true;
                hostcfg.hidePleaseWait();
            }).error(function (data, status) {
                hostcfg.hidePleaseWait();
                hostcfg.showDialog('Host Configuration', data, 'HostDialog.html')

            });
        };


        hostcfg.Cancel = function () {
            hostcfg.Clear();
            hostcfg.showadd = false;
            hostcfg.showtable = true;
        };

        hostcfg.PerformEditAction = function (hostitem, index) {
            hostcfg.Host.Host_Name = hostitem.Host_Name;
            hostcfg.Host.Host_IP = hostitem.Host_IP;
            hostcfg.Host.Coord_Port = hostitem.Coord_Port;
            hostcfg.Host.Coord_Qmgr = hostitem.Coord_Qmgr;
            hostcfg.Host.Default_Channel = hostitem.Default_Channel;
            hostcfg.Host.Userid = hostitem.Userid;
            hostcfg.Host.Agent_QMGR = hostitem.Agent_QMGR;
            hostcfg.Host.Agent_QMGR_Port = hostitem.Agent_QMGR_Port;
            hostcfg.Host.Host_id = hostitem.Host_ID;
            hostcfg.Host.active = hostitem.Active;
            hostcfg.showadd = true;
            hostcfg.savebuttonlabel = 'Update';
            hostcfg.Host.action = 'Update';
            hostcfg.showtable = false;
            hostcfg.updateindex = index;

        };

        hostcfg.PerformAddorUpdate = function () {
            if (hostcfg.checkValidInputs(hostcfg.Host)) {
                hostcfg.Save();
            }
            else {
                hostcfg.showDialog('Host Configuration', 'Please Enter All Config Values', 'HostDialog.html')
            }
        };

        hostcfg.PerformDeleteAction = function (value) {
            hostcfg.showPleaseWait();
            $http({
                url: GlobalURLs.DeleteHost,
                method: 'POST',
                params: { hostid: value.Host_ID }
            }).success(function (data, status) {
                if (data.Status == GlobalConstants.Success) {
                    hostcfg.showDialog('Host Configuration', GlobalConstants.ActionSuccessMessage, 'HostDialog.html')
                } else {
                    hostcfg.showDialog('Host Configuration', GlobalConstants.ActionFailureMessage, 'HostDialog.html')
                }

                hostcfg.resultsObj.fullResults = data.Result.Hosts;
                hostcfg.resultsObj.totalItems = data.Result.Hosts.length;
                hostcfg.resultsObj.setPage(1);
                hostcfg.resultsObj.pageChanged();
                hostcfg.results = data.Result.Hosts;

                hostcfg.Clear();
                hostcfg.showadd = false;
                hostcfg.showtable = true;
                hostcfg.hidePleaseWait();
            }).error(function (data, status) {
                hostcfg.hidePleaseWait();
                hostcfg.showDialog('Host Configuration', data, 'HostDialog.html')

            });
        };

                hostcfg.GetHosts = function () {
                hostcfg.showPleaseWait();
                $http({
                    url: GlobalURLs.HostsList,
                    method: 'get'
                }).success(function (data, status) {
                    if (data.Status == GlobalConstants.Success) {
                        if (data.Result.Hosts != null) {
                            hostcfg.Hostlist = data.Result.Hosts;
                        }
                        else {                            
                            hostcfg.showDialog('Host Configuration', data.Error.Message, 'HostDialog.html')
                        }
                        hostcfg.resultsObj.fullResults = data.Result.Hosts;
                        hostcfg.resultsObj.totalItems = data.Result.Hosts.length;
                        hostcfg.resultsObj.setPage(1);
                        hostcfg.resultsObj.pageChanged();
                        hostcfg.results = data.Result.Hosts;
                        hostcfg.hidePleaseWait();
                        hostcfg.showtable = true;
                        hostcfg.showadd = false;
                    }
                    else
                    {
                        hostcfg.hidePleaseWait();
                        hostcfg.showDialog('Host Configuration', data.ErrorMsg, 'HostDialog.html')

                    }
                }).error(function (data, status) {
                    hostcfg.hidePleaseWait();
                    hostcfg.showDialog('Host Configuration', data, 'HostDialog.html')
                });
            
           

        };

        hostcfg.showDialog = function (heading, details, template) {
            var dialog = $modal.open({
                templateUrl: template,
                controller: 'MDialogCtrl',
                size: 'md',
                resolve: {
                    heading: function () {
                        return heading;
                    },
                    details: function () {
                        return details;
                    }
                }
            });

        };

        hostcfg.DeleteDialog = function (details) {
            var dialog = $modal.open({
                templateUrl: 'DeleteDialog.html',
                controller: 'MDialogCtrl',
                size: 'md',
                resolve: {
                    heading: function () {
                        return "Are you sure to delete " + details.Host_Name + " ?";
                    },
                    details: function () {
                        return details;
                    }
                }
            });
            dialog.result.then(function (result) {
                if (result == 'Yes') {
                    hostcfg.PerformDeleteAction(details);
                }
            });
        };


    }
    ]);

    myApp.controller('MDialogCtrl', function ($scope, $modalInstance, heading, details) {
        var dlgCtrl = this;
        $scope.dlgCtrl = dlgCtrl;
        dlgCtrl.heading = heading;
        dlgCtrl.details = details;
        dlgCtrl.Yes = function () {
            $modalInstance.close('Yes');
        };
        dlgCtrl.Close = function () {
            $modalInstance.dismiss('cancel');
        };
    });

})();