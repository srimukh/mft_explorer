﻿
(function () {
    var myApp = angular.module('Monitors', ['ui.bootstrap'])
     .config(['$httpProvider', function ($httpProvider) {
         $httpProvider.defaults.headers.common["AsyncRequestFromClient"] = "true";
     }]);
    myApp.controller('MonitorController', ['$scope', '$http', '$modal', function ($scope, $http, $modal) {
        var Mntr = this;
        $scope.Mntr = Mntr;
        Mntr.sortType;
        Mntr.sortReverse = true;
        Mntr.search;
        Mntr.showMonitors = false;
        Mntr.pleaseWaitDiv = $('<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m"><div class="modal-content"><div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body"><h1>Processing...</h1><div class="progress progress-striped active" style="margin-bottom:0;">' +
            '<div class="progress-bar" style="width: 100%"></div></div></div></div></div></div>');

        Mntr.HideStatus = 'Stopped';

        Mntr.RefreshMonitor = function () {
            Mntr.GetMonitors();
        };

        Mntr.currentPage = 1;
        Mntr.pageSize = "10";

        Mntr.resultsObj = {
            totalItems: null,
            currentPage: 1,
            setPage: function (pageNum) {
                Mntr.resultsObj.currentPage = pageNum;
            },
            setPageSize: function (size) {
                Mntr.resultsObj.pageSize = size;
                Mntr.resultsObj.pageChanged();
            },
            pageSize: 10,
            availPageSizes: [10, 20, 30, 40],
            numPages: 9,
            startRecord: 1,
            endRecord: 5,
            pageChanged: function () {
                var tempResults = Mntr.resultsObj.fullResults;
                var start = ((Mntr.resultsObj.currentPage == 1) ? 0 : ((Mntr.resultsObj.currentPage - 1) * Mntr.resultsObj.pageSize));
                var end = start + Mntr.resultsObj.pageSize;
                if (end > tempResults.length) {
                    end = tempResults.length;
                }
                Mntr.resultsObj.startRecord = start + 1;
                Mntr.resultsObj.endRecord = end;
                Mntr.resultsObj.currentResults.details = tempResults.slice(start, end);
            },
            fullResults: null,
            currentResults: { details: null }
        };

        Mntr.GetMonitors = function () {
                Mntr.showPleaseWait();
                $http({
                    url: GlobalURLs.MonitorsList,
                    method: 'get'
                }).success(function (data, status) {
                    Mntr.hidePleaseWait();
                    if (data.Status == GlobalConstants.Success) {
                        Mntr.resultsObj.fullResults = data.Result.Monitors;
                        Mntr.resultsObj.totalItems = data.Result.Monitors.length;
                        Mntr.resultsObj.setPage(1);
                        Mntr.resultsObj.pageChanged();
                        Mntr.results = data.Result.Monitors;
                    }
                    else
                    {
                        Mntr.showDialog('Monitor Status', data.Error.Message, 'MonitorDialog.html')
                    }
                }).error(function (data, status) {
                    Mntr.hidePleaseWait();
                    Mntr.showDialog('Monitor Status', data, 'MonitorDialog.html')
                });
           
        };

        Mntr.PerformAction = function (itemdata, action) {
                    Mntr.showPleaseWait();
                    $http({
                        url: GlobalURLs.PerformMonitorAction,
                        method: 'get',
                        params: { QMGR: itemdata.QueueManagerName, MID: itemdata.MonitorID, MName: itemdata.MonitorName, AName: itemdata.AgentName, actiontype: action }
                    }).success(function (data, status) {
                        if (data.Status == GlobalConstants.Success) {
                            Mntr.hidePleaseWait();
                            Mntr.resultsObj.fullResults = data.Result.Monitors;
                            Mntr.resultsObj.totalItems = data.Result.Monitors.length;
                            Mntr.resultsObj.setPage(1);
                            Mntr.resultsObj.pageChanged();
                            Mntr.results = data.Result.Monitors;
                            Mntr.showDialog('Monitor Status', GlobalConstants.ActionSuccessMessage, 'MonitorDialog.html')
                        } else {
                            Mntr.showDialog('Monitor Status', GlobalConstants.ActionFailureMessage, 'MonitorDialog.html')
                        }
                    }).error(function (data, status) {
                         Mntr.hidePleaseWait();
                         Mntr.showDialog('Monitor Status', data, 'MonitorDialog.html')
                    });
            
        };

        Mntr.showPleaseWait = function () {
            Mntr.pleaseWaitDiv.modal();
        };

        Mntr.hidePleaseWait = function () {
            Mntr.pleaseWaitDiv.modal('hide');
        };


       

        Mntr.showDialog = function (heading, details, template) {
            var dialog = $modal.open({
                templateUrl: template,
                controller: 'MDialogCtrl',
                size: 'md',
                resolve: {
                    heading: function () {
                        return heading;
                    },
                    details: function () {
                        return details;
                    }
                }
            });
        };

        Mntr.XmlDialog = function (details) {
            var dialog = $modal.open({
                templateUrl: "XmlDialog.html",
                controller: 'MDialogCtrl',
                size: 'lg',
                resolve: {
                    heading: function () {
                        return "Monitor XML";
                    },
                    details: function () {
                        return details.MonitorXml;
                    }
                }
            });
        };


        Mntr.DeleteDialog = function (details) {
              var dialog = $modal.open({
                templateUrl: 'DeleteDialog.html',
                controller: 'MDialogCtrl',
                size: 'md',
                resolve: {
                    heading: function () {
                        return "Are you sure to delete " + details.MonitorName + " ?";
                    },
                    details: function () {
                        return details;
                    }
                }
            });
              dialog.result.then(function (result) {
                  if (result == 'Yes') {
                    Mntr.PerformAction(details, 'deleted');
                }
            });
        };

    }
    ]);

    myApp.controller('MDialogCtrl', function ($scope, $modalInstance, heading, details) {
        var dlgCtrl = this;
        $scope.dlgCtrl = dlgCtrl;
        dlgCtrl.heading = heading;
        dlgCtrl.details = details;
        dlgCtrl.Yes = function () {
            $modalInstance.close('Yes');
        };
        dlgCtrl.Close = function () {
            $modalInstance.dismiss('cancel');
        };
    });

})();