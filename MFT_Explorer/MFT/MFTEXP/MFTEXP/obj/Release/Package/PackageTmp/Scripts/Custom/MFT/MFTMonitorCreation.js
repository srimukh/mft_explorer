﻿(function () {
    var myApp = angular.module('Trnstemplate', ['ui.bootstrap'])
     .config(['$httpProvider', function ($httpProvider) {
         $httpProvider.defaults.headers.common["AsyncRequestFromClient"] = "true";
     }]);


    myApp.factory('SharedService', function ($rootScope) {
        var dates = {
            startdate: '',
            enddate: ''
        };
        return {
            setstartdate: function (sdate) {
                dates.startdate = sdate;
            },
            getstartdate: function () {
                return dates.startdate;
            },
            setenddate: function (edate) {
                dates.enddate = edate;
            },
            getenddate: function () {
                return dates.enddate;
            }
        };

    });


    myApp.controller('StartDatepickerCtrl', ['$scope', 'SharedService', function ($scope, SharedService) {

        var Strt = this;
        Strt.valuationDate = new Date();
        Strt.valuationDatePickerIsOpen = false;
        Strt.opens = [];
        $scope.$watch(function () {
            SharedService.setstartdate(Strt.valuationDate);
            return Strt.valuationDatePickerIsOpen;
        },
            function (value) {
                Strt.opens.push("valuationDatePickerIsOpen: " + value + " at: " + new Date());
            });

        Strt.valuationDatePickerOpen = function ($event) {
            if ($event) {
                $event.preventDefault();
                $event.stopPropagation();
            }
            this.valuationDatePickerIsOpen = true;

        };
    }]);

    myApp.controller('EndDatepickerCtrl', ['$scope', 'SharedService', function ($scope, SharedService) {

        var Ed = this;
        Ed.valuationDate = new Date();
        Ed.valuationDatePickerIsOpen = false;
        Ed.opens = [];
        $scope.$watch(function () {
            SharedService.setenddate(Ed.valuationDate);
            return Ed.valuationDatePickerIsOpen;
        },
        function (value) {
            Ed.opens.push("valuationDatePickerIsOpen: " + value + " at: " + new Date());
        });
        Ed.valuationDatePickerOpen = function ($event) {

            if ($event) {
                $event.preventDefault();
                $event.stopPropagation();
            }
            this.valuationDatePickerIsOpen = true;
        };

    }]);


    myApp.controller('TransferTemplateController', ['$scope', '$http', '$modal', '$filter', function ($scope, $http, $modal, $filter) {
        var TransTemplate = this;
        $scope.TransTemplate = TransTemplate;
        TransTemplate.AllQueues = [];
        TransTemplate.UserExits = [];
        TransTemplate.AgentandQMgrs = {
            QMgrs: [],
            AgentNames: [],
            SourceAgents: [],
            TargetAgents: []
        };
        TransTemplate.TransType;
        TransTemplate.xmlstring = '';
        TransTemplate.Monitors = [];
        TransTemplate.selected_Monitor;
        TransTemplate.copymonitor = 'no';
        TransTemplate.showMonitors = false;
        TransTemplate.showMonitorEntry = false;
        TransTemplate.selectedAction = '';
        TransTemplate.pleaseWaitDiv = $('<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m"><div class="modal-content"><div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body"><h1>Processing...</h1><div class="progress progress-striped active" style="margin-bottom:0;">' +
            '<div class="progress-bar" style="width: 100%"></div></div></div></div></div></div>');

        TransTemplate.hideCount = 0;

        TransTemplate.showPleaseWait = function () {
            TransTemplate.pleaseWaitDiv.modal();
        };
        TransTemplate.hideWaitDlg = function (inID) {
            if (TransTemplate.hideCount < 6) {
                TransTemplate.hideCount = TransTemplate.hideCount + inID;
            } 
            if (TransTemplate.hideCount == 6) {
                TransTemplate.hidePleaseWait();
                TransTemplate.hideCount = 0;
            }
        }
        TransTemplate.hidePleaseWait = function () {
            TransTemplate.pleaseWaitDiv.modal('hide');
        };

        TransTemplate.GetAgentandQMgr = function () {
            TransTemplate.showPleaseWait();
            $http({
                url: GlobalURLs.GetAgents_QMgrs,
                method: 'get'
            }).success(function (data, status) {
                TransTemplate.AgentandQMgrs = {
                    QMgrs: data.Result.Managers,
                    AgentNames: data.Result.Agents,
                    SourceAgents: [],
                    TargetAgents: []
                };
                //TransTemplate.AgentandQMgrs.SourceAgents = [];
                //TransTemplate.AgentandQMgrs.TargetAgents = [];
                TransTemplate.hideWaitDlg(2);
            }).error(function (data, status) {
                TransTemplate.showDialog('Transfer Template', data, 'TransTempDialog.html')
                TransTemplate.hideWaitDlg(2);
            });
        };

     
        TransTemplate.Timebases = [
       { base: 'admin' },
       { base: 'source' },
       { base: 'UTC' }
        ];

        TransTemplate.PollFrequency = [
       { Poll: 'seconds' },
       { Poll: 'minutes' },
       { Poll: 'hours' },
       { Poll: 'days' }
        ];


        TransTemplate.PatternType = [
       { Type: 'Wildcard' },
       { Type: 'Reqular expression' }
        ];

        TransTemplate.CheckSum = [
        { Check: 'None' },
        { Check: 'MD5' }
        ];

        TransTemplate.type = [
        { Type: 'File' },
        { Type: 'Directory' },
        { Type: 'Data set' },
        { Type: 'Queue' }
        ];

        TransTemplate.filesizebytes = [
        { fsize: 'byte', fvalue: 'B' },
        { fsize: 'Kbyte', fvalue: 'KB' },
        { fsize: 'Mbyte', fvalue: 'MB' },
        { fsize: 'Gbyte', fvalue: 'GB' }
        ];

     
        TransTemplate.Transdetails = {
            Monitor_ID: null,
            Monitor_Name: null,
            Src_QueMgr: null,
            Src_HostName: null,
            Src_HostIP: null,
            Src_Channel: null,
            Src_Port: null,
            Src_UserID: null,
            SelSrc_QueMgr: null,
            Trgt_QueMgr: null,
            Src_Agent: null,
            Trgt_Agent: null,
            resource_type: 'directory',
            resource_info: null,
            Poll_Interval: 1,
            Poll_Frequency: TransTemplate.PollFrequency[1].Poll,
            M_Pattern: '*',
            E_Pattern: null,
            Pattern_type: TransTemplate.PatternType[0].Type,
            Trg_condition: 'fileMatch',
            filesizeb: null,
            polls: null,
            Trans_Mode: 'binary',
            Src_file: null,
            Trgt_file: null,
            Src_disposition: 'leave',
            Trgt_exists: 'error',
            Pre_Src: '',
            Post_Src: '',
            Pre_dest: '',
            Post_dest: '',
            Job_name: null,
            reply_Queue: null,
            Trans_priority: 1,
            Checksums: TransTemplate.CheckSum[1].Check,
            Source_type: TransTemplate.type[0].Type,
            Dest_type: TransTemplate.type[0].Type,
            Action: null,
            Exists_QueMGR: null,
            Exists_AgentName: null
        };

        TransTemplate.Transdetails.Schedule = {
            Timebase: TransTemplate.Timebases[0].base,
            Frequency: TransTemplate.PollFrequency[1].Poll,
            Interval: 1,
            Starttime: null,
            Endtime: null
        };

        TransTemplate.Exits = {
            ExitType: null,
            SelectedExit: null,
            List: [ { Type: "", DetailsStr:"", Details: [] } ],
            NewExit: { Type: '', DetailsStr:"", Details: [] },
            NewExitDetails: { Name: '', ParamStr: '', Params: [] },
            InitExit: function () {
                TransTemplate.Exits.List = [];
                TransTemplate.Exits.List.push({ Type: 'PreSrc', DetailsStr:"", Details: [] })
                TransTemplate.Exits.List.push({ Type: 'PostSrc', DetailsStr:"", Details: [] })
                TransTemplate.Exits.List.push({ Type: 'PreDest',  DetailsStr:"", Details: [] })
                TransTemplate.Exits.List.push({ Type: 'PostDest', DetailsStr:"",Details: [] })
            },
            LoadExitDetails: function (ExitType, ExitString) {
                TransTemplate.Exits.List[ExitType].DetailsStr = ExitString;
                if (ExitType != null && ExitString != null) {
                    TransTemplate.Exits.List[ExitType].Details = [];
                    angular.forEach(ExitString.split("~") , function (exit, exitindex) {
                        var params = exit.split("|");
                        var name = params[0];
                        params.splice(0, 1);
                        TransTemplate.Exits.List[ExitType].Details.push({ Name: name, ParamStr: exit, Params: params });
                    });
                }
            },
            EnterExit: function (exitType) {
                TransTemplate.Exits.CancelExit();
                TransTemplate.Exits.ExitType = exitType;
            },
            SaveExit: function () {
                var valid = true ;
                var paramstr = TransTemplate.Exits.NewExitDetails.Name ;
                angular.forEach(TransTemplate.Exits.NewExitDetails.Params, function (prm, index) {
                    if (prm.Value == null || prm.Value == '') {
                        valid = false;
                    } else {
                        paramstr = paramstr + "|" + prm.Value;
                    }
                });
                if (!valid) {
                    TransTemplate.showDialog("Monitor  / Transfer ", "Please enter all Param Values.", "TransTempDialog.html");
                    return false;
                } else {
                    TransTemplate.Exits.NewExitDetails.ParamStr = paramstr;
                    var ExitType = TransTemplate.Exits.ExitType;
                    TransTemplate.Exits.List[TransTemplate.Exits.ExitType].Details.push(angular.copy(TransTemplate.Exits.NewExitDetails));
                    TransTemplate.Exits.List[ExitType].DetailsStr = "";
                    angular.forEach(TransTemplate.Exits.List[ExitType].Details, function (ext, index) {
                        if (TransTemplate.Exits.List[ExitType].DetailsStr == '') {
                            TransTemplate.Exits.List[ExitType].DetailsStr =
                                TransTemplate.Exits.List[ExitType].DetailsStr +
                                ext.ParamStr;
                        } else {
                            TransTemplate.Exits.List[ExitType].DetailsStr =
                                 TransTemplate.Exits.List[ExitType].DetailsStr + "~" +
                                 ext.ParamStr;
                        }
                    });
                    TransTemplate.Exits.ExitType = null;
                    TransTemplate.Exits.SelectedExit = null;
                    TransTemplate.Exits.NewExitDetails = null; // { Name: '', ParamStr: '', Params: [] };
                    
                }
            },
            ClearExit: function (ExitType) {
                TransTemplate.Exits.List[ExitType].Details = [];
                TransTemplate.Exits.List[ExitType].DetailsStr = "";
            },
            CancelExit: function () {
                TransTemplate.Exits.ExitType = null;
                TransTemplate.Exits.SelectedExit = null;
                TransTemplate.Exits.NewExitDetails = null;
            },
            ExitChanged: function () {
                if (TransTemplate.Exits.SelectedExit == null) {
                    TransTemplate.Exits.NewExitDetails = null;
                } else {
                    //alert(TransTemplate.Exits.SelectedExit.Exit_Params);
                    TransTemplate.Exits.NewExitDetails = { Name: TransTemplate.Exits.SelectedExit.UserExit_detail, ParamStr: '', Params: [] },
                    angular.forEach(TransTemplate.Exits.SelectedExit.Exit_Params.split("|"), function (prm, index) {
                        TransTemplate.Exits.NewExitDetails.Params.push({ Name: prm, Value: "" });
                    });
                }
            }
        };
        

        TransTemplate.GetUserExits = function () {
            TransTemplate.showPleaseWait();
            $http({
                url: GlobalURLs.UserExitsList,
                method: 'get'
            }).success(function (data, status) {
                if (data.Status == GlobalConstants.Success) {
                    TransTemplate.UserExits = data.Result.UserExits;
                } else {
                    TransTemplate.showDialog('Transfer Template', data.Error.Message, 'TransTempDialog.html');
                }
                TransTemplate.hideWaitDlg(1);
            }).error(function (data, status) {
                TransTemplate.showDialog('Transfer Template', data, 'TransTempDialog.html');
                TransTemplate.hideWaitDlg(1);
            });
        };

        TransTemplate.GetAgentsAndQueues = function () {
            if (TransTemplate.Transdetails.SelSrc_QueMgr != null && TransTemplate.Transdetails.Trgt_QueMgr != null) {

                TransTemplate.Transdetails.Src_QueMgr = TransTemplate.Transdetails.SelSrc_QueMgr.QM_Name;
                TransTemplate.Transdetails.Src_HostIP = TransTemplate.Transdetails.SelSrc_QueMgr.Host_IP;
                TransTemplate.Transdetails.Src_HostName = TransTemplate.Transdetails.SelSrc_QueMgr.Host_Name;
                TransTemplate.Transdetails.Src_Channel = TransTemplate.Transdetails.SelSrc_QueMgr.QM_Channel;
                TransTemplate.Transdetails.Src_Port = TransTemplate.Transdetails.SelSrc_QueMgr.QM_Port;
                TransTemplate.Transdetails.Src_UserID = TransTemplate.Transdetails.SelSrc_QueMgr.QM_UserID;

                TransTemplate.GetAgents();
                TransTemplate.GetQueues();
            }
        }

        TransTemplate.CheckAgents = function () {
            //alert(TransTemplate.Transdetails.Src_Agent);
            if (TransTemplate.Transdetails.Src_Agent != null) {
                angular.forEach(TransTemplate.AgentandQMgrs.SourceAgents, function (agt, index) {
                    if (TransTemplate.Transdetails.Src_Agent == agt.Value) {
                        if (agt.Status != 'STARTED' && agt.Status != 'READY') {
                            //alert("Sgent selected is not running. Please start the agent first.");
                            TransTemplate.Transdetails.Src_Agent = null;
                            TransTemplate.showDialog('Monitor Status', "Agent selected is not running. Please start the agent first.", 'TransTempDialog.html')                            
                        }
                    }
                });
            }

            if (TransTemplate.Transdetails.Trgt_Agent != null) {
                angular.forEach(TransTemplate.AgentandQMgrs.TargetAgents , function (agt, index) {
                    if (TransTemplate.Transdetails.Trgt_Agent == agt.Value) {
                        if (agt.Status != 'STARTED'
                         && agt.Status != 'READY') {
                            //alert("Sgent selected is not running. Please start the agent first.");
                            TransTemplate.Transdetails.Trgt_Agent = null;
                            TransTemplate.showDialog('Monitor Status', "Agent selected is not running. Please start the agent first.", 'TransTempDialog.html')
                        }
                    }
                });
            }

        };

        TransTemplate.GetAgents = function () {
            var sAgents = [];
            var tAgents = [];
            angular.forEach(TransTemplate.AgentandQMgrs.AgentNames, function (key, index) {
                //
                if (key.queueManager == TransTemplate.Transdetails.Src_QueMgr && key.agentType != 'BRIDGE' ) {
                    sAgents.push({ Name: key.agentName + '  --  ' + key.AgentStatus, Value: key.agentName, Status : key.AgentStatus});
                }
                if (key.queueManager == TransTemplate.Transdetails.Trgt_QueMgr) {
                    tAgents.push({ Name: key.agentName + '  --  ' + key.AgentStatus, Value: key.agentName, Status : key.AgentStatus });
                }
            });
            TransTemplate.AgentandQMgrs.SourceAgents = sAgents;
            TransTemplate.AgentandQMgrs.TargetAgents = tAgents;
        };

        TransTemplate.GetQueues = function () {
            TransTemplate.showPleaseWait();
            if (TransTemplate.Transdetails.Src_QueMgr != null) {
                $http({
                    url: GlobalURLs.QueuesByManager,
                    method: 'get',
                    params: { QMgr: TransTemplate.Transdetails.Src_QueMgr }
                }).success(function (data, status) {
                    TransTemplate.AllQueues = data.Result.Queues;
                    TransTemplate.hidePleaseWait();
                }).error(function (data, status) {
                    TransTemplate.showDialog('Transfer Template', data, 'TransTempDialog.html')
                    TransTemplate.hidePleaseWait();
                });
            }

        };

        TransTemplate.GetMonitors = function () {
            TransTemplate.showPleaseWait();
            $http({
                url: GlobalURLs.MonitorsList,
                method: 'get'
            }).success(function (data, status) {
                TransTemplate.Monitors = data.Result.Monitors;
                TransTemplate.hideWaitDlg(3);
            }).error(function (data, status) {
                TransTemplate.showDialog('Transfer Template', data, 'TransTempDialog.html')
                TransTemplate.hideWaitDlg(3);
            });
        };

        TransTemplate.RefreshMonitorEntry = function (inCopy) {
            TransTemplate.copymonitor = inCopy;
            if (TransTemplate.copymonitor == 'no') {
                TransTemplate.showMonitorEntry = true;
            } else {
                TransTemplate.showMonitorEntry = false;
            }
            TransTemplate.selected_Monitor = null;
            TransTemplate.Transdetails = {
                Monitor_ID: null,
                Monitor_Name: null,
                Src_QueMgr: null,
                Src_HostName: null,
                Src_HostIP: null,
                Src_Channel: null,
                Src_Port: null,
                Src_UserID: null,
                SelSrc_QueMgr: null,
                Trgt_QueMgr: null,
                Src_Agent: null,
                Trgt_Agent: null,
                resource_type: 'directory',
                resource_info: null,
                Poll_Interval: 1,
                Poll_Frequency: TransTemplate.PollFrequency[1].Poll ,
                M_Pattern: '*',
                E_Pattern: null,
                Pattern_type: TransTemplate.PatternType[0].Type,
                Trg_condition: 'fileMatch',
                filesizeb: null,
                polls: null,
                Trans_Mode: 'binary',
                Src_file: null,
                Trgt_file: null,
                Src_disposition: 'leave',
                Trgt_exists: 'error',
                Pre_Src: '',
                Post_Src: '',
                Pre_dest: '',
                Post_dest: '',
                Job_name: null,
                reply_Queue: null,
                Trans_priority: 1,
            Checksum: TransTemplate.CheckSum[1].Check,
                Source_type: TransTemplate.type[0].Type,
                Dest_type: TransTemplate.type[0].Type,
                Action: 'Create',
                Exists_QueMGR: null,
                Exists_AgentName: null
            }

        }

        TransTemplate.GetSelectedMonitordetails = function (MonitorID) {
            if (MonitorID == '' || MonitorID == null) {
                TransTemplate.showMonitorEntry = false;
            } else {
                TransTemplate.showMonitorEntry = true;
            }

            //var filteredRecord = $filter('filter')(TransTemplate.Monitors, { 'MonitorName': MonitorName });
            var filteredRecord = $filter('filter')(TransTemplate.Monitors, { 'MonitorID': MonitorID });
            if (filteredRecord.length > 0) {
                TransTemplate.showPleaseWait();
                $http({
                    url: GlobalURLs.Monitordetails,
                    method: 'get',
                    params: { MonitorID: filteredRecord[0]["MonitorID"] }
                }).success(function (data, status) {
                    TransTemplate.hidePleaseWait();
                    if (data.Status == GlobalConstants.Success) {

                        TransTemplate.Transdetails = data.Result.MonitorDetails;

                        if (TransTemplate.Transdetails.Pre_Src != null && TransTemplate.Transdetails.Pre_Src != "") {
                            //TransTemplate.Exits.List[0].DetailsStr = TransTemplate.Transdetails.Pre_Src;
                            TransTemplate.Exits.LoadExitDetails(0, TransTemplate.Transdetails.Pre_Src)
                        } else {
                            TransTemplate.Exits.List[0].DetailsStr = "";
                        }
                        if (TransTemplate.Transdetails.Post_Src != null && TransTemplate.Transdetails.Post_Src != "") {
                            //TransTemplate.Exits.List[1].DetailsStr = TransTemplate.Transdetails.Post_Src;
                            TransTemplate.Exits.LoadExitDetails(1, TransTemplate.Transdetails.Post_Src)
                        } else {
                            TransTemplate.Exits.List[1].DetailsStr = "";
                        }
                        if (TransTemplate.Transdetails.Pre_dest != null && TransTemplate.Transdetails.Pre_dest != "") {
                            //TransTemplate.Exits.List[2].DetailsStr = TransTemplate.Transdetails.Pre_dest;
                            TransTemplate.Exits.LoadExitDetails(2, TransTemplate.Transdetails.Pre_dest)
                        } else {
                            TransTemplate.Exits.List[2].DetailsStr = "";
                        }
                        if (TransTemplate.Transdetails.Post_dest != null && TransTemplate.Transdetails.Post_dest != "") {
                            //TransTemplate.Exits.List[3].DetailsStr = TransTemplate.Transdetails.Post_dest;
                            TransTemplate.Exits.LoadExitDetails(3, TransTemplate.Transdetails.Post_dest)
                        } else {
                            TransTemplate.Exits.List[3].DetailsStr = "";
                        }

                        angular.forEach(TransTemplate.AgentandQMgrs.QMgrs, function (mgr, index) {
                            if (mgr.QM_Name == TransTemplate.Transdetails.Src_QueMgr)
                                TransTemplate.Transdetails.SelSrc_QueMgr = TransTemplate.AgentandQMgrs.QMgrs[index];
                        });
                        TransTemplate.Transdetails.Src_QueMgr = TransTemplate.Transdetails.SelSrc_QueMgr.QM_Name;
                        TransTemplate.Transdetails.Src_HostIP = TransTemplate.Transdetails.SelSrc_QueMgr.Host_IP;
                        TransTemplate.Transdetails.Src_HostName = TransTemplate.Transdetails.SelSrc_QueMgr.Host_Name;
                        TransTemplate.Transdetails.Src_Channel = TransTemplate.Transdetails.SelSrc_QueMgr.QM_Channel;
                        TransTemplate.Transdetails.Src_Port = TransTemplate.Transdetails.SelSrc_QueMgr.QM_Port;
                        TransTemplate.Transdetails.Src_UserID = TransTemplate.Transdetails.SelSrc_QueMgr.QM_UserID;

                        TransTemplate.Transdetails.Pattern_type = TransTemplate.PatternType[0].Type;
                        if (TransTemplate.Transdetails.Schedule == null) {
                            TransTemplate.Transdetails.Schedule = {
                                Timebase: null,
                                Frequency: TransTemplate.PollFrequency[1].Poll,
                                Interval: 1,
                                Starttime: null,
                                Endtime: null
                            }
                        }
                        if (TransTemplate.copymonitor == 'yes') {
                            TransTemplate.Transdetails.Action = 'Create';
                            TransTemplate.Transdetails.Monitor_ID = null;
                            TransTemplate.Transdetails.Monitor_Name = null;
                            TransTemplate.Transdetails.Job_name = null;
                        }
                        TransTemplate.hideCount = 0;
                        //TransTemplate.GetAgentsAndQueues();
                        TransTemplate.GetAgents();
                    } else {
                        TransTemplate.showFDialog('Transfer Template', data.Error.Message , 'TransTempDialog.html')
                    }

                }).error(function (data, status) {
                    TransTemplate.hidePleaseWait();
                    TransTemplate.showFDialog('Transfer Template', data, 'TransTempDialog.html')


                });
            }

        };

        TransTemplate.clear = function () {
            angular.forEach(TransTemplate.Transdetails, function (value, key) {
                TransTemplate.Transdetails[key] = null;

            });
            TransTemplate.TransType = null;
            TransTemplate.ActionType = null;
            TransTemplate.copymonitor = 'no';
            TransTemplate.Transdetails.Src_disposition = 'leave';
            TransTemplate.Transdetails.Trgt_exists = 'error';
            TransTemplate.hidePleaseWait();
        };

        TransTemplate.buildAction = function (transtype, action) {
            TransTemplate.clear();
            TransTemplate.TransType = transtype;
            TransTemplate.Transdetails.Poll_Frequency = TransTemplate.PollFrequency[1].Poll;
            TransTemplate.Transdetails.Poll_Interval = 1;
            TransTemplate.Transdetails.Pattern_type = TransTemplate.PatternType[0].Type;
            TransTemplate.Transdetails.resource_type = 'directory';
            TransTemplate.Transdetails.Checksum = TransTemplate.CheckSum[1].Check;
            TransTemplate.Transdetails.Trans_priority = 1;
            TransTemplate.Transdetails.filesizeb = TransTemplate.filesizebytes[0].fvalue;
            TransTemplate.Transdetails.Source_type = TransTemplate.type[0].Type;
            TransTemplate.Transdetails.Dest_type = TransTemplate.type[0].Type;
            TransTemplate.Transdetails.Trans_Mode = 'binary';
            TransTemplate.Transdetails.Trg_condition = 'fileMatch';
            TransTemplate.Transdetails.M_Pattern = '*';
            TransTemplate.Transdetails.Action = action;
            TransTemplate.copymonitor = 'no';
            TransTemplate.selected_Monitor = null;
               
            TransTemplate.Transdetails.Schedule = {
                Timebase: TransTemplate.Timebases[0].base,
                Frequency: TransTemplate.PollFrequency[1].Poll,
                Interval: 1,
                Starttime: null,
                Endtime: null
            }


            if (action == 'transfer') {
                TransTemplate.selectedAction = "One Time Transfer";
                TransTemplate.showMonitorEntry = true;
            } else if (action == 'schedule') {
                TransTemplate.selectedAction = "Schedule Transfer";
                TransTemplate.showMonitorEntry = true;
            } else  {
                TransTemplate.selectedAction = action + " Monitor";
                TransTemplate.showMonitorEntry = false;
            }

            TransTemplate.showMonitors = true;
        };

        TransTemplate.buildMonitor = function () {
            if (TransTemplate.Transdetails.Job_name.length > 0) {
                var MonitorExists = null;
                var filteredMonitor = $filter('filter')(TransTemplate.Monitors, { 'MonitorName': TransTemplate.Transdetails.Monitor_Name }, true);
                if (TransTemplate.Transdetails.Action == 'Edit') {
                    TransTemplate.Transdetails.Monitor_ID = TransTemplate.selected_Monitor;
                    TransTemplate.Transdetails.Exists_QueMGR = filteredMonitor[0]["QueueManagerName"];
                    TransTemplate.Transdetails.Exists_AgentName = filteredMonitor[0]["AgentName"];
                }
                else if (TransTemplate.Transdetails.Action == 'Create' || TransTemplate.Transdetails.Action == 'create') {
                    if (filteredMonitor.length > 0) { MonitorExists = true; }
                }
              
                if (MonitorExists == null) {

                    if (TransTemplate.selectedAction != "Schedule Transfer") {
                        TransTemplate.Transdetails.Schedule = null;
                    } else {
                        //if (TransTemplate.Transdetails.Schedule.Starttime_date != null &&  TransTemplate.Transdetails.Schedule.Starttime_time != null)
                        //{
                        //    TransTemplate.Transdetails.Schedule.Starttime =
                        //                TransTemplate.Transdetails.Schedule.Starttime_date + "T" + TransTemplate.Transdetails.Schedule.Starttime_time;
                        //}
                        //if (TransTemplate.Transdetails.Schedule.Endtime_date != null && TransTemplate.Transdetails.Schedule.Endtime_time != null)
                        //{
                        //    TransTemplate.Transdetails.Schedule.Endtime =
                        //                TransTemplate.Transdetails.Schedule.Endtime_date + "T" + TransTemplate.Transdetails.Schedule.Endtime_time;
                        //}
                    }
                    if (TransTemplate.selected_Monitor != null) {
                        TransTemplate.Transdetails.Monitor_ID = TransTemplate.selected_Monitor;
                    }


                    TransTemplate.Transdetails.Pre_Src = TransTemplate.Exits.List[0].DetailsStr;
                    TransTemplate.Transdetails.Post_Src = TransTemplate.Exits.List[1].DetailsStr;
                    TransTemplate.Transdetails.Pre_dest = TransTemplate.Exits.List[2].DetailsStr;
                    TransTemplate.Transdetails.Post_dest = TransTemplate.Exits.List[3].DetailsStr;

                    TransTemplate.showPleaseWait();
                    $http({
                        url: GlobalURLs.BuildMonitor,
                        method: 'post',
                        params: { requestMonitor: JSON.stringify(TransTemplate.Transdetails)} 
                    }).success(function (data, status) {
                        if (data.Status == GlobalConstants.Success) {
                            TransTemplate.Monitors = data.Result.Monitors;
                            TransTemplate.showDialog('Monitor Status', "Data Saved Successfully.", 'TransTempDialog.html')
                        } else {
                            TransTemplate.showDialog('Monitor Status', data.Error.Message, 'TransTempDialog.html')
                        }
                        TransTemplate.clear();
                    }).error(function (data, status) {
                        TransTemplate.showDialog('Monitor Status', data, 'TransTempDialog.html')
                        // data: TransTemplate.Transdetails
                    });
                    TransTemplate.copymonitor = 'no';
                    TransTemplate.showMonitors = false;
                    TransTemplate.showMonitorEntry = false;
                    TransTemplate.selectedAction = '';
                }
                else {
                    TransTemplate.showDialog('Monitor Status', "Monitor Name Already Exists", 'TransTempDialog.html')
                }
            }
            else {
                TransTemplate.showDialog('Monitor Status', "Please enter mandatory fields", 'TransTempDialog.html')
            };
        };

        TransTemplate.showDialog = function (heading, details, template) {
            var dialog = $modal.open({
                templateUrl: template,
                controller: 'TDialogCtrl',
                size: 'md',
                resolve: {
                    heading: function () {
                        return heading;
                    },
                    details: function () {
                        return details;
                    }
                }
            });

        };

    }
    ]);


    myApp.controller('StartDatepickerCtrl', ['$scope', 'SharedService', function ($scope, SharedService) {

        var Strt = this;
        Strt.valuationDate = new Date();
        Strt.valuationDatePickerIsOpen = false;
        Strt.opens = [];
        $scope.$watch(function () {
            SharedService.setstartdate(Strt.valuationDate);
            return Strt.valuationDatePickerIsOpen;
        },
            function (value) {
                Strt.opens.push("valuationDatePickerIsOpen: " + value + " at: " + new Date());
            });

        Strt.valuationDatePickerOpen = function ($event) {
            if ($event) {
                $event.preventDefault();
                $event.stopPropagation();
            }
            this.valuationDatePickerIsOpen = true;

        };
    }]);

    myApp.controller('EndDatepickerCtrl', ['$scope', 'SharedService', function ($scope, SharedService) {

        var Ed = this;
        Ed.valuationDate = new Date();
        Ed.valuationDatePickerIsOpen = false;
        Ed.opens = [];
        $scope.$watch(function () {
            SharedService.setenddate(Ed.valuationDate);
            return Ed.valuationDatePickerIsOpen;
        },
        function (value) {
            Ed.opens.push("valuationDatePickerIsOpen: " + value + " at: " + new Date());
        });
        Ed.valuationDatePickerOpen = function ($event) {

            if ($event) {
                $event.preventDefault();
                $event.stopPropagation();
            }
            this.valuationDatePickerIsOpen = true;
        };

    }]);

    myApp.controller('TDialogCtrl', function ($scope, $modalInstance, heading, details) {
        var dlgCtrl = this;
        $scope.dlgCtrl = dlgCtrl;
        dlgCtrl.heading = heading;
        dlgCtrl.details = details;
        dlgCtrl.Close = function () {
            $modalInstance.dismiss('cancel');
        };
    });
})();
