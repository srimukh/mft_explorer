﻿(function () {
    var myApp = angular.module('Queues', ['ui.bootstrap'])
     .config(['$httpProvider', function ($httpProvider) {
         $httpProvider.defaults.headers.common["AsyncRequestFromClient"] = "true";
     }]);

    myApp.controller('QueueController', ['$scope', '$http', '$modal', function ($scope, $http, $modal) {
        var Que = this;
        $scope.Que = Que;
        Que.sortType;
        Que.sortReverse = true;
        Que.search;
        Que.pleaseWaitDiv = $('<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m"><div class="modal-content"><div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body"><h1>Processing...</h1><div class="progress progress-striped active" style="margin-bottom:0;">' +
            '<div class="progress-bar" style="width: 100%"></div></div></div></div></div></div>');

        Que.Prefix = "SYSTEM.FTE.*";
        Que.QMgr = "";
        Que.Managers = [];
        Que.ShowData = false;

        Que.GetQueues = function () {
            Que.ShowData = true;
               Que.showPleaseWait();
                $http({
                    url: GlobalURLs.QueuesList,
                    method: 'get',
                    params: { Prefix: Que.Prefix, QMgr: Que.QMgr }
                }).success(function (data, status) {
                    Que.hidePleaseWait();
                    if (data.Status == GlobalConstants.Success) {
                        Que.resultsObj.fullResults = data.Result.Queues;
                        Que.resultsObj.totalItems = data.Result.Queues.length;
                        Que.resultsObj.setPage(1);
                        Que.resultsObj.pageChanged();
                        Que.results = data.Result.Queues;
                    }
                    else
                    {
                        Que.showDialog('Queue Details', data.Error.Message, 'QueueDialog.html', 'md')
                    }
                }).error(function (data, status) {
                    Que.hidePleaseWait();
                    Que.showDialog('Queue Details', data, 'QueueDialog.html','md')
                });
            
        };
         

        Que.GetManagers = function () {
            Que.showPleaseWait();
            $http({
                url: GlobalURLs.QueueManagersList,
                method: 'get',
            }).success(function (data, status) {
                Que.hidePleaseWait();
                if (data.Result.Managers != null ) {
                    Que.Managers = data.Result.Managers;
                }
                else
                {
                    Que.showDialog('Queue Details', data.Error.Message, 'QueueDialog.html', 'md')
                }
            }).error(function (data, status) {
                Que.hidePleaseWait();
                Que.showDialog('Queue Details', data, 'QueueDialog.html','md')
            });
            
        };

        Que.currentPage = 1;
        Que.pageSize = "10";
        Que.resultsObj = {
            totalItems: null,
            currentPage: 1,
            setPage: function (pageNum) {
                Que.resultsObj.currentPage = pageNum;
            },
            setPageSize: function (size) {
                Que.resultsObj.pageSize = size;
                Que.resultsObj.pageChanged();
            },
            pageSize: 10,
            availPageSizes: [10, 20, 30, 40],
            numPages: 9,
            startRecord: 1,
            endRecord: 5,
            pageChanged: function () {
                var tempResults = Que.resultsObj.fullResults;
                var start = ((Que.resultsObj.currentPage == 1) ? 0 : ((Que.resultsObj.currentPage - 1) * Que.resultsObj.pageSize));
                var end = start + Que.resultsObj.pageSize;
                if (end > tempResults.length) {
                    end = tempResults.length;
                }
                Que.resultsObj.startRecord = start + 1;
                Que.resultsObj.endRecord = end;
                Que.resultsObj.currentResults.details = tempResults.slice(start, end);
            },
            fullResults: null,
            currentResults: { details: null }
        };

        Que.showPleaseWait = function () {
           
           Que.pleaseWaitDiv.modal();
        };

        Que.hidePleaseWait = function () {
            Que.pleaseWaitDiv.modal('hide');
        };

        Que.ViewXML = function (Queheader, QueueProperties) {
           
            Que.showDialog(Queheader, QueueProperties, 'ViewXML.html', 'lg')
        };


        Que.showDialog = function (heading, details, template,size) {
            var dialog = $modal.open({
                templateUrl: template,
                controller: 'XMLDialogCtrl',
                size: size,
                resolve: {
                    heading: function () {
                        return heading;
                    },
                    details: function () {
                        return details;
                    }
                }
            });

        };
    }
    ]);

    myApp.controller('XMLDialogCtrl', function ($scope, $modalInstance, heading, details) {
        var dlgCtrl = this;
        $scope.dlgCtrl = dlgCtrl;
        dlgCtrl.heading = heading;
        dlgCtrl.details = details;
        dlgCtrl.Close = function () {
            $modalInstance.dismiss('cancel');
        };
    });

})();

