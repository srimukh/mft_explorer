﻿(function () {
    var myApp = angular.module('Schedulers', ['ui.bootstrap'])
     .config(['$httpProvider', function ($httpProvider) {
         $httpProvider.defaults.headers.common["AsyncRequestFromClient"] = "true";
     }]);

    myApp.controller('SchedulerController', ['$scope', '$http', '$modal', function ($scope, $http, $modal) {
        var Sdle = this;
        $scope.Sdle = Sdle;
        Sdle.sortType;
        Sdle.sortReverse = true;
        Sdle.search;
        Sdle.pleaseWaitDiv = $('<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m"><div class="modal-content"><div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body"><h1>Processing...</h1><div class="progress progress-striped active" style="margin-bottom:0;">' +
            '<div class="progress-bar" style="width: 100%"></div></div></div></div></div></div>');

        Sdle.GetSchedulers = function () {
            Sdle.showPleaseWait();
            $http({
                url: GlobalURLs.ScheduledTransfersList,
                method: 'get'
            }).success(function (data, status) {
                Sdle.hidePleaseWait();
                if (data.Status == GlobalConstants.Success) {
                    Sdle.resultsObj.fullResults = data.Result.Schedulers;
                    Sdle.resultsObj.totalItems = data.Result.Schedulers.length;
                    Sdle.resultsObj.setPage(1);
                    Sdle.resultsObj.pageChanged();
                    Sdle.results = data.Result.Schedulers;
                }
                else {
                    Sdle.showDialog('Scheduled Transfer Details', data.Error.Message, 'SchedulerDialog.html', 'md')
                }
            }).error(function (data, status) {
                Sdle.hidePleaseWait();
                Sdle.showDialog('Scheduled Transfer Details', data, 'SchedulerDialog.html', 'md')
            });

        };

        Sdle.currentPage = 1;
        Sdle.pageSize = "10";
        Sdle.resultsObj = {
            totalItems: null,
            currentPage: 1,
            setPage: function (pageNum) {
                Sdle.resultsObj.currentPage = pageNum;
            },
            setPageSize: function (size) {
                Sdle.resultsObj.pageSize = size;
                Sdle.resultsObj.pageChanged();
            },
            pageSize: 10,
            availPageSizes: [10, 20, 30, 40],
            numPages: 9,
            startRecord: 1,
            endRecord: 5,
            pageChanged: function () {
                var tempResults = Sdle.resultsObj.fullResults;
                var start = ((Sdle.resultsObj.currentPage == 1) ? 0 : ((Sdle.resultsObj.currentPage - 1) * Sdle.resultsObj.pageSize));
                var end = start + Sdle.resultsObj.pageSize;
                if (end > tempResults.length) {
                    end = tempResults.length;
                }
                Sdle.resultsObj.startRecord = start + 1;
                Sdle.resultsObj.endRecord = end;
                Sdle.resultsObj.currentResults.details = tempResults.slice(start, end);
            },
            fullResults: null,
            currentResults: { details: null }
        };

        Sdle.showPleaseWait = function () {

            Sdle.pleaseWaitDiv.modal();
        };

        Sdle.hidePleaseWait = function () {
            Sdle.pleaseWaitDiv.modal('hide');
        };

        Sdle.ViewXML = function (Sdleheader, ScheduledDetails) {

            Sdle.showDialog(Sdleheader, ScheduledDetails, 'ViewXML.html', 'lg')
        };
        Sdle.DeleteTransfer = function (value) {
            Sdle.showPleaseWait();
            $http({
                url: GlobalURLs.DeleteScheduledTransfer,
                method: 'POST',
                params: { TransferId: value.Name, SourceAgent: value.SAgent }
            }).success(function (data, status) {
                Sdle.hidePleaseWait();
                if (data.Status == GlobalConstants.Success) {
                    Sdle.showDialog('Scheduled Transfer Details', GlobalConstants.ActionSuccessMessage, 'SchedulerDialog.html', 'md')
                    Sdle.resultsObj.fullResults = data.Result.Schedulers;
                    Sdle.resultsObj.totalItems = data.Result.Schedulers.length;
                    Sdle.resultsObj.setPage(1);
                    Sdle.resultsObj.pageChanged();
                    Sdle.results = data.Result.Schedulers;
                }
                else {
                    Sdle.showDialog('Scheduled Transfer Details', data.Error.Message, 'SchedulerDialog.html', 'md')
                }                
            }).error(function (data, status) {
                Sdle.hidePleaseWait();
                Sdle.showDialog('Scheduled Transfer', data, 'SchedulerDialog.html')
            });


        };

        Sdle.showDialog = function (heading, details, template, size) {
            var dialog = $modal.open({
                templateUrl: template,
                controller: 'XMLDialogCtrl',
                size: size,
                resolve: {
                    heading: function () {
                        return heading;
                    },
                    details: function () {
                        return details;
                    }
                }
            });
        };


        Sdle.DeleteDialog = function (details) {
            var dialog = $modal.open({
                templateUrl: 'DeleteDialog.html',
                controller: 'XMLDialogCtrl',
                size: 'md',
                resolve: {
                    heading: function () {
                        return "Are you sure to delete " + details.Job + " ?";
                    },
                    details: function () {
                        return details;
                    }
                }
            });
            dialog.result.then(function (result) {
                if (result == 'Yes') {
                    Sdle.DeleteTransfer(details);
                }
            });
        };


    }
    ]);

    myApp.controller('XMLDialogCtrl', function ($scope, $modalInstance, heading, details) {
        var dlgCtrl = this;
        $scope.dlgCtrl = dlgCtrl;
        dlgCtrl.heading = heading;
        dlgCtrl.details = details;
        dlgCtrl.Yes = function () {
            $modalInstance.close('Yes');
        };
        dlgCtrl.Close = function () {
            $modalInstance.dismiss('cancel');
        };
    });

})();

