﻿(function () {
    var myApp = angular.module('USREXTCFG', ['ui.bootstrap'])
     .config(['$httpProvider', function ($httpProvider) {
         $httpProvider.defaults.headers.common["AsyncRequestFromClient"] = "true";
     }]);
    myApp.controller('USREXTController', ['$scope', '$http', '$modal', function ($scope, $http, $modal) {
        var usrextcfg = this;
        $scope.usrextcfg = usrextcfg;
        usrextcfg.UserExitlist = [];
        usrextcfg.showadd = false;
        usrextcfg.showtable = false;
        usrextcfg.savebuttonlabel = 'Save';
        usrextcfg.currentPage = 1;
        usrextcfg.pageSize = "10";
        usrextcfg.UserExitTypes = [];
        usrextcfg.UserExitTypeCode = null;
        usrextcfg.pleaseWaitDiv = $('<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m"><div class="modal-content"><div class="modal-header"><h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body"><h1>Processing...</h1><div class="progress progress-striped active" style="margin-bottom:0;">' +
            '<div class="progress-bar" style="width: 100%"></div></div></div></div></div></div>');

        usrextcfg.showPleaseWait = function () {
            usrextcfg.pleaseWaitDiv.modal();
        };

        usrextcfg.hidePleaseWait = function () {
            usrextcfg.pleaseWaitDiv.modal('hide');
        };

        usrextcfg.resultsObj = {
            totalItems: null,
            currentPage: 1,
            setPage: function (pageNum) {
                usrextcfg.resultsObj.currentPage = pageNum;
            },
            setPageSize: function (size) {
                usrextcfg.resultsObj.pageSize = size;
                usrextcfg.resultsObj.pageChanged();
            },
            pageSize: 10,
            availPageSizes: [10, 20, 30, 40],
            numPages: 9,
            startRecord: 1,
            endRecord: 5,
            pageChanged: function () {
                var tempResults = usrextcfg.resultsObj.fullResults;
                var start = ((usrextcfg.resultsObj.currentPage == 1) ? 0 : ((usrextcfg.resultsObj.currentPage - 1) * usrextcfg.resultsObj.pageSize));
                var end = start + usrextcfg.resultsObj.pageSize;
                if (end > tempResults.length) {
                    end = tempResults.length;
                }
                usrextcfg.resultsObj.startRecord = start + 1;
                usrextcfg.resultsObj.endRecord = end;
                usrextcfg.resultsObj.currentResults.details = tempResults.slice(start, end);
            },
            fullResults: null,
            currentResults: { details: null }
        };

        usrextcfg.Uxt = {
            Exit_id: '',
            Exit_Value: '',
            action: '',
            Exit_Params: '',
            Params: [],
            NewParam: null
        }

        usrextcfg.AddParam = function () {
            if (usrextcfg.Uxt.Params == '' || usrextcfg.Uxt.Params == null)
                usrextcfg.Uxt.Params = [];
            if (usrextcfg.Uxt.NewParam != null) {
                var exists = false;
                angular.forEach(usrextcfg.Uxt.Params, function (prm, index) {
                    if (usrextcfg.Uxt.NewParam == prm) {
                        usrextcfg.showDialog('UserExit Details', "Param already exists.", 'UserExitDialog.html', 'md')
                        exists = true;
                    }
                });
                if (!exists) {
                    usrextcfg.Uxt.Params.push(angular.copy(usrextcfg.Uxt.NewParam));
                }
                usrextcfg.Uxt.NewParam = null;
            }
        }

        usrextcfg.DeleteParam = function (ind) {
            var params = [];
            angular.forEach(usrextcfg.Uxt.Params, function (key, index) {
                if (index != ind) {
                    params.push(key);
                }
            })
            usrextcfg.Uxt.Params = params;
        }

        usrextcfg.GetUxt_Types = function () {
            usrextcfg.showPleaseWait();
            $http({
                url: GlobalURLs.UserExitTypesList,
                method: 'get'
            }).success(function (data, status) {
                usrextcfg.hidePleaseWait();
                if (data.Status == GlobalConstants.Success) {
                    usrextcfg.UserExitTypes = data.Result.UserExits;
                }
                else
                {
                    usrextcfg.showDialog('UserExit Details', data.Error.Message, 'UserExitDialog.html', 'md')
                }
            }).error(function (data, status, headers, config) {
                usrextcfg.hidePleaseWait();
                usrextcfg.showDialog('UserExit Details', data, 'UserExitDialog.html', 'md')
            });
        };



        usrextcfg.Clear = function () {
            angular.forEach(usrextcfg.Uxt, function (value, key) {
                usrextcfg.Uxt[key] = '';
            });
        };

        usrextcfg.CallAdd = function () {
            usrextcfg.showadd = true;
            usrextcfg.savebuttonlabel = 'Save';
            usrextcfg.showtable = false;
            usrextcfg.Clear();
            usrextcfg.Uxt.Id = usrextcfg.UserExitTypeCode;
            usrextcfg.Uxt.action = 'Save';
            usrextcfg.Uxt.Params = [];
            usrextcfg.Uxt.NewParam = null ;
            usrextcfg.Uxt.Exit_Params = '';
        };

        usrextcfg.checkValidInputs = function (value) {
            if (value.Exit_Value != '' && value.Exit_Value != null && value.Exit_Params != null ) {
                return true;
            }
            else {
                return false;
            }
        };


        usrextcfg.Save = function () {

            if (usrextcfg.Uxt.action != 'Delete') {
                if (usrextcfg.Uxt.Params.length == 0) {
                    usrextcfg.showDialog('User Exit Configuration', "Please enter atleast one Param.", 'UserExitDialog.html')
                    return false;
                } else if (usrextcfg.Uxt.Params.length == 1) {
                    usrextcfg.Uxt.Exit_Params = usrextcfg.Uxt.Params[0];
                } else {
                    usrextcfg.Uxt.Exit_Params = usrextcfg.Uxt.Params.join("|");
                }
            }
            usrextcfg.Uxt.Id = usrextcfg.UserExitTypeCode;

            usrextcfg.showPleaseWait();
            $http({
                url: GlobalURLs.SaveUserExits,
                method: 'POST',
                data: usrextcfg.Uxt
            }).success(function (data, status) {
                if (data.Status == GlobalConstants.Success) {
                    usrextcfg.showDialog('User Exit Configuration', GlobalConstants.ActionSuccessMessage, 'UserExitDialog.html')
                } else {
                    usrextcfg.showDialog('User Exit Configuration', data.Error.Message, 'UserExitDialog.html')
                }
                usrextcfg.Clear();
                if (data.Result.UserExits != null) {
                    usrextcfg.resultsObj.fullResults = data.Result.UserExits;
                    usrextcfg.resultsObj.totalItems = data.Result.UserExits.length;
                    usrextcfg.resultsObj.setPage(1);
                    usrextcfg.resultsObj.pageChanged();
                    usrextcfg.results = data.Result.UserExits;
                } else {
                    usrextcfg.showDialog('User Exit Configuration', data.Error.Message, 'UserExitDialog.html')
                }
                usrextcfg.showadd = false;
                usrextcfg.showtable = true;
                usrextcfg.hidePleaseWait();
            }).error(function (data, status) {
                usrextcfg.hidePleaseWait();
                usrextcfg.showDialog('User Exit Configuration', data, 'UserExitDialog.html')

            });
        };


        usrextcfg.Cancel = function () {
            usrextcfg.Clear();
            usrextcfg.showadd = false;
            usrextcfg.showtable = true;
        };

        usrextcfg.PerformEditAction = function (item) {
            usrextcfg.Uxt.Exit_id = item.Exit_id;
            usrextcfg.Uxt.Exit_Value = item.Exit_Value;
            usrextcfg.Uxt.Exit_Params = item.Exit_Params;
            if (item.Exit_Params != null) {
                usrextcfg.Uxt.Params = item.Exit_Params.split("|");
            }
            usrextcfg.showadd = true;
            usrextcfg.savebuttonlabel = 'Update';
            usrextcfg.Uxt.action = 'Update';
            usrextcfg.showtable = false;
          
         };

        usrextcfg.PerformAddorUpdate = function () {
            debugger;
            if (usrextcfg.checkValidInputs(usrextcfg.Uxt)) {
                usrextcfg.Save();
            }
            else {
                usrextcfg.showDialog('User Exit Configuration', 'Please Enter All Config Values', 'UserExitDialog.html')
            }
        };

        usrextcfg.PerformDeleteAction = function (value) {
            usrextcfg.Uxt.Exit_id = value.Exit_id;
            usrextcfg.Uxt.Exit_Value = value.Exit_Value;
            usrextcfg.Uxt.Exit_Params = value.Exit_Params;
            usrextcfg.Uxt.action = 'Delete';
            usrextcfg.Save();
        };

        usrextcfg.GetUserExitValues = function (value) {
             if (value != '' && value != null) {
                usrextcfg.showPleaseWait();
                $http({
                    url: GlobalURLs.UserExitDetails,
                    method: 'get',
                    params:{id:value}
                }).success(function (data, status) {
                    if (data.Status == GlobalConstants.Success) {
                        if (data.Result.UserExits == '') {
                            usrextcfg.showDialog('UserExit Configuration', "No User Exists", 'UserExitDialog.html')

                        }
                        else {
                            usrextcfg.UserExitlist = data.Result.UserExits;
                        }
                        usrextcfg.resultsObj.fullResults = data.Result.UserExits;
                        usrextcfg.resultsObj.totalItems = data.Result.UserExits.length;
                        usrextcfg.resultsObj.setPage(1);
                        usrextcfg.resultsObj.pageChanged();
                        usrextcfg.results = data.Result.UserExits;
                        usrextcfg.hidePleaseWait();
                        usrextcfg.showtable = true;
                        usrextcfg.showadd = false;
                    }
                    else
                    {
                        usrextcfg.hidePleaseWait();
                        usrextcfg.showDialog('UserExit Configuration', data.Error.Message, 'UserExitDialog.html')
                    }
                }).error(function (data, status) {
                    usrextcfg.hidePleaseWait();
                    usrextcfg.showDialog('UserExit Configuration', data, 'UserExitDialog.html')
                });
            } else {
                usrextcfg.showDialog('UserExit Configuration', 'Please select user exit type', 'UserExitDialog.html')
            }


        };

        usrextcfg.showDialog = function (heading, details, template) {
            var dialog = $modal.open({
                templateUrl: template,
                controller: 'MDialogCtrl',
                size: 'md',
                resolve: {
                    heading: function () {
                        return heading;
                    },
                    details: function () {
                        return details;
                    }
                }
            });

        };



        usrextcfg.DeleteDialog = function (details) {
            var dialog = $modal.open({
                templateUrl: 'DeleteDialog.html',
                controller: 'MDialogCtrl',
                size: 'md',
                resolve: {
                    heading: function () {
                        return "Are you sure to delete " + details.Exit_Value + " ?";
                    },
                    details: function () {
                        return details;
                    }
                }
            });
            dialog.result.then(function (result) {
                if (result == 'Yes') {
                    usrextcfg.PerformDeleteAction(details);
                }
            });
        };

    }
    ]);

    myApp.controller('MDialogCtrl', function ($scope, $modalInstance, heading, details) {
        var dlgCtrl = this;
        $scope.dlgCtrl = dlgCtrl;
        dlgCtrl.heading = heading;
        dlgCtrl.details = details;
        dlgCtrl.Yes = function () {
            $modalInstance.close('Yes');
        };
        dlgCtrl.Close = function () {
            $modalInstance.dismiss('cancel');
        };
    });

})();